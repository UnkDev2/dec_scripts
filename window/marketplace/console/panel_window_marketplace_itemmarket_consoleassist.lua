local _panel = Panel_Window_MarketPlace_Main
local ItemMarket_ConsoleAssist = {
  _base,
  _ui = {},
  _focusIdx = 0
}
function PaGlobalFunc_ItemMarket_ConsoleAssist_SetSubList(idx)
  local self = ItemMarket_ConsoleAssist
  local list = self._base._ui.list_MarketItemList_Sub
  local content = list:GetContentByKey(toInt64(0, idx))
  local button = UI.getChildControl(content, "Template_Button_Purchase")
  button:addInputEvent("Mouse_LUp", "InputMLUp_ItemMarket_RequestBiddingList(" .. idx .. ")")
end
function ItemMarket_ConsoleAssist:initControl()
  self._base = PaGlobalFunc_ItemMarket_Get()
end
function ItemMarket_ConsoleAssist:initEvent()
end
function PaGlobalFunc_ItemMarket_ConsoleAssistInit()
  local self = ItemMarket_ConsoleAssist
  self:initControl()
  self:initEvent()
end
registerEvent("FromClient_luaLoadComplete", "PaGlobalFunc_ItemMarket_ConsoleAssistInit")
