function PaGlobal_DescBox_ItemCopy:init()
  if nil == Panel_Window_DescBox_ItemCopy then
    UI.ASSERT_NAME("\235\133\184\235\140\128\236\152\129", false, "PaGlobal_Equip_CharacterTag_ItemCopy:init \236\139\164\237\140\168 Panel_Window_Equip_CharacterTag_ItemCopy \237\140\168\235\132\144\236\157\180 \236\151\134\236\138\181\235\139\136\235\139\164")
    return
  end
  local stc_LineBG = UI.getChildControl(Panel_Window_DescBox_ItemCopy, "Static_LineBG")
  self._ui._frame1 = UI.getChildControl(stc_LineBG, "Frame_1")
  self._ui._frame_content1 = UI.getChildControl(self._ui._frame1, "Frame_1_Content")
  self._ui._txt_Desc = UI.getChildControl(self._ui._frame_content1, "StaticText_Desc")
  self._ui._txt_Desc:SetTextMode(__eTextMode_AutoWrap)
  self._ui._txt_Desc:SetText(self._ui._txt_Desc:GetText())
  self._ui._frame_scroll = UI.getChildControl(self._ui._frame1, "Frame_1_VerticalScroll")
  local gapY = 20
  local textSizeY = self._ui._txt_Desc:GetTextSizeY() + gapY
  self._ui._frame_content1:SetSize(self._ui._frame_content1:GetSizeX(), textSizeY)
  self._ui._txt_Desc:SetSize(self._ui._txt_Desc:GetSizeX(), textSizeY)
  local stc_titleArea = UI.getChildControl(Panel_Window_DescBox_ItemCopy, "Static_TitleArea")
  self._ui._btn_winClose = UI.getChildControl(stc_titleArea, "Button_Win_Close")
  self._ui._btn_function = UI.getChildControl(Panel_Window_DescBox_ItemCopy, "Button_Function")
  PaGlobal_DescBox_ItemCopy:validate()
  PaGlobal_DescBox_ItemCopy:registerEventHandler()
end
function PaGlobal_DescBox_ItemCopy:setConsoleUI()
  if true == self._isConsole then
  end
end
function PaGlobal_DescBox_ItemCopy:prepareOpen()
  if nil == Panel_Window_DescBox_ItemCopy then
    return
  end
  self:open()
end
function PaGlobal_DescBox_ItemCopy:open()
  if nil == Panel_Window_DescBox_ItemCopy then
    return
  end
  Panel_Window_DescBox_ItemCopy:SetShow(true)
end
function PaGlobal_DescBox_ItemCopy:prepareClose()
  if nil == Panel_Window_DescBox_ItemCopy then
    return
  end
  self:close()
end
function PaGlobal_DescBox_ItemCopy:close()
  if nil == Panel_Window_DescBox_ItemCopy then
    return
  end
  Panel_Window_DescBox_ItemCopy:SetShow(false)
end
function PaGlobal_DescBox_ItemCopy:registerEventHandler()
  if nil == Panel_Window_DescBox_ItemCopy then
    return
  end
  self._ui._btn_winClose:addInputEvent("Mouse_LUp", "PaGlobal_DescBox_ItemCopy_Close()")
  self._ui._btn_function:addInputEvent("Mouse_LUp", "PaGlobal_DescBox_ItemCopy_Close()")
end
function PaGlobal_DescBox_ItemCopy:validate()
  if nil == Panel_Window_DescBox_ItemCopy then
    UI.ASSERT_NAME(false, "Panel_Window_DescBox_ItemCopy \235\161\156\235\147\156\235\144\152\236\167\128 \236\149\138\236\149\152\236\138\181\235\139\136\235\139\164.", "\235\133\184\235\140\128\236\152\129")
    return
  end
  self._ui._frame1:isValidate()
  self._ui._frame_content1:isValidate()
  self._ui._frame_scroll:isValidate()
  self._ui._txt_Desc:isValidate()
  self._ui._btn_winClose:isValidate()
  self._ui._btn_function:isValidate()
end
