function PaGlobalFunc_IngameCashshop_GuildGiveaway_List2Update(content, key)
  local idx = Int64toInt32(key)
  local dataWrapper = ToClient_getGiveawayDataWrapper(idx)
  if nil == dataWrapper then
    return
  end
  local self = PaGlobal_IngameCashshop_GuildGiveaway
  local listBg = UI.getChildControl(content, "List2_Content_Bg")
  local txt_title = UI.getChildControl(listBg, "StaticText_MileageTitle")
  local btn_receive = UI.getChildControl(listBg, "Button_Reward")
  listBg:isValidate()
  txt_title:isValidate()
  btn_receive:isValidate()
  local txt_buyer = dataWrapper:getUserNickName()
  local txt_leftTime = converStringFromLeftDateTime(dataWrapper:getDeleteDate())
  local index_s64 = dataWrapper:getIndex()
  for ii = 0, __eGuildGiveawayItemMaxCount - 1 do
    local slotBg = UI.getChildControl(listBg, "Static_ItemSlotBg_" .. tostring(ii))
    local slot = {}
    local id = "ItemSlot_" .. tostring(idx) .. "_" .. tostring(ii)
    local control = UI.getChildControlNoneAssert(slotBg, "Static_" .. id)
    if nil == control then
      SlotItem.new(slot, id, ii, slotBg, self._slotConfig)
      slot:createChild()
    else
      SlotItem.reInclude(slot, id, ii, slotBg, self._slotConfig)
    end
    slot.icon:SetShow(true)
    slot.icon:addInputEvent("Mouse_On", "PaGlobal_IngameCashshop_GuildGiveaway:itemTooltipShow(" .. idx .. "," .. ii .. ")")
    slot.icon:addInputEvent("Mouse_Out", "PaGlobal_IngameCashshop_GuildGiveaway:itemTooltipHide()")
    local itemEnchantKey = ToClient_getGuildGiveawayItemEnchantKey(dataWrapper:getKeyRaw(), ii)
    local itemCount = ToClient_getGuildGiveawayItemCount(dataWrapper:getKeyRaw(), ii)
    local itemSSW = getItemEnchantStaticStatus(itemEnchantKey)
    if nil ~= itemSSW then
      slot:setItemByStaticStatus(itemSSW, itemCount)
    else
      slot:clearItem()
    end
    slotBg:SetShow(true)
  end
  txt_title:SetText(txt_buyer .. " / " .. txt_leftTime)
  btn_receive:addInputEvent("Mouse_LUp", "PaGlobal_IngameCashshop_GuildGiveaway:receiveItem(" .. tostring(index_s64) .. ")")
end
function PaGlobalFunc_IngameCashshop_requestGuildGiveawayList()
  local self = PaGlobal_IngameCashshop_GuildGiveaway
  self:prepareOpen()
end
function FromClient_responseGuildGiveawayList()
end
function FromClient_responseReceiveGuildGiveaway()
  local self = PaGlobal_IngameCashshop_GuildGiveaway
  self:updateGiveawayList()
end
