function PaGlobal_IngameCashshop_GuildGiveaway:initialize()
  self._ui._btn_close = UI.getChildControl(self._ui._stc_titleBg, "Button_Win_Close")
  self._ui._list2_giveawayList = UI.getChildControl(self._ui._stc_mainBg, "List2_RewardList")
  self:validate()
  self:registerEvent()
  self._initialize = true
end
function PaGlobal_IngameCashshop_GuildGiveaway:clear()
end
function PaGlobal_IngameCashshop_GuildGiveaway:prepareOpen()
  self:open()
end
function PaGlobal_IngameCashshop_GuildGiveaway:open()
  if false == _ContentsGroup_GuildGiveaway then
    return
  end
  if false == self._initialize then
    return
  end
  self:clear()
  self:updateGiveawayList()
  Panel_IngameCashShop_GuildGiveaway:SetShow(true)
end
function PaGlobal_IngameCashshop_GuildGiveaway:prepareClose()
  self:close()
end
function PaGlobal_IngameCashshop_GuildGiveaway:close()
  Panel_IngameCashShop_GuildGiveaway:SetShow(false)
end
function PaGlobal_IngameCashshop_GuildGiveaway:validate()
  self._ui._stc_titleBg:isValidate()
  self._ui._stc_mainBg:isValidate()
  self._ui._btn_close:isValidate()
end
function PaGlobal_IngameCashshop_GuildGiveaway:registerEvent()
  self._ui._btn_close:addInputEvent("Mouse_LUp", "PaGlobal_IngameCashshop_GuildGiveaway:prepareClose()")
  self._ui._list2_giveawayList:registEvent(__ePAUIList2EventType_LuaChangeContent, "PaGlobalFunc_IngameCashshop_GuildGiveaway_List2Update")
  self._ui._list2_giveawayList:createChildContent(__ePAUIList2ElementManagerType_List)
  self._ui._list2_giveawayList:ComputePos()
  registerEvent("FromClient_responseGuildGiveawayList", "FromClient_responseGuildGiveawayList")
  registerEvent("FromClient_responseReceiveGuildGiveaway", "FromClient_responseReceiveGuildGiveaway")
end
function PaGlobal_IngameCashshop_GuildGiveaway:updateGiveawayList()
  self._ui._list2_giveawayList:getElementManager():clearKey()
  local count = ToClient_getGiveawayDataCount()
  for ii = 0, count - 1 do
    self._ui._list2_giveawayList:getElementManager():pushKey(ii)
  end
end
function PaGlobal_IngameCashshop_GuildGiveaway:receiveItem(index)
  ToClient_requestReceiveGuildGiveaway(tonumber64(index))
end
function PaGlobal_IngameCashshop_GuildGiveaway:itemTooltipShow(ii, jj)
end
function PaGlobal_IngameCashshop_GuildGiveaway:itemTooltipHide()
  TooltipSimple_Hide()
end
