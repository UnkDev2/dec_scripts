PaGlobal_IngameCashshop_GuildGiveaway = {
  _ui = {
    _stc_titleBg = UI.getChildControl(Panel_IngameCashShop_GuildGiveaway, "Static_TitleArea"),
    _stc_mainBg = UI.getChildControl(Panel_IngameCashShop_GuildGiveaway, "Static_GuildRewardListArea")
  },
  _slotConfig = {
    createIcon = true,
    createBorder = true,
    createEnchant = true,
    createCount = true,
    createCash = true,
    createEnduranceIcon = true
  },
  _initialize = false
}
runLua("UI_Data/Script/Window/IngameCashShop/Panel_IngameCashShop_GuildGiveaway_1.lua")
runLua("UI_Data/Script/Window/IngameCashShop/Panel_IngameCashShop_GuildGiveaway_2.lua")
registerEvent("FromClient_luaLoadComplete", "FromClient_IngameCashShop_GuildGiveaway_Init")
function FromClient_IngameCashShop_GuildGiveaway_Init()
  PaGlobal_IngameCashshop_GuildGiveaway:initialize()
end
