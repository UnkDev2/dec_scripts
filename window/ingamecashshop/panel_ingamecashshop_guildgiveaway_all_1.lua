function PaGlobal_IngameCashshop_GuildGiveaway:initialize()
  if true == PaGlobal_IngameCashshop_GuildGiveaway._initialize then
    return
  end
  self._isConsole = _ContentsGroup_UsePadSnapping
  local stc_TopGroup = UI.getChildControl(Panel_IngameCashShop_GuildGiveaway_All, "Static_TopGroup")
  self._ui_pc.btn_Close = UI.getChildControl(stc_TopGroup, "Button_Win_Close")
  self._ui_pc.btn_Left = UI.getChildControl(Panel_IngameCashShop_GuildGiveaway_All, "Button_Arrow_Left")
  self._ui_pc.btn_Right = UI.getChildControl(Panel_IngameCashShop_GuildGiveaway_All, "Button_Arrow_Right")
  self._ui_pc.btn_GetItem = UI.getChildControl(Panel_IngameCashShop_GuildGiveaway_All, "Button_GetItem")
  self._ui.stc_Deco = UI.getChildControl(Panel_IngameCashShop_GuildGiveaway_All, "Static_DecoTexture")
  self._ui.stc_TimeBg = UI.getChildControl(self._ui.stc_Deco, "Static_LimitTimeBG")
  self._ui.txt_RewardCount = UI.getChildControl(self._ui.stc_Deco, "StaticText_Count")
  self._ui.txt_Time = UI.getChildControl(self._ui.stc_TimeBg, "StaticText_Time")
  self._ui.txt_SubTitle = UI.getChildControl(Panel_IngameCashShop_GuildGiveaway_All, "StaticText_SubTitle")
  self._ui.txt_UserName = UI.getChildControl(Panel_IngameCashShop_GuildGiveaway_All, "StaticText_UserName")
  self._ui.txt_DescBox = UI.getChildControl(Panel_IngameCashShop_GuildGiveaway_All, "Static_DescBox")
  self._ui.txt_SubText = UI.getChildControl(Panel_IngameCashShop_GuildGiveaway_All, "StaticText_SubText")
  self._ui.txt_NoPresent = UI.getChildControl(Panel_IngameCashShop_GuildGiveaway_All, "StaticText_NoPresent")
  self._ui.stc_ItemSlot = UI.getChildControl(Panel_IngameCashShop_GuildGiveaway_All, "Static_ItemSlot")
  local parent = UI.getChildControl(self._ui.stc_Deco, "Static_MainSlot")
  local temp = {}
  SlotItem.new(temp, "MainItemSlot", 0, parent, self._slotConfig)
  temp:createChild()
  temp.icon:SetPosX(4)
  temp.icon:SetPosY(4)
  temp.icon:SetSize(42, 42)
  temp.border:SetSize(44, 44)
  self._mainItemSlot = temp
  self._mainItemSlot:clearItem()
  for idx = 0, self._selectRewardCount - 1 do
    local parent = UI.getChildControl(self._ui.stc_ItemSlot, "Static_ItemSlot0" .. tostring(idx + 1))
    local temp = {}
    SlotItem.new(temp, "ItemSlot", idx, parent, self._slotConfig)
    temp:createChild()
    temp.icon:SetPosX(4)
    temp.icon:SetPosY(4)
    temp.icon:SetSize(42, 42)
    temp.border:SetSize(44, 44)
    self._selectItemSlot[idx] = temp
    self._selectItemSlot[idx]:clearItem()
  end
  self._timePosX = self._ui.stc_TimeBg:GetPosX()
  self._timeSizeX = self._ui.stc_TimeBg:GetSizeX()
  self._timeEndPosX = self._timePosX + self._timeSizeX
  self._ui.stc_smallDeco = UI.getChildControl(Panel_IngameCashShop_GuildGiveaway_All, "Static_Deco_Small")
  self._ui.stc_smallCount = UI.getChildControl(self._ui.stc_smallDeco, "StaticText_Count")
  self._ui.stc_smallTimeBg = UI.getChildControl(self._ui.stc_smallDeco, "Static_LimitTimeBG")
  self._ui.txt_smallTime = UI.getChildControl(self._ui.stc_smallTimeBg, "StaticText_Time")
  local smallparent = UI.getChildControl(self._ui.stc_smallDeco, "Static_MainSlot")
  local smalltemp = {}
  SlotItem.new(smalltemp, "SmallMainItemSlot", 0, smallparent, self._slotConfig)
  smalltemp:createChild()
  temp.icon:SetSize(42, 42)
  temp.border:SetSize(44, 44)
  self._ui.stc_smallmainSlot = smalltemp
  self._ui.stc_smallmainSlot:clearItem()
  self._ui.stc_smallmainSlot.icon:addInputEvent("Mouse_Out", "")
  Panel_IngameCashShop_GuildGiveaway_All:SetSize(Panel_IngameCashShop_GuildGiveaway_All:GetSizeX(), Panel_IngameCashShop_GuildGiveaway_All:GetSizeY() - 130)
  Panel_IngameCashShop_GuildGiveaway_All:ComputePosAllChild()
  self._ui_pc.btn_Left:SetShow(false)
  self._ui_pc.btn_Right:SetShow(false)
  self._ui.txt_SubText:SetShow(true)
  self._ui.txt_RewardCount:SetIgnore(false)
  self._ui.txt_Time:SetIgnore(false)
  self._ui.txt_NoPresent:SetText(PAGetString(Defines.StringSheet_GAME, "LUA_GUILDGIVEAWAY_CHOISEREWARD"))
  self._ui.txt_NoPresent:SetShow(false)
  self._ui.txt_DescBox:SetShow(false)
  self._ui.stc_Deco:SetShow(false)
  self._ui.stc_ItemSlot:SetShow(false)
  self._ui.stc_smallDeco:SetShow(true)
  self._ui.stc_smallCount:SetIgnore(false)
  self._ui.txt_smallTime:SetIgnore(false)
  PaGlobal_IngameCashshop_GuildGiveaway:registEventHandler()
  PaGlobal_IngameCashshop_GuildGiveaway:validate()
  PaGlobal_IngameCashshop_GuildGiveaway._initialize = true
end
function PaGlobal_IngameCashshop_GuildGiveaway:registEventHandler()
  if nil == Panel_IngameCashShop_GuildGiveaway_All then
    return
  end
  if self._isConsole then
  else
    self._ui_pc.btn_Close:addInputEvent("Mouse_LUp", "PaGlobal_IngameCashshop_GuildGiveaway_Close()")
    self._ui.txt_RewardCount:addInputEvent("Mouse_On", "HandleEventMInOut_IngameCashshop_GuildGiveaway_RewardCountTooltip( true)")
    self._ui.txt_RewardCount:addInputEvent("Mouse_Out", "HandleEventMInOut_IngameCashshop_GuildGiveaway_RewardCountTooltip( false )")
    self._ui.txt_Time:addInputEvent("Mouse_On", "HandleEventMInOut_IngameCashshop_GuildGiveaway_RewardTimeTooltip( true)")
    self._ui.txt_Time:addInputEvent("Mouse_Out", "HandleEventMInOut_IngameCashshop_GuildGiveaway_RewardTimeTooltip( false )")
    self._ui.stc_smallCount:addInputEvent("Mouse_On", "HandleEventMInOut_IngameCashshop_GuildGiveaway_RewardCountTooltip( true)")
    self._ui.stc_smallCount:addInputEvent("Mouse_Out", "HandleEventMInOut_IngameCashshop_GuildGiveaway_RewardCountTooltip( false )")
    self._ui.txt_smallTime:addInputEvent("Mouse_On", "HandleEventMInOut_IngameCashshop_GuildGiveaway_RewardTimeTooltip( true)")
    self._ui.txt_smallTime:addInputEvent("Mouse_Out", "HandleEventMInOut_IngameCashshop_GuildGiveaway_RewardTimeTooltip( false )")
  end
  registerEvent("FromClient_responseGuildGiveawayList", "FromClient_responseGuildGiveawayList")
  registerEvent("FromClient_setNewGuildGiveaway", "FromClient_setNewGuildGiveaway")
  registerEvent("FromClient_responseReceiveGuildGiveaway", "FromClient_responseReceiveGuildGiveaway")
  registerEvent("FromClient_UpdatePearlMileage", "FromClient_IngameCashshop_GuildGiveaway_UpdateRewardEffect")
end
function PaGlobal_IngameCashshop_GuildGiveaway:prepareOpen()
  if nil == Panel_IngameCashShop_GuildGiveaway_All then
    return
  end
  if true == PaGlobal_IngameCashshop_GuildGiveaway:isExpireDate() then
    Proc_ShowMessage_Ack(PAGetString(Defines.StringSheet_GAME, "LUA_GUILDGIVEAWAY_NO_REWARD"))
    return
  end
  local showIndex = PaGlobal_IngameCashshop_GuildGiveaway:isNonExpireRewardIndex()
  if -1 == showIndex then
  end
  PaGlobal_IngameCashshop_GuildGiveaway:update(showIndex)
  PaGlobal_IngameCashshop_GuildGiveaway:open()
end
function PaGlobal_IngameCashshop_GuildGiveaway:open()
  if nil == Panel_IngameCashShop_GuildGiveaway_All then
    return
  end
  Panel_IngameCashShop_GuildGiveaway_All:SetShow(true)
end
function PaGlobal_IngameCashshop_GuildGiveaway:prepareClose()
  if nil == Panel_IngameCashShop_GuildGiveaway_All then
    return
  end
  self._selectItemIndex = -1
  PaGlobal_IngameCashshop_GuildGiveaway:close()
end
function PaGlobal_IngameCashshop_GuildGiveaway:close()
  if nil == Panel_IngameCashShop_GuildGiveaway_All then
    return
  end
  Panel_IngameCashShop_GuildGiveaway_All:SetShow(false)
end
function PaGlobal_IngameCashshop_GuildGiveaway:update(idx)
  if nil == Panel_IngameCashShop_GuildGiveaway_All then
    return
  end
  local rewardCount = ToClient_getGiveawayDataCount()
  if idx >= rewardCount then
    return
  end
  local dataWrapper = ToClient_getGiveawayDataWrapper(idx)
  if nil == dataWrapper then
    return
  end
  local txt_leftTime = converStringFromLeftDateTime(dataWrapper:getDeleteDate())
  local index_s64 = dataWrapper:getIndex()
  local itemEnchantKey = ToClient_getGuildGiveawayItemEnchantKey(dataWrapper:getKeyRaw(), 0)
  local itemCount = ToClient_getGuildGiveawayItemCount(dataWrapper:getKeyRaw(), 0)
  local itemSSW = getItemEnchantStaticStatus(itemEnchantKey)
  if nil ~= itemSSW then
    self._mainItemSlot:clearItem()
    self._mainItemSlot:setItemByStaticStatus(itemSSW, itemCount)
    self._ui.txt_SubTitle:SetText(itemSSW:getName())
    self._mainItemSlot.icon:SetShow(true)
    self._mainItemSlot.icon:addInputEvent("Mouse_On", "HandleEventMOn_IngameCashshop_GuildGiveaway_ItemTooltipShow(true , 0 ," .. idx .. ")")
    self._mainItemSlot.icon:addInputEvent("Mouse_Out", "HandleEventMOut_IngameCashshop_GuildGiveaway_ItemTooltipHide()")
  else
    self._ui.txt_SubTitle:SetText("")
    self._mainItemSlot:clearItem()
    self._mainItemSlot.icon:SetShow(false)
    self._mainItemSlot.icon:addInputEvent("Mouse_On", "")
    self._mainItemSlot.icon:addInputEvent("Mouse_Out", "")
  end
  local selectListCount = ToClient_getGuildGiveawaySelectedListCount(dataWrapper:getKeyRaw())
  for index = 0, self._selectRewardCount - 1 do
    if index < selectListCount then
      local selectItemEnchantKey = ToClient_getGuildGiveawaySelectedItemEnchantKey(dataWrapper:getKeyRaw(), index)
      local selectItemCount = ToClient_getGuildGiveawaySelectedItemCount(dataWrapper:getKeyRaw(), index)
      local selectItemSSW = getItemEnchantStaticStatus(selectItemEnchantKey)
      if nil ~= selectItemSSW then
        self._selectItemSlot[index]:clearItem()
        self._selectItemSlot[index]:setItemByStaticStatus(selectItemSSW, selectItemCount)
        self._selectItemSlot[index].icon:SetShow(true)
        self._selectItemSlot[index].icon:addInputEvent("Mouse_LUp", "HandleEventLUp_IngameCashshop_GuildGiveaway_SelectItem(" .. index .. ")")
        self._selectItemSlot[index].icon:addInputEvent("Mouse_On", "HandleEventMOn_IngameCashshop_GuildGiveaway_ItemTooltipShow(false , " .. index .. ", " .. idx .. ")")
        self._selectItemSlot[index].icon:addInputEvent("Mouse_Out", "HandleEventMOut_IngameCashshop_GuildGiveaway_ItemTooltipHide()")
      else
        self._selectItemSlot[index]:clearItem()
        self._selectItemSlot[index].icon:SetShow(false)
        self._selectItemSlot[index].icon:addInputEvent("Mouse_On", "")
        self._selectItemSlot[index].icon:addInputEvent("Mouse_Out", "")
      end
    else
      self._selectItemSlot[index]:clearItem()
      self._selectItemSlot[index].icon:SetShow(false)
      self._selectItemSlot[index].icon:addInputEvent("Mouse_On", "")
      self._selectItemSlot[index].icon:addInputEvent("Mouse_Out", "")
    end
  end
  local nonExpireRewardCount = PaGlobal_IngameCashshop_GuildGiveaway:isNonExpireRewardCount()
  local txt_buyer = dataWrapper:getUserNickName()
  if nil ~= txt_buyer and "" ~= txt_buyer then
    txt_buyer = "<PAColor0xFFFFCE22>" .. "[" .. txt_buyer .. "]" .. "<PAOldColor>"
    self._ui.txt_UserName:SetText(txt_buyer)
    self._ui.txt_UserName:SetShow(true)
  else
    self._ui.txt_UserName:SetShow(false)
  end
  self._ui.txt_RewardCount:SetText(nonExpireRewardCount)
  self._ui.txt_Time:SetText(txt_leftTime)
  local diff = self._ui.stc_TimeBg:GetPosX() + self._ui.txt_Time:GetTextSizeX() + 40 - self._timeEndPosX
  if diff > 0 then
    self._ui.stc_TimeBg:SetPosX(self._timePosX - diff)
    self._ui.stc_TimeBg:SetSize(self._timeSizeX + diff, self._ui.stc_TimeBg:GetSizeY())
  end
  self._ui_pc.btn_GetItem:addInputEvent("Mouse_LUp", "HandleEventLUp_IngameCashshop_GuildGiveaway_GetItem(" .. tostring(index_s64) .. ")")
  self._ui_pc.btn_GetItem:SetIgnore(false)
  self._selectItemIndex = -1
  if selectListCount > 0 then
    self._ui.txt_NoPresent:SetShow(true)
  else
    self._ui.txt_NoPresent:SetShow(false)
  end
  self._ui.stc_smallCount:SetText(nonExpireRewardCount)
  self._ui.txt_smallTime:SetText(txt_leftTime)
  local diff = self._ui.stc_smallTimeBg:GetPosX() + self._ui.txt_smallTime:GetTextSizeX() + 40 - self._timeEndPosX
  if diff > 0 then
    self._ui.stc_smallTimeBg:SetPosX(self._timePosX - diff)
    self._ui.stc_smallTimeBg:SetSize(self._timeSizeX + diff, self._ui.stc_smallTimeBg:GetSizeY())
  end
  if nil ~= itemSSW then
    self._ui.stc_smallmainSlot:clearItem()
    self._ui.stc_smallmainSlot:setItemByStaticStatus(itemSSW, itemCount)
    self._ui.txt_SubTitle:SetText(itemSSW:getName())
    self._ui.stc_smallmainSlot.icon:SetShow(true)
    self._ui.stc_smallmainSlot.icon:addInputEvent("Mouse_On", "HandleEventMOn_IngameCashshop_GuildGiveaway_ItemTooltipShow(true , 0 ," .. idx .. ")")
    self._ui.stc_smallmainSlot.icon:addInputEvent("Mouse_Out", "HandleEventMOut_IngameCashshop_GuildGiveaway_ItemTooltipHide()")
  else
    self._ui.stc_smallmainSlot:clearItem()
  end
end
function PaGlobal_IngameCashshop_GuildGiveaway:updatePerFrame(deltaTime)
  if nil == Panel_IngameCashShop_GuildGiveaway_All then
    return
  end
  self._curTime = self._curTime + deltaTime
  if self._curTime < 2 then
    return
  end
  self._curTime = 0
  self._ui.txt_UserName:EraseAllEffect()
  self._mainItemSlot.icon:EraseAllEffect()
  for index = 0, self._selectRewardCount - 1 do
    self._selectItemSlot[index].icon:EraseAllEffect()
  end
  self._ui.stc_smallmainSlot.icon:EraseAllEffect()
  local showIndex = PaGlobal_IngameCashshop_GuildGiveaway:isNonExpireRewardIndex()
  if -1 == showIndex then
    Proc_ShowMessage_Ack(PAGetString(Defines.StringSheet_GAME, "LUA_GUILDGIVEAWAY_All_RECIEVE"))
    PaGlobal_IngameCashshop_GuildGiveaway_Close()
    return
  end
  PaGlobal_IngameCashshop_GuildGiveaway:update(showIndex)
end
function PaGlobal_IngameCashshop_GuildGiveaway:updateRecieveItem()
  if nil == Panel_IngameCashShop_GuildGiveaway_All then
    return
  end
  self._selectItemIndex = -1
  self._ui_pc.btn_GetItem:SetIgnore(true)
  PaGlobal_IngameCashshop_GuildGiveaway:checkBtnEffect()
end
function PaGlobal_IngameCashshop_GuildGiveaway:validate()
  if nil == Panel_IngameCashShop_GuildGiveaway_All then
    return
  end
end
function PaGlobal_IngameCashshop_GuildGiveaway:checkBtnEffect()
  if nil == Panel_IngameCashShop_GuildGiveaway_All then
    return
  end
  local rewardCount = PaGlobal_IngameCashshop_GuildGiveaway:isNonExpireRewardCount()
  PaGlobalFunc_IngameCashShopSetEquip_GuildRewardBtnEffect(rewardCount)
end
function PaGlobal_IngameCashshop_GuildGiveaway:isExpireDate()
  local rewardCount = ToClient_getGiveawayDataCount()
  if rewardCount <= 0 then
    return true
  end
  local expireCount = 0
  for idx = 0, rewardCount - 1 do
    local dataWrapper = ToClient_getGiveawayDataWrapper(idx)
    if nil ~= dataWrapper then
      local leftDate = getLeftSecond_TTime64(dataWrapper:getDeleteDate())
      if leftDate <= toInt64(0, 0) then
        expireCount = expireCount + 1
      end
    end
  end
  if expireCount == rewardCount then
    return true
  end
  return false
end
function PaGlobal_IngameCashshop_GuildGiveaway:isNonExpireRewardCount()
  local rewardCount = ToClient_getGiveawayDataCount()
  if rewardCount <= 0 then
    return 0
  end
  local nonExpireCount = 0
  for idx = 0, rewardCount - 1 do
    local dataWrapper = ToClient_getGiveawayDataWrapper(idx)
    if nil ~= dataWrapper then
      local leftDate = getLeftSecond_TTime64(dataWrapper:getDeleteDate())
      if leftDate > toInt64(0, 0) then
        nonExpireCount = nonExpireCount + 1
      end
    end
  end
  return nonExpireCount
end
function PaGlobal_IngameCashshop_GuildGiveaway:isNonExpireRewardIndex()
  local rewardCount = ToClient_getGiveawayDataCount()
  if rewardCount <= 0 then
    return -1
  end
  local expireCount = 0
  for idx = 0, rewardCount - 1 do
    local dataWrapper = ToClient_getGiveawayDataWrapper(idx)
    if nil ~= dataWrapper then
      local leftDate = getLeftSecond_TTime64(dataWrapper:getDeleteDate())
      if leftDate > toInt64(0, 0) then
        return idx
      end
    end
  end
  return -1
end
function PaGlobal_IngameCashshop_GuildGiveaway:itemTooltipShow(isMainSlot, selectSlotIdex, RewardIndex)
  if true == isMainSlot then
    if nil == self._ui.stc_smallmainSlot then
      return
    end
    local dataWrapper = ToClient_getGiveawayDataWrapper(RewardIndex)
    if nil == dataWrapper then
      return
    end
    local itemEnchantKey = ToClient_getGuildGiveawayItemEnchantKey(dataWrapper:getKeyRaw(), RewardIndex)
    local itemSSW = getItemEnchantStaticStatus(itemEnchantKey)
    if nil ~= itemSSW then
      local name = itemSSW:getName()
      local desc = itemSSW:getDescription()
      Panel_Tooltip_Item_Show(itemSSW, self._ui.stc_smallmainSlot.icon, true)
    end
  else
    if nil == self._selectItemSlot[selectSlotIdex] then
      return
    end
    local dataWrapper = ToClient_getGiveawayDataWrapper(RewardIndex)
    if nil == dataWrapper then
      return
    end
    local itemEnchantKey = ToClient_getGuildGiveawaySelectedItemEnchantKey(dataWrapper:getKeyRaw(), selectSlotIdex)
    local itemSSW = getItemEnchantStaticStatus(itemEnchantKey)
    if nil ~= itemSSW then
      local name = itemSSW:getName()
      local desc = itemSSW:getDescription()
      Panel_Tooltip_Item_Show(itemSSW, self._selectItemSlot[selectSlotIdex].icon, true)
    end
  end
end
function PaGlobal_IngameCashshop_GuildGiveaway:itemTooltipHide()
  Panel_Tooltip_Item_hideTooltip()
end
