function HandleEventLUp_IngameCashshop_GuildGiveaway_SelectItem(index)
  if nil == index then
    return
  end
  PaGlobal_IngameCashshop_GuildGiveaway._selectItemIndex = index
end
function HandleEventLUp_IngameCashshop_GuildGiveaway_GetItem(index)
  if nil == Panel_IngameCashShop_GuildGiveaway_All then
    return
  end
  if nil == index then
    return
  end
  if true == PaGlobal_IngameCashshop_GuildGiveaway._ui.txt_UserName:GetShow() then
    PaGlobal_IngameCashshop_GuildGiveaway._ui.txt_UserName:AddEffect("fUI_GuildCompensation_Name_01A", true, 0, 0)
  end
  if true == PaGlobal_IngameCashshop_GuildGiveaway._mainItemSlot.icon:GetShow() then
    PaGlobal_IngameCashshop_GuildGiveaway._mainItemSlot.icon:AddEffect("fUI_GuildCompensation_01A", true, 0, 0)
  end
  for idx = 0, PaGlobal_IngameCashshop_GuildGiveaway._selectRewardCount - 1 do
    if nil ~= PaGlobal_IngameCashshop_GuildGiveaway._selectItemSlot[idx] and true == PaGlobal_IngameCashshop_GuildGiveaway._selectItemSlot[idx].icon:GetShow() then
      PaGlobal_IngameCashshop_GuildGiveaway._selectItemSlot[idx].icon:AddEffect("fUI_GuildCompensation_01A", true, 0, 0)
    end
  end
  if true == PaGlobal_IngameCashshop_GuildGiveaway._ui.stc_smallmainSlot.icon:GetShow() then
    PaGlobal_IngameCashshop_GuildGiveaway._ui.stc_smallmainSlot.icon:AddEffect("fUI_GuildCompensation_01A", true, 0, 0)
  end
  Panel_IngameCashShop_GuildGiveaway_All:RegisterUpdateFunc("PaGlobal_IngameCashshop_GuildGiveaway_UpdatePerFrame")
  ToClient_requestReceiveGuildGiveaway(tonumber64(index), PaGlobal_IngameCashshop_GuildGiveaway._selectItemIndex)
end
function HandleEventMOn_IngameCashshop_GuildGiveaway_ItemTooltipShow(isMainSlot, selectSlotIdex, RewardIndex)
  if nil == Panel_IngameCashShop_GuildGiveaway_All then
    return
  end
  if nil == isMainSlot or nil == selectSlotIdex or nil == RewardIndex then
    return
  end
  PaGlobal_IngameCashshop_GuildGiveaway:itemTooltipShow(isMainSlot, selectSlotIdex, RewardIndex)
end
function HandleEventMOut_IngameCashshop_GuildGiveaway_ItemTooltipHide()
  PaGlobal_IngameCashshop_GuildGiveaway:itemTooltipHide()
end
function HandleEventMInOut_IngameCashshop_GuildGiveaway_RewardCountTooltip(isOn)
  local control
  local name = ""
  local desc
  if true == isOn then
    control = PaGlobal_IngameCashshop_GuildGiveaway._ui.stc_smallCount
    name = PAGetString(Defines.StringSheet_GAME, "LUA_GUILDGIVEAWAY_REWARDCOUNT_TITLE")
    desc = PAGetString(Defines.StringSheet_GAME, "LUA_GUILDGIVEAWAY_REWARDCOUNT_DESC")
    TooltipSimple_Show(control, name, desc)
  else
    TooltipSimple_Hide()
  end
end
function HandleEventMInOut_IngameCashshop_GuildGiveaway_RewardTimeTooltip(isOn)
  local control
  local name = ""
  local desc
  if true == isOn then
    control = PaGlobal_IngameCashshop_GuildGiveaway._ui.stc_smallTimeBg
    name = PAGetString(Defines.StringSheet_GAME, "LUA_GUILDGIVEAWAY_REWARDTIME_TITLE")
    desc = PAGetString(Defines.StringSheet_GAME, "LUA_GUILDGIVEAWAY_REWARDTIME_DESC")
    TooltipSimple_Show(control, name, desc)
  else
    TooltipSimple_Hide()
  end
end
function PaGlobal_IngameCashshop_GuildGiveaway_Open()
  if nil == Panel_IngameCashShop_GuildGiveaway_All then
    return
  end
  PaGlobal_IngameCashshop_GuildGiveaway:prepareOpen()
end
function PaGlobal_IngameCashshop_GuildGiveaway_Close()
  if nil == Panel_IngameCashShop_GuildGiveaway_All then
    return
  end
  PaGlobal_IngameCashshop_GuildGiveaway:prepareClose()
end
function PaGlobal_IngameCashshop_GuildGiveaway_UpdatePerFrame(deltaTime)
  if nil == Panel_IngameCashShop_GuildGiveaway_All then
    return
  end
  PaGlobal_IngameCashshop_GuildGiveaway:updatePerFrame(deltaTime)
end
function FromClient_responseReceiveGuildGiveaway()
  if nil == Panel_IngameCashShop_GuildGiveaway_All then
    return
  end
  PaGlobal_IngameCashshop_GuildGiveaway:updateRecieveItem()
end
function FromClient_IngameCashshop_GuildGiveaway_UpdateRewardEffect()
  if nil == Panel_IngameCashShop_GuildGiveaway_All then
    return
  end
  PaGlobal_IngameCashshop_GuildGiveaway:checkBtnEffect()
end
function FromClient_responseGuildGiveawayList()
end
function FromClient_setNewGuildGiveaway()
  if nil == Panel_IngameCashShop_GuildGiveaway_All then
    return
  end
  PaGlobal_IngameCashshop_GuildGiveaway:checkBtnEffect()
end
