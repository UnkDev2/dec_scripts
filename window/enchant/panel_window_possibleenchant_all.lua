PaGlobal_PossibleEnchant_All = {
  _ui = {
    txt_title = nil,
    list2_title = nil,
    txt_subTitle = nil,
    list2_subtitle = nil,
    btn_close = nil
  },
  _slotConfig = {
    createIcon = false,
    createBorder = true,
    createCount = true,
    createEnchant = true,
    createCash = true
  },
  _titleList = {},
  _subtitleList = {},
  _LIST_TYPE = {
    EQUIP = 1,
    VALKS = 2,
    MATERIAL = 3,
    DARK = 4,
    BOOK = 5
  },
  _controlSize = {
    [1] = {
      titleListSizeY = 435,
      subTitleSpanY = 515,
      subListSpanY = 550,
      subListSizeY = 250,
      isShowSubList = ToClient_IsContentsGroupOpen("234")
    },
    [2] = {
      titleListSizeY = 170,
      subTitleSpanY = 245,
      subListSpanY = 280,
      subListSizeY = 525,
      isShowSubList = true
    },
    [3] = {
      titleListSizeY = 745,
      subTitleSpanY = 515,
      subListSpanY = 550,
      subListSizeY = 250,
      isShowSubList = false
    },
    [4] = {
      titleListSizeY = 745,
      subTitleSpanY = 515,
      subListSpanY = 550,
      subListSizeY = 250,
      isShowSubList = false
    },
    [5] = {
      titleListSizeY = 745,
      subTitleSpanY = 515,
      subListSpanY = 550,
      subListSizeY = 250,
      isShowSubList = false
    }
  },
  _curlistType = nil,
  _prevlistType = nil,
  _isLastEnchant = false,
  _whereType = CppEnums.ItemWhereType.eInventory,
  _cashWhereType = CppEnums.ItemWhereType.eCashInventory,
  _initialize = false
}
runLua("UI_Data/Script/Window/Enchant/Panel_Window_PossibleEnchant_All_1.lua")
runLua("UI_Data/Script/Window/Enchant/Panel_Window_PossibleEnchant_All_2.lua")
registerEvent("FromClient_luaLoadComplete", "FromClient_PossibleEnchant_All_Init")
function FromClient_PossibleEnchant_All_Init()
  PaGlobal_PossibleEnchant_All:initialize()
end
