function PaGlobal_PossibleEnchant_All:initialize()
  if true == PaGlobal_PossibleEnchant_All._initialize then
    return
  end
  self._ui.txt_title = UI.getChildControl(Panel_Window_PossibleEnchant_All, "StaticText_Title")
  self._ui.list2_title = UI.getChildControl(Panel_Window_PossibleEnchant_All, "List2_PossibleEquip")
  local listContent = UI.getChildControl(self._ui.list2_title, "List2_1_Content")
  local btn_list = UI.getChildControl(listContent, "RadioButton_EnchantEquipItem")
  local stc_slotBg = UI.getChildControl(btn_list, "Static_ItemSlotBG")
  local stc_slot = UI.getChildControl(stc_slotBg, "Static_ItemIcon")
  local slot = {}
  SlotItem.new(slot, "PossibleEnchant_All_Slot_", 0, stc_slot, self._slotConfig)
  slot:createChild()
  slot.empty = true
  slot:clearItem()
  slot.icon:SetPosX(1)
  slot.icon:SetPosY(1)
  slot.border:SetSize(44, 44)
  self._ui.txt_subTitle = UI.getChildControl(Panel_Window_PossibleEnchant_All, "StaticText_CaphrasPossibleItem")
  self._ui.list2_subtitle = UI.getChildControl(Panel_Window_PossibleEnchant_All, "List2_CaphrasPossibleItem")
  local sub_listContent = UI.getChildControl(self._ui.list2_subtitle, "List2_2_Content")
  local sub_btn_list = UI.getChildControl(sub_listContent, "RadioButton_CaphrasEnchantItem")
  local sub_stc_slotBg = UI.getChildControl(sub_btn_list, "Static_ItemSlotBG")
  local sub_stc_slot = UI.getChildControl(sub_stc_slotBg, "Static_ItemIcon")
  local sub_slot = {}
  SlotItem.new(sub_slot, "PossibleEnchant_All_SubSlot_", 0, sub_stc_slot, self._slotConfig)
  sub_slot:createChild()
  sub_slot.empty = true
  sub_slot:clearItem()
  sub_slot.icon:SetPosX(1)
  sub_slot.icon:SetPosY(1)
  sub_slot.border:SetSize(44, 44)
  self._ui.btn_close = UI.getChildControl(Panel_Window_PossibleEnchant_All, "Button_Close")
  PaGlobal_PossibleEnchant_All:registEventHandler()
  PaGlobal_PossibleEnchant_All:validate()
  PaGlobal_PossibleEnchant_All._initialize = true
end
function PaGlobal_PossibleEnchant_All:prepareOpen()
  if nil == Panel_Window_PossibleEnchant_All then
    return
  end
  self._ui.btn_close:SetShow(false)
  PaGlobal_PossibleEnchant_All._curlistType = PaGlobal_PossibleEnchant_All._LIST_TYPE.EUQIP
  PaGlobal_PossibleEnchant_All:resize()
  PaGlobal_PossibleEnchant_All:open()
end
function PaGlobal_PossibleEnchant_All:open()
  if nil == Panel_Window_PossibleEnchant_All then
    return
  end
  Panel_Window_PossibleEnchant_All:SetShow(true)
end
function PaGlobal_PossibleEnchant_All:prepareClose()
  if nil == Panel_Window_PossibleEnchant_All then
    return
  end
  PaGlobal_PossibleEnchant_All:close()
end
function PaGlobal_PossibleEnchant_All:close()
  if nil == Panel_Window_PossibleEnchant_All then
    return
  end
  Panel_Window_PossibleEnchant_All:SetShow(false)
end
function PaGlobal_PossibleEnchant_All:updateEnchantEquip()
  if nil == Panel_Window_PossibleEnchant_All then
    return
  end
  local selfPlayerWrapper = getSelfPlayer()
  if nil == selfPlayerWrapper then
    return
  end
  local selfPlayer = selfPlayerWrapper:get()
  local invenSize = selfPlayer:getInventorySlotCount(false)
  local listCount = -1
  self._ui.list2_title:getElementManager():clearKey()
  self._titleList = {}
  for idx = 0, invenSize - 1 do
    local itemWrapper = getInventoryItemByType(self._whereType, idx)
    if nil ~= itemWrapper and true == self:filterEnchantEquipItem(idx, itemWrapper) then
      listCount = listCount + 1
      self._titleList[listCount] = {}
      self._titleList[listCount].whereType = self._whereType
      self._titleList[listCount].slotNo = idx
      self._ui.list2_title:getElementManager():pushKey(toInt64(0, listCount))
    end
  end
  local cashInventory = selfPlayer:getInventory(self._cashWhereType)
  if nil == cashInventory then
    return
  end
  local cashInvenMaxSize = cashInventory:sizeXXX()
  for idx = 0, cashInvenMaxSize - 1 do
    local itemWrapper = getInventoryItemByType(self._cashWhereType, idx)
    if nil ~= itemWrapper and true == self:filterEnchantEquipItem(idx, itemWrapper) then
      listCount = listCount + 1
      self._titleList[listCount] = {}
      self._titleList[listCount].whereType = self._cashWhereType
      self._titleList[listCount].slotNo = idx
      self._ui.list2_title:getElementManager():pushKey(toInt64(0, listCount))
    end
  end
end
function PaGlobal_PossibleEnchant_All:updateValksHowl()
  if nil == Panel_Window_PossibleEnchant_All then
    return
  end
  local selfPlayerWrapper = getSelfPlayer()
  if nil == selfPlayerWrapper then
    return
  end
  local selfPlayer = selfPlayerWrapper:get()
  local invenSize = selfPlayer:getInventorySlotCount(false)
  local listCount = -1
  self._ui.list2_title:getElementManager():clearKey()
  self._titleList = {}
  for idx = 0, invenSize - 1 do
    local itemWrapper = getInventoryItemByType(self._whereType, idx)
    if nil ~= itemWrapper then
      local itemKey = itemWrapper:getStaticStatus():get()._key:get()
      if true == self:filterValksHowlItem(idx, itemWrapper) then
        listCount = listCount + 1
        self._titleList[listCount] = {}
        self._titleList[listCount].whereType = self._whereType
        self._titleList[listCount].slotNo = idx
        self._ui.list2_title:getElementManager():pushKey(toInt64(0, listCount))
      end
    end
  end
  local cashInventory = selfPlayer:getInventory(self._cashWhereType)
  if nil == cashInventory then
    return
  end
  local cashInvenMaxSize = cashInventory:sizeXXX()
  for idx = 0, cashInvenMaxSize - 1 do
    local itemWrapper = getInventoryItemByType(self._cashWhereType, idx)
    if nil ~= itemWrapper then
      local itemKey = itemWrapper:getStaticStatus():get()._key:get()
      if 17643 == itemKey then
        listCount = listCount + 1
        self._titleList[listCount] = {}
        self._titleList[listCount].whereType = self._cashWhereType
        self._titleList[listCount].slotNo = idx
        self._ui.list2_title:getElementManager():pushKey(toInt64(0, listCount))
      end
    end
  end
end
function PaGlobal_PossibleEnchant_All:updateCaphrasEquip()
  if nil == Panel_Window_PossibleEnchant_All then
    return
  end
  local selfPlayerWrapper = getSelfPlayer()
  if nil == selfPlayerWrapper then
    return
  end
  local selfPlayer = selfPlayerWrapper:get()
  local invenSize = selfPlayer:getInventorySlotCount(false)
  local listCount = -1
  self._ui.list2_subtitle:getElementManager():clearKey()
  self._subtitleList = {}
  for idx = 0, invenSize - 1 do
    local itemWrapper = getInventoryItemByType(self._whereType, idx)
    if nil ~= itemWrapper and false == self:filterCaphrasEquipItem(idx, itemWrapper) then
      listCount = listCount + 1
      self._subtitleList[listCount] = {}
      self._subtitleList[listCount].whereType = self._whereType
      self._subtitleList[listCount].slotNo = idx
      self._ui.list2_subtitle:getElementManager():pushKey(toInt64(0, listCount))
    end
  end
end
function PaGlobal_PossibleEnchant_All:updateValksAdvise()
  if nil == Panel_Window_PossibleEnchant_All then
    return
  end
  local selfPlayerWrapper = getSelfPlayer()
  if nil == selfPlayerWrapper then
    return
  end
  local selfPlayer = selfPlayerWrapper:get()
  local cashInventory = selfPlayer:getInventory(self._cashWhereType)
  if nil == cashInventory then
    return
  end
  self._ui.list2_subtitle:getElementManager():clearKey()
  self._subtitleList = {}
  local listCount = -1
  local cashInvenMaxSize = cashInventory:sizeXXX()
  for idx = 0, cashInvenMaxSize - 1 do
    local itemWrapper = getInventoryItemByType(self._cashWhereType, idx)
    if nil ~= itemWrapper and true == self:filterValksAdviceItem(idx, itemWrapper) then
      listCount = listCount + 1
      self._subtitleList[listCount] = {}
      self._subtitleList[listCount].whereType = self._cashWhereType
      self._subtitleList[listCount].slotNo = idx
      self._ui.list2_subtitle:getElementManager():pushKey(toInt64(0, listCount))
    end
  end
end
function PaGlobal_PossibleEnchant_All:updateMaterialItem()
  if nil == Panel_Window_PossibleEnchant_All then
    return
  end
  local selfPlayerWrapper = getSelfPlayer()
  if nil == selfPlayerWrapper then
    return
  end
  local selfPlayer = selfPlayerWrapper:get()
  local invenSize = selfPlayer:getInventorySlotCount(false)
  local listCount = -1
  self._ui.list2_title:getElementManager():clearKey()
  self._titleList = {}
  for idx = 0, invenSize - 1 do
    local itemWrapper = getInventoryItemByType(self._whereType, idx)
    if nil ~= itemWrapper and false == self:filterMaterialItem(idx, itemWrapper, self._whereType) then
      listCount = listCount + 1
      self._titleList[listCount] = {}
      self._titleList[listCount].whereType = self._whereType
      self._titleList[listCount].slotNo = idx
      self._ui.list2_title:getElementManager():pushKey(toInt64(0, listCount))
    end
  end
end
function PaGlobal_PossibleEnchant_All:updateDarkPredationItem()
  if nil == Panel_Window_PossibleEnchant_All then
    return
  end
  local selfPlayerWrapper = getSelfPlayer()
  if nil == selfPlayerWrapper then
    return
  end
  local selfPlayer = selfPlayerWrapper:get()
  local invenSize = selfPlayer:getInventorySlotCount(false)
  local listCount = -1
  self._ui.list2_title:getElementManager():clearKey()
  self._titleList = {}
  for idx = 0, invenSize - 1 do
    local itemWrapper = getInventoryItemByType(self._whereType, idx)
    if nil ~= itemWrapper and true == self:filterDarkPredationItem(idx, self._whereType) then
      listCount = listCount + 1
      self._titleList[listCount] = {}
      self._titleList[listCount].whereType = self._whereType
      self._titleList[listCount].slotNo = idx
      self._ui.list2_title:getElementManager():pushKey(toInt64(0, listCount))
    end
  end
end
function PaGlobal_PossibleEnchant_All:updateBookExtractionItem()
  if nil == Panel_Window_PossibleEnchant_All then
    return
  end
  local selfPlayerWrapper = getSelfPlayer()
  if nil == selfPlayerWrapper then
    return
  end
  local selfPlayer = selfPlayerWrapper:get()
  local invenSize = selfPlayer:getInventorySlotCount(false)
  local listCount = -1
  self._ui.list2_title:getElementManager():clearKey()
  self._titleList = {}
  for idx = 0, invenSize - 1 do
    local itemWrapper = getInventoryItemByType(self._whereType, idx)
    if nil ~= itemWrapper and false == self:filterBookExtraction(idx, self._whereType) then
      listCount = listCount + 1
      self._titleList[listCount] = {}
      self._titleList[listCount].whereType = self._whereType
      self._titleList[listCount].slotNo = idx
      self._ui.list2_title:getElementManager():pushKey(toInt64(0, listCount))
    end
  end
end
function PaGlobal_PossibleEnchant_All:registEventHandler()
  if nil == Panel_Window_PossibleEnchant_All then
    return
  end
  self._ui.list2_title:registEvent(CppEnums.PAUIList2EventType.luaChangeContent, "PaGlobalFunc_PossibleEnchant_All_UpdateList")
  self._ui.list2_title:createChildContent(CppEnums.PAUIList2ElementManagerType.list)
  self._ui.list2_subtitle:registEvent(CppEnums.PAUIList2EventType.luaChangeContent, "PaGlobalFunc_PossibleEnchant_All_UpdateSubList")
  self._ui.list2_subtitle:createChildContent(CppEnums.PAUIList2ElementManagerType.list)
  self._ui.btn_close:addInputEvent("Mouse_LUp", "HandleEventLUp_SpritEnchant_All_ShowToggleValks()")
  registerEvent("FromClient_InventoryUpdate", "FromClient_PossibleEnchant_All_UpdateList")
end
function PaGlobal_PossibleEnchant_All:validate()
  if nil == Panel_Window_PossibleEnchant_All then
    return
  end
end
function PaGlobal_PossibleEnchant_All:resize()
  if nil == Panel_Window_PossibleEnchant_All then
    return
  end
  if nil ~= Panel_Window_StackExtraction_All then
    Panel_Window_PossibleEnchant_All:SetPosY(Panel_Window_StackExtraction_All:GetPosY())
    Panel_Window_PossibleEnchant_All:SetPosX(Panel_Window_StackExtraction_All:GetPosX() + Panel_Window_StackExtraction_All:GetSizeX() + 10)
  end
end
function PaGlobal_PossibleEnchant_All:filterEnchantEquipItem(slotNo, itemWrapper)
  if nil == itemWrapper then
    return false
  end
  if true == itemWrapper:checkToValksItem() then
    return false
  end
  if true == ToClient_Inventory_CheckItemLock(slotNo, whereType) then
    return false
  end
  return true == itemWrapper:checkToEnchantItem(false)
end
function PaGlobal_PossibleEnchant_All:filterCaphrasEquipItem(slotNo, itemWrapper)
  if nil == itemWrapper then
    return true
  end
  if ToClient_Inventory_CheckItemLock(slotNo, whereType) then
    return true
  end
  return false == itemWrapper:checkToUpgradeItem() and false == itemWrapper:isPossibleCronPromotion()
end
function PaGlobal_PossibleEnchant_All:filterValksHowlItem(slotNo, itemWrapper)
  if nil == itemWrapper then
    return false
  end
  if true == itemWrapper:checkToValksItem() then
    return true
  end
  if true == ToClient_Inventory_CheckItemLock(slotNo, whereType) then
    return false
  end
  return false
end
function PaGlobal_PossibleEnchant_All:filterValksAdviceItem(slotNo, itemWrapper)
  if nil == itemWrapper then
    return false
  end
  local itemKey = itemWrapper:getStaticStatus():get()._key:get()
  if 17900 == itemKey or 17643 == itemKey then
    return false
  end
  if true == itemWrapper:checkToValksItem() then
    return true
  end
  if true == ToClient_Inventory_CheckItemLock(slotNo, whereType) then
    return false
  end
  return false
end
function PaGlobal_PossibleEnchant_All:filterMaterialItem(slotNo, itemWrapper, whereType)
  if nil == itemWrapper or true == PaGlobalFunc_SpiritEnchant_All_IsDarkPredation() then
    return true
  end
  if itemWrapper:checkToValksItem() then
    return true
  end
  if CppEnums.ItemWhereType.eCashInventory == whereType then
    return true
  end
  local returnValue = true
  if slotNo == getEnchantInformation():ToClient_getNeedNewPerfectItemSlotNo() then
    returnValue = false
  elseif slotNo ~= getEnchantInformation():ToClient_getNeedItemSlotNo() then
    returnValue = true
  else
    returnValue = false
    if PaGlobalFunc_SpiritEnchant_All_GetTargetItem().slot.slotNo == slotNo and CppEnums.ItemWhereType.eInventory ~= whereType then
      returnValue = true
    end
  end
  if ToClient_Inventory_CheckItemLock(slotNo, whereType) then
    returnValue = true
  end
  return returnValue
end
function PaGlobal_PossibleEnchant_All:filterDarkPredationItem(slotNo, whereType)
  local returnValue = false
  if true == ToClient_Inventory_AvailFeedItem(whereType, slotNo) then
    returnValue = true
  else
    returnValue = false
  end
  return returnValue
end
function PaGlobal_PossibleEnchant_All:filterBookExtraction(slotNo, whereType)
  local itemWrapper = getInventoryItemByType(whereType, slotNo)
  local itemSSW = itemWrapper:getStaticStatus()
  local failCount = getSelfPlayer():get():getEnchantFailCount()
  if CppEnums.ContentsEventType.ContentsType_ConvertEnchantFailCountToItem == itemSSW:get():getContentsEventType() then
    local maxEnchantParam = itemSSW:getContentsEventParam1()
    return failCount > maxEnchantParam
  else
    return true
  end
end
