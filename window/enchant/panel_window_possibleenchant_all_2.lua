function PaGlbalFunc_PossibleEnchant_All_Open()
  PaGlobal_PossibleEnchant_All:prepareOpen()
end
function PaGlbalFunc_PossibleEnchant_All_Close()
  PaGlobal_PossibleEnchant_All:prepareClose()
end
function HandleEventLUp_SpritEnchant_All_ShowToggleValks()
  if PaGlobal_PossibleEnchant_All._LIST_TYPE.EUQIP == PaGlobal_PossibleEnchant_All._curlistType then
    PaGlobal_PossibleEnchant_All._prevlistType = PaGlobal_PossibleEnchant_All._LIST_TYPE.EUQIP
    PaGlobalFunc_PossibleEnchant_All_SetValksItem()
  elseif PaGlobal_PossibleEnchant_All._LIST_TYPE.MATERIAL == PaGlobal_PossibleEnchant_All._curlistType then
    PaGlobal_PossibleEnchant_All._prevlistType = PaGlobal_PossibleEnchant_All._LIST_TYPE.MATERIAL
    PaGlobalFunc_PossibleEnchant_All_SetValksItem()
  elseif PaGlobal_PossibleEnchant_All._LIST_TYPE.VALKS == PaGlobal_PossibleEnchant_All._curlistType then
    if PaGlobal_PossibleEnchant_All._LIST_TYPE.EQUIP == PaGlobal_PossibleEnchant_All._prevlistType then
      PaGlobalFunc_PossibleEnchant_All_SetEnchantEquip()
    elseif PaGlobal_PossibleEnchant_All._LIST_TYPE.MATERIAL == PaGlobal_PossibleEnchant_All._prevlistType then
      PaGlobalFunc_PossibleEnchant_All_SetMaterialItem()
    else
      PaGlobalFunc_PossibleEnchant_All_SetEnchantEquip()
    end
  end
end
function HandleEventLUp_PossibleEnchant_All_SelectItem(isMain, slotNo, inventoryType)
  if true == PaGlobal_SpiritEnchant_All._isAnimating then
    return
  end
  if nil == slotNo or nil == inventoryType then
    return
  end
  local itemWrapper = getInventoryItemByType(inventoryType, slotNo)
  if nil == itemWrapper then
    return
  end
  if true == itemWrapper:checkToValksItem() then
    inventoryUseItem(inventoryType, slotNo, 0, true)
    return
  end
  if true == PaGlobalFunc_SpiritEnchant_All_IsDarkPredation() then
    PaGlobalFunc_SpiritEnchant_All_SetItemDarkPredation(slotNo, itemWrapper, inventoryType)
    return
  end
  if true == isMain then
    if PaGlobal_PossibleEnchant_All._LIST_TYPE.EUQIP == PaGlobal_PossibleEnchant_All._curlistType then
      if true == PaGlobalFunc_SpiritEnchant_All_GetTargetItem().slot.empty then
        PaGlobalFunc_SpiritEnchant_All_SetItemEnchantTarget(slotNo, itemWrapper, inventoryType, nil, true)
      end
    elseif PaGlobal_PossibleEnchant_All._LIST_TYPE.MATERIAL == PaGlobal_PossibleEnchant_All._curlistType then
      PaGlobalFunc_SpiritEnchant_All_SetMaterialItem(slotNo, itemWrapper, inventoryType)
    elseif PaGlobal_PossibleEnchant_All._LIST_TYPE.DARK == PaGlobal_PossibleEnchant_All._curlistType then
      PaGlobalFunc_SpiritEnchant_All_SetItemDarkPredation(slotNo, itemWrapper, inventoryType)
    elseif PaGlobal_PossibleEnchant_All._LIST_TYPE.BOOK == PaGlobal_PossibleEnchant_All._curlistType then
      PaGlboalFunc_SpiritEnchant_All_SetItemBookExtraction(slotNo, itemWrapper, inventoryType)
    end
  elseif PaGlobal_PossibleEnchant_All._LIST_TYPE.EUQIP == PaGlobal_PossibleEnchant_All._curlistType then
    PaGlobalFunc_SpiritEnchant_All_SetItemCaphrasTarget(slotNo, itemWrapper, inventoryType)
  end
end
function HandleEventOnOut_PossibleEnchant_All_ItemTooltip(isShow, slotNo, inventoryType)
  if false == isShow then
    Panel_Tooltip_Item_hideTooltip()
    return
  end
  if nil == slotNo and nil == inventoryType then
    return
  end
  if nil == Panel_Window_PossibleEnchant_All then
    return
  end
  local itemWrapper = getInventoryItemByType(inventoryType, slotNo)
  if nil == itemWrapper then
    return
  end
  Panel_Tooltip_Item_Show(itemWrapper, Panel_Window_PossibleEnchant_All, false, true, nil)
end
function FromClient_PossibleEnchant_All_UpdateList()
  if false == PaGlobal_PossibleEnchant_All._initialize then
    return
  end
  if false == Panel_Window_PossibleEnchant_All:GetShow() then
    return
  end
  if PaGlobal_PossibleEnchant_All._LIST_TYPE.VALKS == PaGlobal_PossibleEnchant_All._curlistType then
    PaGlobal_PossibleEnchant_All:updateValksHowl()
    PaGlobal_PossibleEnchant_All:updateValksAdvise()
  end
end
function PaGlobalFunc_PossibleEnchant_All_SetEnchantEquip()
  PaGlobal_PossibleEnchant_All._curlistType = PaGlobal_PossibleEnchant_All._LIST_TYPE.EUQIP
  PaGlobalFunc_PossibleEnchant_All_SetControlSize(PaGlobal_PossibleEnchant_All._LIST_TYPE.EQUIP)
  PaGlobal_PossibleEnchant_All._ui.txt_title:SetText(PAGetString(Defines.StringSheet_GAME, "LUA_SPIRITENCHANT_ALL_LIST_NORMAL"))
  PaGlobal_PossibleEnchant_All._ui.txt_subTitle:SetText(PAGetString(Defines.StringSheet_GAME, "LUA_SPIRITENCHANT_ALL_LIST_CAPHRAS"))
  PaGlobal_PossibleEnchant_All._ui.btn_close:SetShow(false)
  PaGlobal_PossibleEnchant_All:updateEnchantEquip()
  PaGlobal_PossibleEnchant_All:updateCaphrasEquip()
end
function PaGlobalFunc_PossibleEnchant_All_SetValksItem()
  PaGlobal_PossibleEnchant_All._curlistType = PaGlobal_PossibleEnchant_All._LIST_TYPE.VALKS
  PaGlobalFunc_PossibleEnchant_All_SetControlSize(PaGlobal_PossibleEnchant_All._LIST_TYPE.VALKS)
  PaGlobal_PossibleEnchant_All._ui.txt_title:SetText(PAGetString(Defines.StringSheet_GAME, "LUA_SPIRITENCHANT_ALL_LIST_VALKSHOWL"))
  PaGlobal_PossibleEnchant_All._ui.txt_subTitle:SetText(PAGetString(Defines.StringSheet_GAME, "LUA_SPIRITENCHANT_ALL_LIST_VALKSADVISE"))
  PaGlobal_PossibleEnchant_All._ui.btn_close:SetShow(true)
  PaGlobal_PossibleEnchant_All:updateValksHowl()
  PaGlobal_PossibleEnchant_All:updateValksAdvise()
end
function PaGlobalFunc_PossibleEnchant_All_SetMaterialItem()
  PaGlobal_PossibleEnchant_All._curlistType = PaGlobal_PossibleEnchant_All._LIST_TYPE.MATERIAL
  PaGlobalFunc_PossibleEnchant_All_SetControlSize(PaGlobal_PossibleEnchant_All._LIST_TYPE.MATERIAL)
  PaGlobal_PossibleEnchant_All._ui.txt_title:SetText(PAGetString(Defines.StringSheet_GAME, "LUA_SPIRITENCHANT_ALL_LIST_MATERIAL"))
  PaGlobal_PossibleEnchant_All._ui.btn_close:SetShow(false)
  PaGlobal_PossibleEnchant_All:updateMaterialItem()
end
function PaGlobalFunc_PossibleEnchant_All_SetDarkPredationItem()
  PaGlobal_PossibleEnchant_All._curlistType = PaGlobal_PossibleEnchant_All._LIST_TYPE.DARK
  PaGlobalFunc_PossibleEnchant_All_SetControlSize(PaGlobal_PossibleEnchant_All._LIST_TYPE.DARK)
  PaGlobal_PossibleEnchant_All._ui.txt_title:SetText(PAGetString(Defines.StringSheet_GAME, "LUA_SPIRITENCHANT_ALL_LIST_DARKPREDATION"))
  PaGlobal_PossibleEnchant_All._ui.btn_close:SetShow(false)
  PaGlobal_PossibleEnchant_All:updateDarkPredationItem()
end
function PaGlobalFunc_PossibleEnchant_All_SetBookExtactionItem()
  PaGlobal_PossibleEnchant_All._curlistType = PaGlobal_PossibleEnchant_All._LIST_TYPE.BOOK
  PaGlobalFunc_PossibleEnchant_All_SetControlSize(PaGlobal_PossibleEnchant_All._LIST_TYPE.BOOK)
  PaGlobal_PossibleEnchant_All._ui.txt_title:SetText(PAGetString(Defines.StringSheet_GAME, "LUA_SPIRITENCHANT_ALL_LIST_BOOK"))
  PaGlobal_PossibleEnchant_All._ui.btn_close:SetShow(false)
  PaGlobal_PossibleEnchant_All:updateBookExtractionItem()
end
function PaGlobalFunc_PossibleEnchant_All_UpdateList(control, key)
  local btn_list = UI.getChildControl(control, "RadioButton_EnchantEquipItem")
  local stc_slotBg = UI.getChildControl(btn_list, "Static_ItemSlotBG")
  local stc_itemIcon = UI.getChildControl(stc_slotBg, "Static_ItemIcon")
  local txt_name = UI.getChildControl(stc_slotBg, "StaticText_ItemName")
  local txt_count = UI.getChildControl(btn_list, "StaticText_ItemCnt")
  local key_32 = Int64toInt32(key)
  local slotIdx = -1
  slotIdx = PaGlobal_PossibleEnchant_All._titleList[key_32].slotNo
  local itemWrapper, itemKey
  itemWrapper = getInventoryItemByType(PaGlobal_PossibleEnchant_All._titleList[key_32].whereType, slotIdx)
  if nil == itemWrapper then
    return
  end
  itemKey = itemWrapper:get():getKey():getItemKey()
  txt_name:SetText(itemWrapper:getStaticStatus():getName())
  btn_list:setNotImpactScrollEvent(true)
  txt_count:SetShow(false)
  local slot = {}
  SlotItem.reInclude(slot, "PossibleEnchant_All_Slot_", 0, stc_itemIcon, PaGlobal_PossibleEnchant_All._slotConfig)
  slot:clearItem()
  slot:setItem(itemWrapper)
  slot.icon:addInputEvent("Mouse_On", "HandleEventOnOut_PossibleEnchant_All_ItemTooltip(true," .. slotIdx .. "," .. PaGlobal_PossibleEnchant_All._titleList[key_32].whereType .. ")")
  slot.icon:addInputEvent("Mouse_Out", "HandleEventOnOut_PossibleEnchant_All_ItemTooltip(false," .. slotIdx .. "," .. PaGlobal_PossibleEnchant_All._titleList[key_32].whereType .. ")")
  btn_list:addInputEvent("Mouse_LUp", "HandleEventLUp_PossibleEnchant_All_SelectItem(true," .. slotIdx .. "," .. PaGlobal_PossibleEnchant_All._titleList[key_32].whereType .. ")")
end
function PaGlobalFunc_PossibleEnchant_All_UpdateSubList(control, key)
  local btn_list = UI.getChildControl(control, "RadioButton_CaphrasEnchantItem")
  local stc_slotBg = UI.getChildControl(btn_list, "Static_ItemSlotBG")
  local stc_itemIcon = UI.getChildControl(stc_slotBg, "Static_ItemIcon")
  local txt_name = UI.getChildControl(stc_slotBg, "StaticText_ItemName")
  local txt_count = UI.getChildControl(btn_list, "StaticText_ItemCnt")
  local key_32 = Int64toInt32(key)
  local slotIdx = -1
  slotIdx = PaGlobal_PossibleEnchant_All._subtitleList[key_32].slotNo
  local itemWrapper, itemKey
  itemWrapper = getInventoryItemByType(PaGlobal_PossibleEnchant_All._subtitleList[key_32].whereType, slotIdx)
  if nil == itemWrapper then
    return
  end
  itemKey = itemWrapper:get():getKey():getItemKey()
  txt_name:SetText(itemWrapper:getStaticStatus():getName())
  btn_list:setNotImpactScrollEvent(true)
  txt_count:SetShow(false)
  local slot = {}
  SlotItem.reInclude(slot, "PossibleEnchant_All_SubSlot_", 0, stc_itemIcon, PaGlobal_PossibleEnchant_All._slotConfig)
  slot:clearItem()
  slot:setItem(itemWrapper)
  slot.icon:addInputEvent("Mouse_On", "HandleEventOnOut_PossibleEnchant_All_ItemTooltip(true," .. slotIdx .. "," .. PaGlobal_PossibleEnchant_All._subtitleList[key_32].whereType .. ")")
  slot.icon:addInputEvent("Mouse_Out", "HandleEventOnOut_PossibleEnchant_All_ItemTooltip(false," .. slotIdx .. "," .. PaGlobal_PossibleEnchant_All._subtitleList[key_32].whereType .. ")")
  btn_list:addInputEvent("Mouse_LUp", "HandleEventLUp_PossibleEnchant_All_SelectItem(false," .. slotIdx .. "," .. PaGlobal_PossibleEnchant_All._subtitleList[key_32].whereType .. ")")
end
function PaGlobalFunc_PossibleEnchant_All_SetControlSize(listType)
  if nil == Panel_Window_PossibleEnchant_All then
    return
  end
  PaGlobal_PossibleEnchant_All._ui.list2_title:SetSize(PaGlobal_PossibleEnchant_All._ui.list2_title:GetSizeX(), PaGlobal_PossibleEnchant_All._controlSize[listType].titleListSizeY)
  PaGlobal_PossibleEnchant_All._ui.txt_subTitle:SetSpanSize(PaGlobal_PossibleEnchant_All._ui.txt_subTitle:GetSpanSize().x, PaGlobal_PossibleEnchant_All._controlSize[listType].subTitleSpanY)
  PaGlobal_PossibleEnchant_All._ui.txt_subTitle:ComputePos()
  PaGlobal_PossibleEnchant_All._ui.list2_subtitle:SetSize(PaGlobal_PossibleEnchant_All._ui.list2_subtitle:GetSizeX(), PaGlobal_PossibleEnchant_All._controlSize[listType].subListSizeY)
  PaGlobal_PossibleEnchant_All._ui.list2_subtitle:SetSpanSize(PaGlobal_PossibleEnchant_All._ui.list2_subtitle:GetSpanSize().x, PaGlobal_PossibleEnchant_All._controlSize[listType].subListSpanY)
  PaGlobal_PossibleEnchant_All._ui.list2_subtitle:ComputePos()
  PaGlobal_PossibleEnchant_All._ui.txt_subTitle:SetShow(PaGlobal_PossibleEnchant_All._controlSize[listType].isShowSubList)
  PaGlobal_PossibleEnchant_All._ui.list2_subtitle:SetShow(PaGlobal_PossibleEnchant_All._controlSize[listType].isShowSubList)
end
