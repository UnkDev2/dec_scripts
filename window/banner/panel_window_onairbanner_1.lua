function PaGlobal_OnAirBanner:initialize()
  if true == PaGlobal_OnAirBanner._initialize then
    return
  end
  PaGlobal_OnAirBanner._ui._stc_bannerArea = UI.getChildControl(Panel_Window_OnAirBanner, "Static_BannerArea")
  PaGlobal_OnAirBanner._ui._txt_close = UI.getChildControl(PaGlobal_OnAirBanner._ui._stc_bannerArea, "StaticText_Close")
  PaGlobal_OnAirBanner:resize()
  PaGlobal_OnAirBanner:registEventHandler()
  PaGlobal_OnAirBanner:validate()
  local temporaryWrapper = getTemporaryInformationWrapper()
  if nil ~= temporaryWrapper and temporaryWrapper:isOnAir() then
    PaGlobal_OnAirBanner:prepareOpen()
  end
  PaGlobal_OnAirBanner._initialize = true
end
function PaGlobal_OnAirBanner:registEventHandler()
  if nil == Panel_Window_OnAirBanner then
    return
  end
  PaGlobal_OnAirBanner._ui._stc_bannerArea:addInputEvent("Mouse_LUp", "HandleEventMLUp_OnAirBanner_connectURL()")
  PaGlobal_OnAirBanner._ui._txt_close:addInputEvent("Mouse_LUp", "HandleEventMLUp_OnAirBanner_notShowClose()")
  Panel_Window_OnAirBanner:RegisterShowEventFunc(true, "PaGloabl_OnAirBanner_ShowAni()")
  Panel_Window_OnAirBanner:RegisterShowEventFunc(false, "PaGloabl_OnAirBanner_HideAni()")
  registerEvent("FromClient_OnAir", "FromClient_OnAirBanner_ShowOnAirBanner")
end
function PaGlobal_OnAirBanner:prepareOpen()
  if nil == Panel_Window_OnAirBanner then
    return
  end
  if true == PaGlobal_OnAirBanner._isAnimation then
    return
  end
  if false == PaGlobal_OnAirBanner:isDayCheck() then
    return
  end
  PaGlobal_OnAirBanner:clearData()
  Panel_Window_OnAirBanner:RegisterUpdateFunc("PaGlobal_OnAirBanner_UpdatePerframe")
  PaGlobal_OnAirBanner:open()
end
function PaGlobal_OnAirBanner:open()
  if nil == Panel_Window_OnAirBanner then
    return
  end
  Panel_Window_OnAirBanner:SetShow(true, true)
end
function PaGlobal_OnAirBanner:prepareClose()
  if nil == Panel_Window_OnAirBanner then
    return
  end
  if false == Panel_Window_OnAirBanner:GetShow() then
    return
  end
  PaGlobal_OnAirBanner:clearData()
  Panel_Window_OnAirBanner:ClearUpdateLuaFunc()
  PaGlobal_OnAirBanner:close()
end
function PaGlobal_OnAirBanner:close()
  if nil == Panel_Window_OnAirBanner then
    return
  end
  Panel_Window_OnAirBanner:SetShow(false, true)
end
function PaGlobal_OnAirBanner:update()
  if nil == Panel_Window_OnAirBanner then
    return
  end
end
function PaGlobal_OnAirBanner:validate()
  if nil == Panel_Window_OnAirBanner then
    return
  end
  PaGlobal_OnAirBanner._ui._stc_bannerArea:isValidate()
  PaGlobal_OnAirBanner._ui._txt_close:isValidate()
end
function PaGlobal_OnAirBanner:resize()
  Panel_Window_OnAirBanner:SetPosXY(getScreenSizeX() - Panel_Window_OnAirBanner:GetSizeX() - 10, getScreenSizeY())
end
function PaGlobal_OnAirBanner:isDayCheck()
  local _year = ToClient_GetThisYear()
  local _month = ToClient_GetThisMonth()
  local _day = ToClient_GetToday()
  local dayCheck = ToClient_getGameUIManagerWrapper():getLuaCacheDataListTime(__eOnAirBanner)
  if nil ~= dayCheck then
    local savedYear = dayCheck:GetYear()
    local savedMonth = dayCheck:GetMonth()
    local savedDay = dayCheck:GetDay()
    if _year == savedYear and _month == savedMonth and _day == savedDay then
      return false
    end
  end
  return true
end
function PaGlobal_OnAirBanner:saveNoShowToDay()
  local _year = ToClient_GetThisYear()
  local _month = ToClient_GetThisMonth()
  local _day = ToClient_GetToday()
  ToClient_getGameUIManagerWrapper():setLuaCacheDataListTime(__eOnAirBanner, _year, _month, _day, 0, 0, 0, CppEnums.VariableStorageType.eVariableStorageType_User)
end
function PaGlobal_OnAirBanner:clearData()
  PaGlobal_OnAirBanner._updateTime = 0
  PaGlobal_OnAirBanner._isAnimation = false
end
function PaGlobal_OnAirBanner:updatePerFrame(deltaTime)
  PaGlobal_OnAirBanner._isAnimation = true
  PaGlobal_OnAirBanner._updateTime = PaGlobal_OnAirBanner._updateTime + deltaTime
  if PaGlobal_OnAirBanner._endTime < PaGlobal_OnAirBanner._updateTime then
    PaGlobal_OnAirBanner:prepareClose()
  end
end
