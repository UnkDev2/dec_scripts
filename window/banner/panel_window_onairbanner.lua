PaGlobal_OnAirBanner = {
  _ui = {_stc_bannerArea = nil, _txt_close = nil},
  _updateTime = 0,
  _endTime = 8,
  _isAnimation = false,
  _initialize = false
}
runLua("UI_Data/Script/Window/Banner/Panel_Window_OnAirBanner_1.lua")
runLua("UI_Data/Script/Window/Banner/Panel_Window_OnAirBanner_2.lua")
registerEvent("FromClient_luaLoadComplete", "FromClient_OnAirBannerInit")
function FromClient_OnAirBannerInit()
  PaGlobal_OnAirBanner:initialize()
end
