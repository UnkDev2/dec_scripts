function HandleEventMLUp_OnAirBanner_connectURL()
  ToClient_OpenChargeWebPage("https://www.twitch.tv/blackdesertkr")
  PaGlobal_OnAirBanner:prepareClose()
end
function HandleEventMLUp_OnAirBanner_notShowClose()
  PaGlobal_OnAirBanner:saveNoShowToDay()
  PaGlobal_OnAirBanner:prepareClose()
end
function FromClient_OnAirBanner_ShowOnAirBanner(isOnAir)
  if true == isOnAir then
    PaGlobal_OnAirBanner:prepareOpen()
  elseif true == PaGlobal_OnAirBanner._isAnimation then
    PaGlobal_OnAirBanner:prepareClose()
  end
end
function PaGloabl_OnAirBanner_ShowAni()
  if nil == Panel_Window_OnAirBanner then
    return
  end
  local ImageMoveAni = Panel_Window_OnAirBanner:addMoveAnimation(2, 2.5, CppEnums.PAUI_ANIM_ADVANCE_TYPE.PAUI_ANIM_ADVANCE_SIN_HALF_PI)
  ImageMoveAni:SetStartPosition(getScreenSizeX() - Panel_Window_OnAirBanner:GetSizeX() - 10, getScreenSizeY() - 10)
  ImageMoveAni:SetEndPosition(getScreenSizeX() - Panel_Window_OnAirBanner:GetSizeX() - 10, getScreenSizeY() - Panel_Window_OnAirBanner:GetSizeY() - 10)
  ImageMoveAni.IsChangeChild = true
  Panel_Window_OnAirBanner:CalcUIAniPos(ImageMoveAni)
  ImageMoveAni:SetDisableWhileAni(true)
end
function PaGloabl_OnAirBanner_HideAni()
  if nil == Panel_Window_OnAirBanner then
    return
  end
  local ImageMoveAni = Panel_Window_OnAirBanner:addMoveAnimation(0, 0.3, CppEnums.PAUI_ANIM_ADVANCE_TYPE.PAUI_ANIM_ADVANCE_SIN_HALF_PI)
  ImageMoveAni:SetStartPosition(getScreenSizeX() - Panel_Window_OnAirBanner:GetSizeX() - 10, getScreenSizeY() - Panel_Window_OnAirBanner:GetSizeY() - 10)
  ImageMoveAni:SetEndPosition(getScreenSizeX() - Panel_Window_OnAirBanner:GetSizeX() + 10, getScreenSizeY() + 10)
  ImageMoveAni.IsChangeChild = true
  Panel_Window_OnAirBanner:CalcUIAniPos(ImageMoveAni)
  ImageMoveAni:SetDisableWhileAni(true)
  ImageMoveAni:SetHideAtEnd(false)
end
function PaGlobal_OnAirBanner_UpdatePerframe(deltaTime)
  PaGlobal_OnAirBanner:updatePerFrame(deltaTime)
end
