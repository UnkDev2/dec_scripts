if true == ToClient_IsDevelopment() then
  registerEvent("FromClient_HorseRacing_JoinAck", "FromClient_HorseRacing_JoinNotice")
  registerEvent("FromClient_HorseRacing_UpdateAck", "FromClient_HorseRacing_UpdateNotice")
end
function FromClient_HorseRacing_JoinNotice(playerCount)
  if false == ToClient_IsDevelopment() then
    return
  end
  Proc_ShowMessage_Ack("2\235\170\133\236\157\180 \235\170\168\236\157\180\235\169\180 \234\178\189\234\184\176\234\176\128 \236\139\156\236\158\145\235\144\169\235\139\136\235\139\164.")
  Proc_ShowMessage_Ack(PAGetString(Defines.StringSheet_RESOURCE, "PANEL_HORSERACING_WAITINGCOUNT") .. " " .. tostring(playerCount) .. "/2")
end
function FromClient_HorseRacing_UpdateNotice(state)
  if false == ToClient_IsDevelopment() or nil == state then
    return
  end
  if false == ToClient_IsInstanceFieldPlayerbyContentsType(__eInstanceContentsType_HorseRacing) then
    return
  end
  if __eHorseRacing_Loading == state then
    Proc_ShowMessage_Ack(PAGetString(Defines.StringSheet_GAME, "LUA_PARTYCOMBAT_MATCHINFO_RACESTARTWAITING"))
  elseif __eeHorseRacing_Play == state then
    Proc_ShowMessage_Ack(PAGetString(Defines.StringSheet_RESOURCE, "PANEL_HORSERACING_NAKMSG_START"))
  elseif __eHorseRacing_GoalLinePass == state then
    Proc_ShowMessage_Ack(PAGetStringParam1(Defines.StringSheet_GAME, "LUA_RACEMATCH_RACENOTIFYPASS_SUB", "nodeName", "\234\178\176\236\138\185\236\132\160"))
    Proc_ShowMessage_Ack(PAGetStringParam1(Defines.StringSheet_GAME, "LUA_RACEMATCH_FINISH_TIME_COUNT", "finishTime", 10))
  elseif __eHorseRacing_Result == state then
    Proc_ShowMessage_Ack(PAGetString(Defines.StringSheet_RESOURCE, "LUA_LOCALWAR_FINISH_SOON_SUB"))
  elseif __eHorseRacing_Exit == state then
    Proc_ShowMessage_Ack(PAGetString(Defines.StringSheet_RESOURCE, "PANEL_COMMON_EXIT"))
  end
end
