PaGlobal_Extraction_Caphras_All = {
  _ui = {
    stc_extractableItemSlot,
    stc_resultItmeSlot,
    stc_moneyArea,
    stc_moneyIcon,
    stc_money,
    btn_inven,
    stc_invenMoney,
    btn_warehouse,
    stc_warehouseMoney,
    stc_noticeDesc,
    btn_aniSkip,
    list2_extractableItem
  },
  _ui_pc = {btn_close, btn_extraction},
  _ui_console = {
    stc_bottom,
    btn_extraction,
    btn_select,
    btn_close
  },
  _initialize = false,
  _isConsole = false,
  _isSkipAni = false,
  _caphrasCnt = nil,
  _scrollIdx = nil,
  _isAniStart = false,
  _const_ani_time = 3,
  _delta_ani_time = 0,
  _equipCnt = 0,
  _equipNo = {},
  _itemInfo = {
    name = {},
    iconPath = {},
    slotNo = {},
    isExtractionEquip = {false}
  },
  _fromWhereType = -1,
  _fromSlotNo = -1,
  _moneyWhereType = 0,
  _savedCount = 0,
  _resultWhereType = -1,
  _resultSlotNo = -1,
  _fromSlotOn = false,
  _resultSlotOn = false,
  _slotConfig = {
    createBorder = false,
    createCount = true,
    createCooltime = false,
    createCooltimeText = false,
    createCash = true,
    createEnchant = true,
    createQuickslotBagIcon = false
  },
  _preSelectKey = nil,
  _curSelectKey = nil,
  _listControl = {}
}
runLua("UI_Data/Script/Window/Extraction/Panel_Window_Extraction_Caphras_All_1.lua")
runLua("UI_Data/Script/Window/Extraction/Panel_Window_Extraction_Caphras_All_2.lua")
registerEvent("FromClient_luaLoadComplete", "FromClient_Extraction_Caphras_AllInit")
function FromClient_Extraction_Caphras_AllInit()
  PaGlobal_Extraction_Caphras_All:initialize()
end
