function PaGlobal_GuildMainServerSet:initialize()
  self._ui.stc_titleBg = UI.getChildControl(Panel_Guild_MainServerSet_All, "Static_TitleArea")
  self._ui.btn_close = UI.getChildControl(self._ui.stc_titleBg, "Button_Close")
  self._ui.stc_selectBg = UI.getChildControl(Panel_Guild_MainServerSet_All, "Static_SelectBG")
  self._ui.list2_serverList = UI.getChildControl(self._ui.stc_selectBg, "List2_MainServerList")
  self:registEventHandler()
end
function PaGlobal_GuildMainServerSet:registEventHandler()
  self._ui.btn_close:addInputEvent("Mouse_LUp", "PaGlobalFunc_GuildMainServerSet_Close()")
  self._ui.list2_serverList:registEvent(__ePAUIList2EventType_LuaChangeContent, "PaGlobalFunc_GuildMainServerSet_ListCreateControl")
  self._ui.list2_serverList:createChildContent(__ePAUIList2ElementManagerType_List)
end
function PaGlobal_GuildMainServerSet:prepareOpen()
  if nil == Panel_Guild_MainServerSet_All then
    return
  end
  if false == _ContentsGroup_GuildServerWar then
    return
  end
  self:update()
  self:open()
end
function PaGlobal_GuildMainServerSet:open()
  if nil == Panel_Guild_MainServerSet_All then
    return
  end
  Panel_Guild_MainServerSet_All:SetShow(true)
end
function PaGlobal_GuildMainServerSet:prepareClose()
  if nil == Panel_Guild_MainServerSet_All then
    return
  end
  self:close()
end
function PaGlobal_GuildMainServerSet:close()
  if nil == Panel_Guild_MainServerSet_All then
    return
  end
  Panel_Guild_MainServerSet_All:SetShow(false)
end
function PaGlobal_GuildMainServerSet:update()
  local normalServerGroupCount = Int64toInt32(ToClient_GetNormalServerGroupCount())
  local rowCount = math.ceil(normalServerGroupCount * 0.5)
  self._ui.list2_serverList:getElementManager():clearKey()
  for index = 0, rowCount - 1 do
    self._ui.list2_serverList:getElementManager():pushKey(toInt64(0, index))
  end
end
