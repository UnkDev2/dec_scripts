function PaGlobalFunc_GuildMainServerSet_Open()
  if nil == Panel_Guild_MainServerSet_All then
    return
  end
  PaGlobal_GuildMainServerSet:prepareOpen()
end
function PaGlobalFunc_GuildMainServerSet_Close()
  if nil == Panel_Guild_MainServerSet_All then
    return
  end
  PaGlobal_GuildMainServerSet:prepareClose()
end
function PaGlobalFunc_GuildMainServerSet_ListCreateControl(content, key)
  local index = Int64toInt32(key) * 2
  local leftButton = UI.getChildControl(content, "Button_MainServerList_Left")
  local rightButton = UI.getChildControl(content, "Button_MainServerList_Right")
  if nil == leftButton or nil == rightButton then
    return
  end
  local leftServerInfo = ToClient_GetNormalServerInfoByIndex(index)
  if nil == leftServerInfo then
    leftButton:SetShow(false)
    rightButton:SetShow(false)
    return
  end
  local channelName = getChannelName(leftServerInfo._worldNo, leftServerInfo._serverNo)
  local splitString = string.split(channelName, "-")
  local groupName = splitString[1]
  leftButton:SetTextMode(__eTextMode_LimitText)
  leftButton:SetText(groupName)
  leftButton:addInputEvent("Mouse_LUp", "HandleEventLUp_GuildMainServerSet_SetServer(" .. index .. ")")
  local rightServerInfo = ToClient_GetNormalServerInfoByIndex(index + 1)
  if nil == rightServerInfo then
    rightButton:SetShow(false)
    return
  end
  channelName = getChannelName(rightServerInfo._worldNo, rightServerInfo._serverNo)
  splitString = string.split(channelName, "-")
  groupName = splitString[1]
  rightButton:SetTextMode(__eTextMode_LimitText)
  rightButton:SetText(groupName)
  rightButton:addInputEvent("Mouse_LUp", "HandleEventLUp_GuildMainServerSet_SetServer(" .. index + 1 .. ")")
end
function HandleEventLUp_GuildMainServerSet_SetServer(index)
  local serverInfo = ToClient_GetNormalServerInfoByIndex(index)
  if nil == serverInfo then
    return
  end
  local function setGuildMainServer()
    ToClient_SetGuildMainServerGroup(serverInfo._serverGroupNo)
  end
  local channelName = getChannelName(serverInfo._worldNo, serverInfo._serverNo)
  local splitString = string.split(channelName, "-")
  local groupName = splitString[1]
  local titleString = PAGetString(Defines.StringSheet_RESOURCE, "PANEL_GUILDSERVERSET_SELECT_TITLE")
  local contentString = groupName .. "\236\156\188\235\161\156 \235\147\177\235\161\157\237\149\152\236\139\156\234\178\160\236\138\181\235\139\136\234\185\140?"
  local messageboxData = {
    title = titleString,
    content = contentString,
    functionYes = setGuildMainServer,
    functionNo = MessageBox_Empty_function,
    priority = CppEnums.PAUIMB_PRIORITY.PAUIMB_PRIORITY_LOW
  }
  MessageBox.showMessageBox(messageboxData)
end
