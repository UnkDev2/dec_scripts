PaGlobal_GuildMainServerSet = {
  _ui = {
    _stc_title = UI.getChildControl(Panel_Guild_MainServerSet, "Static_TitleArea"),
    _btn_close = nil,
    _btn_update = UI.getChildControl(Panel_Guild_MainServerSet, "Button_Update"),
    _stc_selectBg = UI.getChildControl(Panel_Guild_MainServerSet, "Static_SelectBG"),
    _list2_ServerList = nil,
    _txt_time = nil,
    _originPanelSize = 0,
    _originDescSize = 0
  },
  _serverInfoList = {},
  _selectIndex = -1
}
runLua("UI_Data/Script/Window/Guild/Panel_Window_GuildMainServerSet_1.lua")
registerEvent("FromClient_luaLoadComplete", "FromClient_GuildMainServerSetInit")
function FromClient_GuildMainServerSetInit()
  local self = PaGlobal_GuildMainServerSet
  self:init()
end
