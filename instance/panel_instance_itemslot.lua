local _panel = Panel_Instance_ItemSlot
local ItemSlot = {
  _ui = {},
  _maxSlot = 4,
  _inventroyType = -1,
  _inventroySize = -1,
  _itemData = {
    [723154] = {_slotNo = 0},
    [56140] = {_slotNo = 1},
    [56138] = {_slotNo = 2},
    [723160] = {_slotNo = 3},
    [723156] = {_slotNo = 4}
  },
  slotConfig = {
    createIcon = true,
    createBorder = false,
    createCount = true,
    createCooltime = true,
    createCooltimeText = true,
    createEnchant = true
  },
  _allowItemType = 2
}
function ItemSlot:initialize()
  self._ui.slot = {}
  for i = 0, self._maxSlot do
    local slotControl = UI.getChildControl(_panel, string.format("Static_Slot_%d", i))
    local slot = {}
    SlotItem.new(slot, string.format("slotItem_%d", i), i, slotControl, ItemSlot.slotConfig)
    slot:createChild()
    slot.keyTxt = UI.getChildControl(_panel, string.format("Static_keyTxt_%d", i))
    slot.icon:SetPosX(2)
    slot.icon:SetPosY(2)
    slot.icon:addInputEvent("Mouse_LUp", string.format("PaGlobalFunc_ItemSlot_MouseLUp(%d)", i))
    slot.icon:addInputEvent("Mouse_PressMove", string.format("PaGlobalFunc_ItemSlot_MousePressMove(%d)", i))
    slot.icon:SetEnableDragAndDrop(true)
    slot.icon:addInputEvent("Mouse_On", string.format("Panel_Tooltip_Item_Show_GeneralStatic(%d, \"ItemSlot\", true)", i))
    slot.icon:addInputEvent("Mouse_Out", string.format("Panel_Tooltip_Item_Show_GeneralStatic(%d, \"ItemSlot\", false)", i))
    Panel_Tooltip_Item_SetPosition(i, slot, "ItemSlot")
    self:initSlotData(slot)
    self._ui.slot[i] = slot
  end
  self._inventroyType = Inventory_GetCurrentInventoryType()
  self._inventroySize = getSelfPlayer():get():getInventorySlotCount(true) - 1
  self:registEventHandler()
end
function ItemSlot:registEventHandler()
  registerEvent("FromClient_InventoryUpdate", "FromClient_ItemSlot_UpdateInventory")
  _panel:RegisterUpdateFunc("FromClient_ItemSlot_UpdatePerFrame")
end
function ItemSlot:open()
  if _panel:GetShow() == true then
    self:close()
    return
  end
  _panel:SetShow(true)
end
function ItemSlot:close()
  _panel:SetShow(false)
end
function ItemSlot:updateInventory()
  self:clearItemAll()
  for i = 1, self._inventroySize do
    self:addItemWrapper(i)
  end
  self:updateSlot()
end
function ItemSlot:updateSlot()
  for i = 0, self._maxSlot do
    local slot = self._ui.slot[i]
    self:initSlotData(slot)
    slot:clearItem()
    self:ChangeSlotEnchantKeyColor(false, slot)
  end
  for key, value in pairs(self._itemData) do
    if 0 <= value._slotNo then
      self:setSlotItem(self._ui.slot[value._slotNo], value)
    end
  end
end
function ItemSlot:updateCoolTime(fDeltaTime)
  if fDeltaTime <= 0 then
    return
  end
  for i = 0, self._maxSlot do
    local slot = self._ui.slot[i]
    if 0 < slot._itemCount then
      local remainTime = 0
      local itemReuseTime = 0
      local realRemainTime = 0
      local initRemainTime = 0
      if CppEnums.TInventorySlotNoUndefined ~= slot.slotNo then
        remainTime = getItemCooltime(self._inventroyType, slot.slotNo)
        itemReuseTime = getItemReuseCycle(self._inventroyType, slot.slotNo) * 0.001
        realRemainTime = remainTime * itemReuseTime
        initRemainTime = realRemainTime - realRemainTime % 1 + 1
      end
      if remainTime > 0 then
        slot.cooltime:UpdateCoolTime(remainTime)
        slot.cooltime:SetShow(true)
        slot.cooltimeText:SetText(Time_Formatting_ShowTop(initRemainTime))
        if itemReuseTime >= initRemainTime then
          slot.cooltimeText:SetShow(true)
        else
          slot.cooltimeText:SetShow(false)
        end
      elseif slot.cooltime:GetShow() then
        slot.cooltime:SetShow(false)
        slot.cooltimeText:SetShow(false)
        audioPostEvent_SystemUi(2, 1)
      end
    end
  end
end
function ItemSlot:addItemWrapper(index)
  local itemWrapper = getInventoryItemByType(self._inventroyType, index)
  if nil == itemWrapper then
    return
  end
  local itemSSW = itemWrapper:getStaticStatus()
  local itemType = itemSSW:getItemType()
  if self._allowItemType ~= itemType then
    return
  end
  local itemKey = itemWrapper:get():getKey():getItemKey()
  local item = self._itemData[itemKey]
  if nil == item then
    item._count = 0
    item._slotNo = -1
  end
  item._count = item._count + Int64toInt32(itemWrapper:get():getCount_s64())
  item.inventoryIdx = index
end
function ItemSlot:itemUse(idx)
  local slot = self._ui.slot[idx]
  local _itemCount = slot._itemCount
  if _itemCount > 0 then
    if restrictedActionForUseItem() then
      Proc_ShowMessage_Ack(PAGetString(Defines.StringSheet_GAME, "LUA_INVENTORY_TEXT_CANT_USEITEM"))
      return
    end
    if false == slot.cooltime:GetShow() then
      audioPostEvent_SystemUi(8, 2)
      _AudioPostEvent_SystemUiForXBOX(8, 2)
      slot.icon:AddEffect("fUI_SkillButton01", false, 0, 0)
      slot.icon:AddEffect("UI_SkillButton01", false, 0, 0)
    end
    inventoryUseItem(self._inventroyType, slot.slotNo, nil, true)
  end
end
function ItemSlot:setSlotItem(slot, item)
  local itemWrapper = getInventoryItemByType(self._inventroyType, item.inventoryIdx)
  if nil == itemWrapper then
    local itemSSW = getItemEnchantStaticStatus(ItemEnchantKey(item.itemKey))
    if nil == itemSSW then
      ASSERT(false)
      return
    end
    slot:setItemByStaticStatus(itemSSW)
    slot.icon:SetMonoTone(true)
    self:initSlotData(slot)
  else
    slot:setItem(itemWrapper)
    slot.slotNo = item.inventoryIdx
    slot._itemCount = item._count
    slot.icon:SetMonoTone(false)
    self:ChangeSlotEnchantKeyColor(true, slot)
  end
  slot._item = item.itemKey
  slot.count:SetShow(true)
  slot.count:SetText(tostring(item._count))
end
function ItemSlot:clearItemAll()
  for key, value in pairs(self._itemData) do
    value.itemKey = key
    value._count = 0
    value.inventoryIdx = -1
  end
end
function ItemSlot:initSlotData(slot)
  slot.slotNo = -1
  slot._itemCount = 0
  slot._item = nil
end
function ItemSlot:ChangeSlotEnchantKeyColor(isActive, slot)
  if true == isActive then
    slot.keyTxt:SetFontColor(Defines.Color.C_FFFFFFFF)
  else
    slot.keyTxt:SetFontColor(Defines.Color.C_FF888888)
  end
end
function ItemSlot:mousePressMove(slotIndex)
  local slot = self._ui.slot[slotIndex]
  local itemSSW = getItemEnchantStaticStatus(ItemEnchantKey(slot._item))
  if nil == itemSSW then
    return
  end
  DragManager:setDragInfo(_panel, nil, slotIndex, string.format("Icon/%s", itemSSW:getIconPath()), PaGlobalFunc_ItemSlot_GroudClick, nil)
end
function ItemSlot:mouseLUp(slotIndex)
  if nil == DragManager.dragStartPanel then
    self:itemUse(slotIndex)
    return
  end
  if DragManager.dragStartPanel == Panel_Window_Inventory then
    self:dragInventoryItemSlot(slotIndex, DragManager.dragSlotInfo)
  elseif DragManager.dragStartPanel == _panel then
    self:dragChangeSlotItem(slotIndex, DragManager.dragSlotInfo)
  end
  audioPostEvent_SystemUi(0, 8)
  _AudioPostEvent_SystemUiForXBOX(0, 8)
  DragManager:clearInfo()
end
function ItemSlot:dragInventoryItemSlot(slotIndex, inventoryIdx)
  local itemWrapper = getInventoryItemByType(self._inventroyType, inventoryIdx)
  if nil == itemWrapper then
    return
  end
  local itemSSW = itemWrapper:getStaticStatus()
  local itemType = itemSSW:getItemType()
  if self._allowItemType ~= itemType then
    return
  end
  local findData = self:findItemDataSlotNo(slotIndex)
  local itemKey = itemWrapper:get():getKey():getItemKey()
  if nil ~= findData then
    findData._slotNo = -1
  end
  self._itemData[itemKey]._slotNo = slotIndex
  self:updateSlot()
end
function ItemSlot:dragChangeSlotItem(slotIndex, dragIndex)
  local current = self:findItemDataSlotNo(slotIndex)
  local moveData = self:findItemDataSlotNo(dragIndex)
  if nil == current then
    moveData._slotNo = slotIndex
  else
    local tempNo = current._slotNo
    current._slotNo = moveData._slotNo
    moveData._slotNo = tempNo
  end
  self:updateSlot()
end
function ItemSlot:findItemDataSlotNo(slotNo)
  for key, value in pairs(self._itemData) do
    if value._slotNo == slotNo then
      return value
    end
  end
  return nil
end
function FromClient_ItemSlot_Init()
  local self = ItemSlot
  self:initialize()
  self:open()
end
function PaGlobalFunc_ItemSlot_Open()
  local self = ItemSlot
  self:open()
end
function PaGlobalFunc_ItemSlot_Close()
  local self = ItemSlot
  self:close()
end
function FromClient_ItemSlot_UpdateInventory()
  local self = ItemSlot
  self:updateInventory()
end
function PaGlobalFunc_ItemSlot_Use(idx)
  local self = ItemSlot
  self:itemUse(idx)
end
function FromClient_ItemSlot_UpdatePerFrame(fDeltaTime)
  local self = ItemSlot
  self:updateCoolTime(fDeltaTime)
end
function PaGlobalFunc_ItemSlot_MousePressMove(slotIndex)
  local self = ItemSlot
  self:mousePressMove(slotIndex)
end
function PaGlobalFunc_ItemSlot_MouseLUp(slotIndex)
  local self = ItemSlot
  self:mouseLUp(slotIndex)
end
function PaGlobalFunc_ItemSlot_GroudClick(whereType, slotIndex)
end
registerEvent("FromClient_luaLoadComplete", "FromClient_ItemSlot_Init")
