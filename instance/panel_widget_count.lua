Panel_Widget_Count:SetShow(false)
local PaGlobal_TimeCount = {
  _ui = {
    _leftTimeBg = UI.getChildControl(Panel_Widget_Count, "StaticText_LeftTimeBg"),
    _leftCountBg = UI.getChildControl(Panel_Widget_Count, "StaticText_LeftCountBg"),
    _desc = UI.getChildControl(Panel_Widget_Count, "StaticText_Desc"),
    _leftTime = nil,
    _leftCount = nil,
    _leftCountTitle = nil
  },
  _maxMemberCount = 50,
  _playTime = 0,
  _onceCheck = false
}
PaGlobal_TimeCount._ui._leftTime = UI.getChildControl(PaGlobal_TimeCount._ui._leftTimeBg, "StaticText_LeftTimeValue")
PaGlobal_TimeCount._ui._leftCount = UI.getChildControl(PaGlobal_TimeCount._ui._leftCountBg, "StaticText_LeftCountValue")
PaGlobal_TimeCount._ui._leftCountTitle = UI.getChildControl(PaGlobal_TimeCount._ui._leftCountBg, "StaticText_CountIcon")
function FromClient_ChangeBattleRoyalePlayerCount()
  local self = PaGlobal_TimeCount
  local currentPlayerCount = ToClient_BattleRoyaleRemainPlayerCount()
  local maxPlayerCount = ToClient_BattleRoyaleTotalPlayerCount()
  local isGameStart = 0 < Int64toInt32(ToClient_BattleRoyaleRemainTime()) and currentPlayerCount > 0
  if isGameStart then
    self._ui._leftCountTitle:SetText(PAGetString(Defines.StringSheet_GAME, "LUA_BATTLEROYAL_SURVIVALMEMBER_TITLE"))
    self._ui._desc:SetShow(false)
    PaGlobal_TimeCount._ui._leftCount:SetText(PAGetStringParam2(Defines.StringSheet_GAME, "LUA_BATTLEROYAL_MEMBERCOUNT", "count1", currentPlayerCount, "count2", maxPlayerCount))
    if false == self._onceCheck then
      PaGlobal_BattleRoyal_PlayStart()
      self._onceCheck = true
    end
  else
    self._ui._leftCountTitle:SetText(PAGetString(Defines.StringSheet_GAME, "LUA_BATTLEROYAL_JOINMEMBER_TITLE"))
    self._ui._desc:SetShow(true)
    PaGlobal_TimeCount._ui._leftCount:SetText(PAGetStringParam2(Defines.StringSheet_GAME, "LUA_BATTLEROYAL_MEMBERCOUNT", "count1", maxPlayerCount, "count2", self._maxMemberCount))
  end
end
function PaGlobalFunc_TimeCheck(deltaTime)
  local self = PaGlobal_TimeCount
  self._playTime = self._playTime + deltaTime
  if self._playTime > 1 then
    local leftTime = Int64toInt32(ToClient_BattleRoyaleRemainTime())
    if leftTime > 0 then
      self._ui._leftTime:SetText(Util.Time.timeFormatting_Minute(Int64toInt32(ToClient_BattleRoyaleRemainTime())))
      self._playTime = 0
      self._ui._leftCountTitle:SetText(PAGetString(Defines.StringSheet_GAME, "LUA_BATTLEROYAL_SURVIVALMEMBER_TITLE"))
      self._ui._desc:SetShow(false)
    else
      self._ui._leftCountTitle:SetText(PAGetString(Defines.StringSheet_GAME, "LUA_BATTLEROYAL_JOINMEMBER_TITLE"))
      self._ui._desc:SetShow(true)
    end
  end
end
function PaGlobalFunc_TimerOpen()
  Panel_Widget_Count:SetShow(true)
  FromClient_ChangeBattleRoyalePlayerCount()
end
function PaGlobal_TimeCount:Init()
  registerEvent("FromClient_ChangeBattleRoyalePlayerCount", "FromClient_ChangeBattleRoyalePlayerCount")
  registerEvent("FromClient_luaLoadComplete", "PaGlobalFunc_TimerOpen")
end
PaGlobal_TimeCount:Init()
