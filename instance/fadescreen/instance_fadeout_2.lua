function PaGlobal_Instance_FadeOutShowAni()
  PaGlobal_Instance_FadeOut:showAni()
end
function PaGlobal_Instance_FadeOutHideAni()
  PaGlobal_Instance_FadeOut:hideAni()
end
function PaGlobal_Instance_FadeOutResize()
  PaGlobal_Instance_FadeOut:resize()
end
function PaGlobal_Instance_FadeOutOpen()
  PaGlobal_Instance_FadeOut:open()
end
function PaGlobal_Instance_FadeOutClose()
  PaGlobal_Instance_FadeOut:close()
end
