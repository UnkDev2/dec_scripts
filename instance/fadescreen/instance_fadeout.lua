PaGlobal_Instance_FadeOut = {
  _ui = {_loadingText = nil},
  _ANIMATION_TIME = 0.5,
  _initialize = false
}
runLua("UI_Data/Script/Instance/FadeScreen/Instance_FadeOut_1.lua")
runLua("UI_Data/Script/Instance/FadeScreen/Instance_FadeOut_2.lua")
registerEvent("FromClient_luaLoadComplete", "FromClient_Instance_FadeOutInit")
function FromClient_Instance_FadeOutInit()
  PaGlobal_Instance_FadeOut:initialize()
end
