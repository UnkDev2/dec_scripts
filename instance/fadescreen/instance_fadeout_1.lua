function PaGlobal_Instance_FadeOut:initialize()
  if true == PaGlobal_Instance_FadeOut._initialize then
    return
  end
  PaGlobal_Instance_FadeOut._ui._loadingText = UI.getChildControl(Instance_FadeOut, "StaticText_LoadingEffect")
  PaGlobal_Instance_FadeOut._ui._loadingText:EraseAllEffect()
  PaGlobal_Instance_FadeOut._ui._loadingText:AddEffect("UI_Loading_01A", true, 0, 0)
  PaGlobal_Instance_FadeOut:registEventHandler()
  PaGlobal_Instance_FadeOut:validate()
  PaGlobal_Instance_FadeOut._initialize = true
end
function PaGlobal_Instance_FadeOut:registEventHandler()
  Instance_FadeOut:RegisterShowEventFunc(true, "PaGlobal_Instance_FadeOutShowAni()")
  Instance_FadeOut:RegisterShowEventFunc(false, "PaGlobal_Instance_FadeOutHideAni()")
  registerEvent("onScreenResize", "PaGlobal_Instance_FadeOutResize")
  registerEvent("FromClient_BattleRoyaleLoading", "PaGlobal_Instance_FadeOutOpen")
  registerEvent("FromClient_CloseBattleRoyaleEnterLoading", "PaGlobal_Instance_FadeOutClose")
end
function PaGlobal_Instance_FadeOut:showAni()
  if nil == Instance_FadeOut then
    return
  end
  local showAni = Instance_FadeOut:addColorAnimation(0, PaGlobal_Instance_FadeOut._ANIMATION_TIME, CppEnums.PAUI_ANIM_ADVANCE_TYPE.PAUI_ANIM_ADVANCE_SIN_HALF_PI)
  showAni:SetStartColor(Defines.Color.C_00000000)
  showAni:SetEndColor(Defines.Color.C_FF000000)
  showAni:SetStartIntensity(3)
  showAni:SetEndIntensity(1)
  showAni.IsChangeChild = true
end
function PaGlobal_Instance_FadeOut:hideAni()
  if nil == Instance_FadeOut then
    return
  end
  local closeAni = Instance_FadeOut:addColorAnimation(0, PaGlobal_Instance_FadeOut._ANIMATION_TIME, CppEnums.PAUI_ANIM_ADVANCE_TYPE.PAUI_ANIM_ADVANCE_SIN_HALF_PI)
  closeAni:SetStartColor(Defines.Color.C_00000000)
  closeAni:SetEndColor(Defines.Color.C_FF000000)
  closeAni:SetStartIntensity(3)
  closeAni:SetEndIntensity(1)
  closeAni.IsChangeChild = true
  closeAni:SetHideAtEnd(true)
end
function PaGlobal_Instance_FadeOut:resize()
  if nil == Instance_FadeOut then
    return
  end
  if false == PaGlobal_Instance_FadeOut._initialize then
    return
  end
  Instance_FadeOut:SetSize(getScreenSizeX(), getScreenSizeY())
  PaGlobal_Instance_FadeOut._ui._loadingText:ComputePos()
  PaGlobal_Instance_FadeOut._ui._loadingText:SetPosX(getScreenSizeX() / 2 - (PaGlobal_Instance_FadeOut._ui._loadingText:GetTextSpan().x + PaGlobal_Instance_FadeOut._ui._loadingText:GetTextSizeX()) / 2)
end
function PaGlobal_Instance_FadeOut:open()
  if nil == Instance_FadeOut then
    return
  end
  PaGlobal_Instance_FadeOut:resize()
  Instance_FadeOut:SetShow(true, true)
end
function PaGlobal_Instance_FadeOut:close()
  if nil == Instance_FadeOut then
    return
  end
  if true == Instance_FadeOut:GetShow() then
    Instance_FadeOut:SetShow(false, true)
  end
end
function PaGlobal_Instance_FadeOut:validate()
  PaGlobal_Instance_FadeOut._ui._loadingText:isValidate()
end
