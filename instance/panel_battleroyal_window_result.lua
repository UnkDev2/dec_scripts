local result = {
  _panel = Panel_BattleRoyal_Window_Result,
  _ui = {
    rank = UI.getChildControl(Panel_BattleRoyal_Window_Result, "StaticText_Rank"),
    gamePlayTime = UI.getChildControl(Panel_BattleRoyal_Window_Result, "StaticText_GamePlayTimeValue"),
    surviveTime = UI.getChildControl(Panel_BattleRoyal_Window_Result, "StaticText_SurviveTimeValue"),
    damage = UI.getChildControl(Panel_BattleRoyal_Window_Result, "StaticText_DamageValue"),
    templateItemSlotBg = UI.getChildControl(Panel_BattleRoyal_Window_Result, "Static_ItemSlotBg"),
    btnLeave = UI.getChildControl(Panel_BattleRoyal_Window_Result, "Button_Leave")
  },
  _config = {
    createIcon = true,
    createBorder = true,
    createCount = true
  },
  _item = {},
  _prevHp = nil,
  _damage = 0,
  _playTime = 0,
  _surviveTime = 0,
  _isPlay = false,
  _isSurvive = false,
  _leftTime = 60
}
function result:init()
  self._panel:RegisterShowEventFunc(true, "BattleRoyal_Result_ShowAni()")
  self._panel:RegisterShowEventFunc(false, "BattleRoyal_Result_HideAni()")
  self._panel:SetSize(self._panel:GetSizeX(), getScreenSizeY())
  self._ui.btnLeave:ComputePos()
end
function result:open()
  self._panel:SetShow(true, true)
end
function result:close()
  self._panel:SetShow(false, false)
end
function result:allUIClose()
  Panel_Widget_Leave:SetShow(false)
  Panel_Instance_ItemSlot:SetShow(false)
  Panel_Widget_Count:SetShow(false)
  Panel_DeadMessage:SetShow(false)
  Panel_QuickSlot:SetShow(false)
  Panel_MainStatus_User_Bar:SetShow(false)
  Panel_ClassResource:SetShow(false)
end
function BattleRoyal_Result_ShowAni()
  local panel = result._panel
  local ImageMoveAni = panel:addMoveAnimation(0, 0.5, CppEnums.PAUI_ANIM_ADVANCE_TYPE.PAUI_ANIM_ADVANCE_SIN_HALF_PI)
  ImageMoveAni:SetStartPosition(getScreenSizeX(), 0)
  ImageMoveAni:SetEndPosition(getScreenSizeX() - panel:GetSizeX() + 8, 0)
  ImageMoveAni.IsChangeChild = true
  panel:CalcUIAniPos(ImageMoveAni)
  ImageMoveAni:SetDisableWhileAni(true)
  ImageMoveAni:SetIgnoreUpdateSnapping(true)
end
function BattleRoyal_Result_HideAni()
end
function FromClient_BattleRoyal_AddItem(itemEnchantKey, slotNo, itemCount)
  local self = result
  if nil == self._item[itemEnchantKey] then
    self._item[itemEnchantKey] = Int64toInt32(itemCount)
  else
    self._item[itemEnchantKey] = self._item[itemEnchantKey] + Int64toInt32(itemCount)
  end
end
function FromClient_BattleRoyal_AddDamage()
  local self = result
  local playerWrapper = getSelfPlayer()
  if nil == playerWrapper then
    return
  end
  local currentHp = math.floor(playerWrapper:get():getHp())
  if nil == self._prevHp then
    self._prevHp = currentHp
  end
  if currentHp < self._prevHp then
    self._damage = self._damage + self._prevHp - currentHp
  end
  self._prevHp = currentHp
end
function PaGlobal_BattleRoyal_PlayStart()
  local self = result
  self._isPlay = true
  self._isSurvive = true
end
function FromClient_BattleRoyal_Dead()
  local self = result
  self._isSurvive = false
end
function FromClient_BattleRoyal_PlayEnd(isWinner)
  local self = result
  self._isPlay = false
  local rankString = ""
  if isWinner then
    rankString = PAGetString(Defines.StringSheet_GAME, "LUA_BATTLEROYAL_RESULT_FIRSTRANK")
  elseif nil == PaGlobal_BattleRoyal_MyRank() then
    rankString = PAGetString(Defines.StringSheet_GAME, "LUA_BATTLEROYAL_RESULT_SECOND")
  else
    rankString = PAGetStringParam1(Defines.StringSheet_GAME, "LUA_BATTLEROYAL_RESULT_RANK", "rank", tostring(PaGlobal_BattleRoyal_MyRank()))
  end
  self._ui.rank:SetText(rankString)
  local playTimeString = PAGetStringParam2(Defines.StringSheet_GAME, "LUA_BATTLEROYAL_RESULT_TIME", "minute", math.floor(self._playTime / 60), "second", math.floor(self._playTime % 60))
  local surviveTimeString = PAGetStringParam2(Defines.StringSheet_GAME, "LUA_BATTLEROYAL_RESULT_TIME", "minute", math.floor(self._surviveTime / 60), "second", math.floor(self._surviveTime % 60))
  self._ui.gamePlayTime:SetText(playTimeString)
  self._ui.surviveTime:SetText(surviveTimeString)
  self._ui.damage:SetText(makeDotMoney(toInt64(0, self._damage)))
  local slotCount = 0
  for index, itemKey in pairs(self._item) do
    local itemSSW = getItemEnchantStaticStatus(ItemEnchantKey(index))
    slotCount = slotCount + 1
    local slotBg = UI.createAndCopyBasePropertyControl(self._panel, "Static_ItemSlotBg", self._panel, "Static_ItemSlotBg_" .. slotCount)
    slotBg:SetPosX(50 + (slotCount - 1) % 7 * 60)
    slotBg:SetPosY(340 + math.floor((slotCount - 1) / 7) * 60)
    local slot = {}
    SlotItem.new(slot, string.format("slotItem_%d", slotCount), slotCount, slotBg, self._config)
    slot:createChild()
    slot:setItemByStaticStatus(itemSSW, toInt64(0, self._item[index]))
  end
  self:allUIClose()
  self:open()
end
local tempTime1, tempTime2 = 0, 0
function PaGlobal_PlayTimeCheck(deltaTime)
  local self = result
  if self._isPlay then
    tempTime1 = tempTime1 + deltaTime
    if tempTime1 > 1 then
      self._playTime = self._playTime + tempTime1
      tempTime1 = 0
    end
  end
end
function PaGlobal_SurviveTimeCheck(deltaTime)
  local self = result
  if self._isSurvive then
    tempTime2 = tempTime2 + deltaTime
    if tempTime2 > 1 then
      self._surviveTime = self._surviveTime + tempTime2
      tempTime2 = 0
    end
  end
end
function result:registEventHandler()
  registerEvent("EventAddItemToInventory", "FromClient_BattleRoyal_AddItem")
  registerEvent("FromClient_SelfPlayerHpChanged", "FromClient_BattleRoyal_AddDamage")
  registerEvent("EventSelfPlayerDead", "FromClient_BattleRoyal_Dead")
  registerEvent("FromClient_BattleRoyaleWinner", "FromClient_BattleRoyal_PlayEnd")
  self._ui.btnLeave:addInputEvent("Mouse_LUp", "ToClient_ExitBattleRoyale()")
end
function FromClient_BattleRoyal_Init()
  local self = result
  self:init()
  self:registEventHandler()
end
registerEvent("FromClient_luaLoadComplete", "FromClient_BattleRoyal_Init")
