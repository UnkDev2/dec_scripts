Panel_Widget_Leave:SetShow(false)
function PaGlobal_Leave_Init()
  Panel_Widget_Leave:SetShow(true)
  local buttonLeave = UI.getChildControl(Panel_Widget_Leave, "Button_Leave")
  buttonLeave:addInputEvent("Mouse_LUp", "PaGlobal_Leave_Out()")
end
function PaGlobal_Leave_Out()
  ToClient_ExitBattleRoyale()
end
registerEvent("FromClient_luaLoadComplete", "PaGlobal_Leave_Init")
