function HandleEventMouseRup_Edana_closeSkillSelectWindow()
  if nil == Panel_Window_EdanaContract_SkillSelect then
    return
  end
  local self = PaGlobal_Window_Edana_SkillSelect
  self:prepareClose()
end
function HandleEventMouseRup_Edana_applyStat(statIndex)
  if nil == Panel_Window_EdanaContract_SkillSelect then
    return
  end
  if true == ToClient_IsSetEdanaStat(statIndex) then
    Proc_ShowMessage_Ack(PAGetString(Defines.StringSheet_GAME, "LUA_EDANA_ALREADY_APPLY_SKILL"))
    return
  end
  ToClient_RequestVaryEdanaStat(statIndex)
end
function HandleEventChangeListContent_SkillListCreate(list_content, key)
  local skillType = Int64toInt32(key)
  local skillIconBg = UI.getChildControl(list_content, "Static_SKillIConBg")
  local skillIcon = UI.getChildControl(skillIconBg, "Static_SkillIcon")
  local applyButton = UI.getChildControl(list_content, "Button_Apply")
  local skillName = UI.getChildControl(list_content, "StaticText_SkillName")
  local skillDescription = UI.getChildControl(list_content, "StaticText_SkillDesc")
  local edanaStatWrapper = ToClient_GetContractOfEdanaStatWrapper(skillType)
  skillName:SetText(edanaStatWrapper:getStatName())
  skillDescription:SetText(edanaStatWrapper:getStatDescription())
  local iconBackgroundFile = edanaStatWrapper:getIconBackgroundPath()
  local leftTopUV = edanaStatWrapper:getBackgroundLeftTopUV()
  local rightBottomUV = edanaStatWrapper:getBackgroundRightBottomUV()
  skillIconBg:ChangeTextureInfoName(iconBackgroundFile)
  local x1, y1, x2, y2 = setTextureUV_Func(skillIconBg, leftTopUV.x, leftTopUV.y, rightBottomUV.x, rightBottomUV.y)
  skillIconBg:getBaseTexture():setUV(x1, y1, x2, y2)
  skillIconBg:setRenderTexture(skillIconBg:getBaseTexture())
  skillIconBg:SetShow(true)
  local iconFile = edanaStatWrapper:getIconPath()
  skillIcon:ChangeTextureInfoName("Icon/" .. iconFile)
  skillIcon:setRenderTexture(skillIcon:getBaseTexture())
  skillIcon:SetShow(true)
  applyButton:addInputEvent("Mouse_LUp", "HandleEventMouseRup_Edana_applyStat(" .. skillType .. ")")
end
function FromClient_UpdateEdanaSkillWindow()
  if nil == Panel_Window_EdanaContract_SkillSelect then
    return
  end
  local self = PaGlobal_Window_Edana_SkillSelect
  self:updateSkillList()
end
