function PaGlobal_Window_Edana_SkillSelect:initialize()
  if true == self._initialize then
    return
  end
  local Static_Title_BG = UI.getChildControl(Panel_Window_EdanaContract_SkillSelect, "Static_Title_BG")
  self._ui._closeButton = UI.getChildControl(Static_Title_BG, "Button_Win_Close")
  self._ui._skillList = UI.getChildControl(Panel_Window_EdanaContract_SkillSelect, "List2_SkillSelect")
  self:registEventHandler()
  self:validate()
  self._initialize = true
end
function PaGlobal_Window_Edana_SkillSelect:registEventHandler()
  if nil == Panel_Window_EdanaContract_SkillSelect then
    return
  end
  self._ui._closeButton:addInputEvent("Mouse_LUp", "HandleEventMouseRup_Edana_closeSkillSelectWindow()")
  self._ui._skillList:registEvent(CppEnums.PAUIList2EventType.luaChangeContent, "HandleEventChangeListContent_SkillListCreate")
  self._ui._skillList:createChildContent(CppEnums.PAUIList2ElementManagerType.list)
end
function PaGlobal_Window_Edana_SkillSelect:prepareOpen()
  if nil == Panel_Window_EdanaContract_SkillSelect then
    return
  end
  self:updateSkillList()
  self:open()
  self._isOpen = true
end
function PaGlobal_Window_Edana_SkillSelect:open()
  if nil == Panel_Window_EdanaContract_SkillSelect then
    return
  end
  Panel_Window_EdanaContract_SkillSelect:SetShow(true)
end
function PaGlobal_Window_Edana_SkillSelect:prepareClose()
  if nil == Panel_Window_EdanaContract_SkillSelect then
    return
  end
  self:close()
  self._isOpen = false
end
function PaGlobal_Window_Edana_SkillSelect:close()
  if nil == Panel_Window_EdanaContract_SkillSelect then
    return
  end
  Panel_Window_EdanaContract_SkillSelect:SetShow(false)
end
function PaGlobal_Window_Edana_SkillSelect:validate()
  if nil == Panel_Window_EdanaContract_SkillSelect then
    return
  end
  self._ui._closeButton:isValidate()
  self._ui._skillList:isValidate()
end
function PaGlobal_Window_Edana_SkillSelect:isOpen()
  if nil == Panel_Window_EdanaContract_SkillSelect then
    return
  end
  return self._isOpen
end
function PaGlobal_Window_Edana_SkillSelect:updateSkillList()
  if nil == Panel_Window_EdanaContract_SkillSelect or false == self._initialize then
    return
  end
  self._ui._skillList:getElementManager():clearKey()
  local skillTypeCount = ToClient_GetEdanaStatTypeCount()
  for skillType = 0, skillTypeCount - 1 do
    self:AddSkillType(skillType)
  end
end
function PaGlobal_Window_Edana_SkillSelect:AddSkillType(skillType)
  if nil == Panel_Window_EdanaContract_SkillSelect or false == self._initialize then
    return
  end
  if true == ToClient_IsSetEdanaStat(skillType) then
    return
  end
  self._ui._skillList:getElementManager():pushKey(toInt64(0, skillType))
  self._ui._skillList:requestUpdateByKey(toInt64(0, skillType))
end
