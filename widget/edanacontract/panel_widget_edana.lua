PaGlobal_Widget_Edana = {
  _ui = {
    _button = nil,
    _skillWindow = nil,
    _progress = nil
  },
  _isStart = false,
  _initialize = false
}
runLua("UI_Data/Script/Widget/EdanaContract/Panel_Widget_Edana_1.lua")
runLua("UI_Data/Script/Widget/EdanaContract/Panel_Widget_Edana_2.lua")
registerEvent("FromClient_luaLoadComplete", "FromClient_WidgetEdanaInit")
registerEvent("FromClient_UpdateEdanaPoints", "FromClient_UpdateEdanaPoints")
function FromClient_WidgetEdanaInit()
  PaGlobal_Widget_Edana:initialize()
end
