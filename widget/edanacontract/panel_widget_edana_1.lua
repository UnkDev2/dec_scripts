function PaGlobal_Widget_Edana:initialize()
  if true == self._initialize then
    return
  end
  self._ui._button = UI.getChildControl(Panel_Widget_Edana, "Button_Edana_template")
  self._skillWindow = PaGlobal_Window_Edana_Main
  self._ui._progress = UI.getChildControl(Panel_Widget_Edana, "CircularProgress_Point")
  self._skillWindow:initialize()
  self:registEventHandler()
  self:validate()
  self._initialize = true
end
function PaGlobal_Widget_Edana:registEventHandler()
  if nil == Panel_Widget_Edana then
    return
  end
  self._ui._button:addInputEvent("Mouse_LUp", "HandleEventMouseLup_Edana_CreateOrClearTotem()")
  self._ui._button:addInputEvent("Mouse_RUp", "HandleEventMouseRup_Edana_openSkillWindow()")
end
function PaGlobal_Widget_Edana:prepareOpen()
  if nil == Panel_Widget_Edana then
    return
  end
  self:updateProgress()
  self:open()
end
function PaGlobal_Widget_Edana:open()
  if nil == Panel_Widget_Edana then
    return
  end
  Panel_Widget_Edana:SetShow(true)
end
function PaGlobal_Widget_Edana:prepareClose()
  if nil == Panel_Widget_Edana then
    return
  end
  self:close()
end
function PaGlobal_Widget_Edana:close()
  if nil == Panel_Widget_Edana then
    return
  end
  Panel_Widget_Edana:SetShow(false)
end
function PaGlobal_Widget_Edana:validate()
  if nil == Panel_Widget_Edana then
    return
  end
  self._ui._button:isValidate()
  self._ui._progress:isValidate()
end
function PaGlobal_Widget_Edana:updateProgress()
  if nil == Panel_Widget_Edana then
    return
  end
  if true == self._initialize then
    local pointRate = ToClient_GetEdanaPoint() / ToClient_GetEdanaMaxPoint()
    self._ui._progress:SetProgressRate(pointRate * 100)
  end
end
