function HandleEventMouseLup_Edana_CreateOrClearTotem()
  if nil == Panel_Widget_Edana then
    return
  end
  local self = PaGlobal_Widget_Edana
  local isStart = self._isStart
  if false == isStart then
    ToClient_EdanaCreateTotem()
    self._isStart = not isStart
  elseif true == isStart then
    ToClient_EdanaClearTotem()
    self._isStart = not isStart
  end
end
function HandleEventMouseRup_Edana_openSkillWindow()
  local self = PaGlobal_Widget_Edana
  if nil == Panel_Widget_Edana or true == self._skillWindow:isOpen() then
    return
  end
  self._skillWindow:prepareOpen()
end
function FromClient_UpdateEdanaPoints()
  if nil == Panel_Widget_Edana then
    return
  end
  local self = PaGlobal_Widget_Edana
  self:updateProgress()
end
