function PaGlobal_FadeOut:initialize()
  if true == PaGlobal_FadeOut._initialize then
    return
  end
  PaGlobal_FadeOut._ui._loadingText = UI.getChildControl(Panel_FadeOut, "StaticText_LoadingEffect")
  PaGlobal_FadeOut._ui._loadingText:EraseAllEffect()
  PaGlobal_FadeOut._ui._loadingText:AddEffect("UI_Loading_01A", true, 0, 0)
  PaGlobal_FadeOut:registEventHandler()
  PaGlobal_FadeOut:validate()
  PaGlobal_FadeOut._initialize = true
end
function PaGlobal_FadeOut:registEventHandler()
  Panel_FadeOut:RegisterShowEventFunc(true, "PaGlobal_FadeOutShowAni()")
  Panel_FadeOut:RegisterShowEventFunc(false, "PaGlobal_FadeOutHideAni()")
  registerEvent("onScreenResize", "PaGlobal_FadeOutResize")
end
function PaGlobal_FadeOut:showAni()
  if nil == Panel_FadeOut then
    return
  end
  local showAni = Panel_FadeOut:addColorAnimation(0, PaGlobal_FadeOut._ANIMATION_TIME, CppEnums.PAUI_ANIM_ADVANCE_TYPE.PAUI_ANIM_ADVANCE_SIN_HALF_PI)
  showAni:SetStartColor(Defines.Color.C_00000000)
  showAni:SetEndColor(Defines.Color.C_FF000000)
  showAni:SetStartIntensity(3)
  showAni:SetEndIntensity(1)
  showAni.IsChangeChild = true
end
function PaGlobal_FadeOut:hideAni()
  if nil == Panel_FadeOut then
    return
  end
  local closeAni = Panel_FadeOut:addColorAnimation(0, PaGlobal_FadeOut._ANIMATION_TIME, CppEnums.PAUI_ANIM_ADVANCE_TYPE.PAUI_ANIM_ADVANCE_SIN_HALF_PI)
  closeAni:SetStartColor(Defines.Color.C_00000000)
  closeAni:SetEndColor(Defines.Color.C_FF000000)
  closeAni:SetStartIntensity(3)
  closeAni:SetEndIntensity(1)
  closeAni.IsChangeChild = true
  closeAni:SetHideAtEnd(true)
end
function PaGlobal_FadeOut:resize()
  if nil == Panel_FadeOut then
    return
  end
  if false == PaGlobal_FadeOut._initialize then
    return
  end
  Panel_FadeOut:SetSize(getScreenSizeX(), getScreenSizeY())
  PaGlobal_FadeOut._ui._loadingText:ComputePos()
  PaGlobal_FadeOut._ui._loadingText:SetPosX(getScreenSizeX() / 2 - (PaGlobal_FadeOut._ui._loadingText:GetTextSpan().x + PaGlobal_FadeOut._ui._loadingText:GetTextSizeX()) / 2)
end
function PaGlobal_FadeOut:open()
  if nil == Panel_FadeOut then
    return
  end
  PaGlobal_FadeOut:resize()
  Panel_FadeOut:SetShow(true, true)
end
function PaGlobal_FadeOut:close()
  if nil == Panel_FadeOut then
    return
  end
  if true == Panel_FadeOut:GetShow() then
    Panel_FadeOut:SetShow(false, true)
  end
end
function PaGlobal_FadeOut:validate()
  PaGlobal_FadeOut._ui._loadingText:isValidate()
end
