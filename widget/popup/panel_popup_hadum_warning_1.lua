function PaGlobal_Popup_HadumWarning:initialize()
  if true == PaGlobal_Popup_HadumWarning._initialize then
    return
  end
  self._ui.btn_Yes = UI.getChildControl(Panel_Popup_Hadum_Warning, "Button_Yes")
  self._ui.btn_No = UI.getChildControl(Panel_Popup_Hadum_Warning, "Button_No")
  PaGlobal_Popup_HadumWarning:registEventHandler()
  PaGlobal_Popup_HadumWarning:validate()
  PaGlobal_Popup_HadumWarning._initialize = true
end
function PaGlobal_Popup_HadumWarning:registEventHandler()
  if nil == Panel_Popup_Hadum_Warning then
    return
  end
  self._ui.btn_Yes:addInputEvent("Mouse_LUp", "HandleEventLUp_HadumWarning_EnterServer()")
  self._ui.btn_No:addInputEvent("Mouse_LUp", "PaGlobal_Popup_HadumWarning_Close()")
end
function PaGlobal_Popup_HadumWarning:prepareOpen()
  if nil == Panel_Popup_Hadum_Warning then
    return
  end
  PaGlobal_Popup_HadumWarning:open()
end
function PaGlobal_Popup_HadumWarning:open()
  if nil == Panel_Popup_Hadum_Warning then
    return
  end
  Panel_Popup_Hadum_Warning:SetShow(true)
end
function PaGlobal_Popup_HadumWarning:prepareClose()
  if nil == Panel_Popup_Hadum_Warning then
    return
  end
  PaGlobal_Popup_HadumWarning:close()
end
function PaGlobal_Popup_HadumWarning:close()
  if nil == Panel_Popup_Hadum_Warning then
    return
  end
  Panel_Popup_Hadum_Warning:SetShow(false)
end
function PaGlobal_Popup_HadumWarning:update()
  if nil == Panel_Popup_Hadum_Warning then
    return
  end
end
function PaGlobal_Popup_HadumWarning:validate()
  if nil == Panel_Popup_Hadum_Warning then
    return
  end
  self._ui.btn_Yes:isValidate()
  self._ui.btn_No:isValidate()
end
