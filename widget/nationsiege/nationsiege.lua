local NationSiege = {
  _ui = {},
  _variable = {}
}
function NationSiege:init()
  Panel_NationSiege:SetShow(true)
  local bg = UI.getChildControl(Panel_NationSiege, "Static_NationSiegeCenter")
  bg.SetShow(true)
  local deadCount = UI.getChildControl(bg, "StaticText_DeadCount")
  deadCount:SetText("\234\181\173\234\176\128\236\160\132")
end
function PaGloblaFunc_NationSiege_Init()
end
function FromClient_NationSiegeStart()
  local msg_type = 5
  local msg_Main = PAGetString(Defines.StringSheet_GAME, "LUA_WARINFOMESSAGE_NOTIFYSTARTSIEGE_SIEGESTART_MAIN")
  local msg_Sub = PAGetString(Defines.StringSheet_GAME, "LUA_WARINFOMESSAGE_NOTIFYSTARTSIEGE_SIEGESTART_SUB")
  local message = {
    main = msg_Main,
    sub = msg_Sub,
    addMsg = ""
  }
  Proc_ShowMessage_Ack_For_RewardSelect(message, 4, 10)
  if true == _ContentsGroup_SeigeSeason5 then
    local msg_Main2 = PAGetString(Defines.StringSheet_GAME, "LUA_WARINFOMESSAGE_NOTIFYSTARTSIEGE_SIEGEPROGRESS_PLUNDER")
    local message = {
      main = msg_Main2,
      sub = "",
      addMsg = ""
    }
    Proc_ShowMessage_Ack_For_RewardSelect(message, 4, 10)
  end
end
function FromClient_NationSiegeStop()
  local msg_Main = PAGetStringParam1(Defines.StringSheet_GAME, "LUA_WARINFOMESSAGE_NOTIFYOCCUPYSIEGE_END_MAIN", "territoryName", "")
  local msg_Sub = ""
  local message = {
    main = msg_Main,
    sub = msg_Sub,
    addMsg = ""
  }
  Proc_ShowMessage_Ack_For_RewardSelect(message, 4, 11)
end
function FromClient_NationSiegeVictory()
end
function FromClient_NationSiegeUpdatePlayerCount()
end
function FromClient_NationSiegeUpdateInformation()
end
registerEvent("FromClient_NationSiegeStart", "FromClient_NationSiegeStart")
registerEvent("FromClient_NationSiegeStop", "FromClient_NationSiegeStop")
registerEvent("FromClient_NationSiegeVictory", "FromClient_NationSiegeVictory")
registerEvent("FromClient_NationSiegeUpdatePlayerCount", "FromClient_NationSiegeUpdatePlayerCount")
registerEvent("FromClient_NationSiegeUpdateInformation", "FromClient_NationSiegeUpdateInformation")
registerEvent("FromClient_luaLoadComplete", "PaGloblaFunc_NationSiege_Init")
