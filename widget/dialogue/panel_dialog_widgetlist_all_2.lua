function PaGlobalFunc_DialogWidgetList_All_Open()
  PaGlobal_DialogWidgetList_All:prepareOpen()
end
function PaGlobalFunc_DialogWidgetList_All_Close()
  PaGlobal_DialogWidgetList_All:prepareClose()
end
function HandleEventLUp_DialogWidgetList_All_PagePrevClick()
  if nil == Panel_Dialog_WidgetList_All then
    return
  end
  PaGlobal_DialogWidgetList_All._curPage = PaGlobal_DialogWidgetList_All._curPage - 1
  if PaGlobal_DialogWidgetList_All._curPage == 0 then
    PaGlobal_DialogWidgetList_All._curPage = 1
  end
  PaGlobal_DialogWidgetList_All:updateDialogPage()
end
function HandleEventLUp_DialogWidgetList_All_PageNextClick()
  if nil == Panel_Dialog_WidgetList_All then
    return
  end
  PaGlobal_DialogWidgetList_All._curPage = PaGlobal_DialogWidgetList_All._curPage + 1
  if PaGlobal_DialogWidgetList_All._curPage == PaGlobal_DialogWidgetList_All._dialogMaxPage + 1 then
    PaGlobal_DialogWidgetList_All._curPage = PaGlobal_DialogWidgetList_All._dialogMaxPage
  end
  PaGlobal_DialogWidgetList_All:updateDialogPage()
end
function HandleEventLUp_DialogWidgetList_All_ButtonClick(index)
  if nil == Panel_Dialog_WidgetList_All then
    return
  end
  if MessageBoxGetShow() then
    return
  end
  if nil == index then
    _PA_ASSERT_NAME(false, "HandleEventLUp_DialogWidgetList_All_ButtonClick\236\157\152 index\234\176\128 nil\236\158\133\235\139\136\235\139\164", "\236\160\149\236\167\128\237\152\156")
    return
  end
  PaGlobal_DialogWidgetList_All._selectIndex = index
  local _doConfirmYes = function()
    PaGlobalFunc_DialogQuest_All_ClearSelectRewardItemName()
    PaGlobal_DialogWidgetList_All:clickList(PaGlobal_DialogWidgetList_All._selectIndex)
  end
  local _selectRewardItemName = PaGlobalFunc_DialogQuest_All_GetSelectedRewardItemName()
  if false ~= _selectRewardItemName and true == PaGlobal_DialogWidgetList_All._isQuestComplete then
    local messageBoxTitle = PAGetString(Defines.StringSheet_GAME, "LUA_COMMON_ALERT_NOTIFICATIONS")
    local messageBoxMemo = PAGetStringParam1(Defines.StringSheet_GAME, "LUA_NOTIFICATIONS_SELECTREWARD", "_selectRewardItemName", _selectRewardItemName)
    local messageBoxData = {
      title = messageBoxTitle,
      content = messageBoxMemo,
      functionYes = _doConfirmYes,
      functionNo = MessageBox_Empty_function,
      priority = CppEnums.PAUIMB_PRIORITY.PAUIMB_PRIORITY_LOW
    }
    MessageBox.showMessageBox(messageBoxData, "middle")
    return
  else
    PaGlobal_DialogWidgetList_All:clickList(PaGlobal_DialogWidgetList_All._selectIndex)
  end
end
function HandleEventOnOut_DialogWidgetList_All_NeedItemTooltip(isShow, needItemKey, index)
  if false == isShow then
    Panel_Tooltip_Item_hideTooltip()
    return
  end
  if nil == Panel_Dialog_WidgetList_All then
    return
  end
  if nil == needItemKey or nil == index then
    return
  end
  local itemStaticWrapper = getItemEnchantStaticStatus(ItemEnchantKey(needItemKey))
  Panel_Tooltip_Item_Show(itemStaticWrapper, PaGlobal_DialogWidgetList_All._ui.stc_dialogList[index], true, false, nil)
end
function HandleEventLUp_DialogWidgetList_All_SelectTab(index)
  PaGlobalFunc_DialogWidgetList_All_SetFilterOption(index)
end
function HandleEventScroll_DialogWidgetList_All_ListMove(isUp)
  if nil == Panel_Dialog_WidgetList_All then
    return
  end
  if 0 >= PaGlobal_DialogWidgetList_All._dialogListCount then
    return
  end
  if true == isUp then
    HandleEventLUp_DialogWidgetList_All_PagePrevClick()
  elseif false == isUp then
    HandleEventLUp_DialogWidgetList_All_PageNextClick()
  end
end
function PaGlobalFunc_DialogWidgetList_All_IsQuestComplete()
  return PaGlobal_DialogWidgetList_All._isQuestComplete
end
function PaGlobalFunc_DialogWidgetList_All_IsReContactDialog()
  if nil == Panel_Dialog_WidgetList_All then
    return false
  end
  return PaGlobal_DialogWidgetList_All._isReContactDialog
end
function PaGlobalFunc_DialogWidgetList_All_IsAbleQuest()
  if nil == Panel_Dialog_WidgetList_All then
    return false
  end
  return PaGlobal_DialogWidgetList_All._isAbleDisplayQuest
end
function PaGlobalFunc_DialogWidgetList_All_Update()
  if nil == Panel_Dialog_WidgetList_All then
    return
  end
  PaGlobal_DialogWidgetList_All:updateDialog()
end
function PaGlobalFunc_DialogWidgetList_All_SetProposeToNpc()
  PaGlobalFunc_DialogWidgetList_All_Open()
  PaGlobal_DialogWidgetList_All:updateDialog(true)
end
function PaGlobalFunc_DialogWidgetList_All_IsVisibleButton(buttonValue)
  local dialogData = ToClient_GetCurrentDialogData()
  if dialogData ~= nil then
    local dialogButtonCount = dialogData:getDialogButtonCount()
    for i = 0, dialogButtonCount - 1 do
      local dialogButton = dialogData:getDialogButtonAt(i)
      if dialogButton ~= nil and buttonValue == tostring(dialogButton._linkType) then
        return true
      end
    end
  end
  return false
end
