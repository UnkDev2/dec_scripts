PaGlobal_DialogWidgetList_All = {
  _ui = {
    stc_dialogGroup = nil,
    stc_dialogList = {},
    btn_dialogList = {}
  },
  _ui_pc = {
    stc_pageGroup = nil,
    btn_before = nil,
    btn_next = nil,
    txt_page = nil
  },
  _ui_console = {
    stc_bottomBg = nil,
    stc_iconA = nil,
    stc_iconB = nil
  },
  _dialogQuestButtonIcon = {
    [0] = {
      texture = "Combine/Etc/Combine_Etc_Quest_00.dds",
      x1 = 42,
      y1 = 103,
      x2 = 82,
      y2 = 143
    },
    [1] = {
      texture = "Combine/Etc/Combine_Etc_Quest_00.dds",
      x1 = 288,
      y1 = 103,
      x2 = 328,
      y2 = 143
    },
    [2] = {
      texture = "Combine/Etc/Combine_Etc_Quest_00.dds",
      x1 = 247,
      y1 = 103,
      x2 = 287,
      y2 = 143
    },
    [3] = {
      texture = "Combine/Etc/Combine_Etc_Quest_00.dds",
      x1 = 124,
      y1 = 103,
      x2 = 164,
      y2 = 143
    },
    [4] = {
      texture = "Combine/Etc/Combine_Etc_Quest_00.dds",
      x1 = 165,
      y1 = 103,
      x2 = 205,
      y2 = 143
    },
    [5] = {
      texture = "Combine/Etc/Combine_Etc_Quest_00.dds",
      x1 = 206,
      y1 = 103,
      x2 = 246,
      y2 = 143
    },
    [6] = {
      texture = "Combine/Etc/Combine_Etc_Quest_00.dds",
      x1 = 83,
      y1 = 103,
      x2 = 123,
      y2 = 143
    },
    [7] = {
      texture = "CRenewal/UI_Icon/Console_Icon_02.dds",
      x1 = 156,
      y1 = 229,
      x2 = 186,
      y2 = 259
    },
    [8] = {
      texture = "Combine/Etc/Combine_Etc_Quest_00.dds",
      x1 = 1,
      y1 = 103,
      x2 = 41,
      y2 = 143
    },
    [9] = {
      texture = "Renewal/UI_Icon/Console_Icon_02.dds",
      x1 = 125,
      y1 = 229,
      x2 = 155,
      y2 = 259
    },
    [10] = {
      texture = "Renewal/UI_Icon/Console_Icon_02.dds",
      x1 = 0,
      y1 = 0,
      x2 = 1,
      y2 = 1
    },
    [11] = {
      texture = "Renewal/UI_Icon/Console_Icon_02.dds",
      x1 = 0,
      y1 = 0,
      x2 = 1,
      y2 = 1
    }
  },
  _dialogButtonIcon = {
    [0] = {
      texture = "Combine/Icon/Combine_Dialogue_Icon_00.dds",
      x1 = 319,
      y1 = 339,
      x2 = 349,
      y2 = 369
    },
    [1] = {
      texture = "Combine/Icon/Combine_Dialogue_Icon_00.dds",
      x1 = 412,
      y1 = 339,
      x2 = 442,
      y2 = 369
    },
    [2] = {
      texture = "Combine/Icon/Combine_Dialogue_Icon_00.dds",
      x1 = 443,
      y1 = 339,
      x2 = 473,
      y2 = 369
    },
    [3] = {
      texture = "Combine/Icon/Combine_Dialogue_Icon_00.dds",
      x1 = 381,
      y1 = 339,
      x2 = 411,
      y2 = 369
    },
    [4] = {
      texture = "Combine/Icon/Combine_Dialogue_Icon_00.dds",
      x1 = 350,
      y1 = 339,
      x2 = 380,
      y2 = 369
    },
    [5] = {
      texture = "Combine/Icon/Combine_Dialogue_Icon_00.dds",
      x1 = 350,
      y1 = 339,
      x2 = 380,
      y2 = 369
    },
    [6] = {
      texture = "Renewal/UI_Icon/Console_DialogueIcon_00.dds",
      x1 = 218,
      y1 = 1,
      x2 = 248,
      y2 = 31
    }
  },
  _dialogListCount = 0,
  _dialogMaxPage = 1,
  _curPage = 1,
  _selectIndex = 0,
  _panelMinimumSizeX = 525,
  _panelMinimumSizeY = 210,
  _dialogSizeY = 55,
  _isQuestComplete = false,
  _isAbleDisplayQuest = false,
  _isReContactDialog = false,
  _isQuestView = false,
  _isExchangeButtonIndex = {},
  _btnSplitString = {},
  _initialize = false
}
runLua("UI_Data/Script/Widget/Dialogue/Panel_Dialog_WidgetList_All_1.lua")
runLua("UI_Data/Script/Widget/Dialogue/Panel_Dialog_WidgetList_All_2.lua")
registerEvent("FromClient_luaLoadComplete", "FromClient_DialogWidgetList_All_Init")
function FromClient_DialogWidgetList_All_Init()
  PaGlobal_DialogWidgetList_All:initialize()
end
