function PaGlobalFunc_Teleport_Open()
  PaGlobal_Teleport:prepareOpen()
end
function PaGlobalFunc_Teleport_Close()
  PaGlobal_Teleport:prepareClose()
end
function HandleEventEnter_Teleport_ClickOK()
  local _doConfirmYes = function()
    PaGlobalFunc_Teleport_Close()
  end
  local _doConfirmNo = function()
  end
  local messageBoxTitle = PAGetString(Defines.StringSheet_GAME, "LUA_UNDERTHESEA_OK_TITLE")
  local messageBoxMemo = PAGetString(Defines.StringSheet_GAME, "LUA_UNDERTHESEA_OK_MESSAGE")
  local messageBoxData = {
    title = messageBoxTitle,
    content = messageBoxMemo,
    functionYes = _doConfirmYes,
    functionNo = _doConfirmNo,
    priority = CppEnums.PAUIMB_PRIORITY.PAUIMB_PRIORITY_LOW
  }
  MessageBox.showMessageBox(messageBoxData, "middle")
end
function HandleEventEnter_Teleport_ClickClose()
  PaGlobalFunc_Teleport_Close()
end
function HandleEventEnter_Teleport_ClickTeleport(index)
  if nil == PaGlobal_Teleport then
    return
  end
  local function _doConfirmYes()
    local regionKey = PaGlobal_Teleport._regionkey_list[index]
    ToClient_TeleportToNode(regionKey)
  end
  local _doConfirmNo = function()
  end
  local messageBoxTitle = PAGetString(Defines.StringSheet_GAME, "LUA_UNDERTHESEA_TITLE")
  local messageBoxMemo = PAGetStringParam1(Defines.StringSheet_GAME, "LUA_TO_UNDERTHESEA", "to", PaGlobal_Teleport._name_list[index])
  local messageBoxData = {
    title = messageBoxTitle,
    content = messageBoxMemo,
    functionYes = _doConfirmYes,
    functionNo = _doConfirmNo,
    priority = CppEnums.PAUIMB_PRIORITY.PAUIMB_PRIORITY_LOW
  }
  MessageBox.showMessageBox(messageBoxData, "middle")
end
