function PaGlobal_Teleport:initialize()
  if true == PaGlobal_Teleport._initialize then
    return
  end
  self:controlInit()
  self:controlSetShow()
  PaGlobal_Teleport:registEventHandler()
  PaGlobal_Teleport:validate()
  PaGlobal_Teleport._initialize = true
end
function PaGlobal_Teleport:controlInit()
  if nil == Panel_UndertheSea then
    return
  end
  self._ui.stc_titlebg = UI.getChildControl(Panel_UndertheSea, "Static_TitleBg")
  self._ui.stc_title = UI.getChildControl(self._ui.stc_titlebg, "StaticText_Title")
  self._ui.btn_close = UI.getChildControl(self._ui.stc_titlebg, "Button_Close")
  self._ui.btn_ok = UI.getChildControl(Panel_UndertheSea, "Button_1")
  self._ui.list2_list = UI.getChildControl(Panel_UndertheSea, "List2_List")
  self._ui.list2_list:createChildContent(__ePAUIList2ElementManagerType_List)
end
function PaGlobal_Teleport:prepareOpen()
  if nil == Panel_UndertheSea then
    return
  end
  PaGlobal_Teleport:open()
  PaGlobal_Teleport:teleportListUpdate()
  PaGlobal_Teleport:resize()
end
function PaGlobal_Teleport:controlSetShow()
  if nil == Panel_UndertheSea then
    return
  end
  self._ui.stc_title:SetText(PAGetString(Defines.StringSheet_GAME, "LUA_UNDERTHESEA_LIST"))
  self._ui.btn_ok:SetText(PAGetString(Defines.StringSheet_GAME, "LUA_UNDERTHESEA_OK_BUTTON_TEXT"))
  self._ui.stc_title:SetShow(true)
  self._ui.btn_close:SetShow(true)
  self._ui.btn_ok:SetShow(true)
end
function PaGlobal_Teleport:open()
  if nil == Panel_UndertheSea then
    return
  end
  Panel_UndertheSea:SetShow(true)
end
function PaGlobal_Teleport:prepareClose()
  if nil == Panel_UndertheSea then
    return
  end
  PaGlobal_Teleport:close()
end
function PaGlobal_Teleport:close()
  if nil == Panel_UndertheSea then
    return
  end
  Panel_UndertheSea:SetShow(false)
end
function PaGlobal_Teleport:registEventHandler()
  if nil == Panel_UndertheSea then
    return
  end
  self._ui.btn_ok:addInputEvent("Mouse_LUp", "HandleEventEnter_Teleport_ClickOK()")
  self._ui.btn_close:addInputEvent("Mouse_LUp", "HandleEventEnter_Teleport_ClickClose()")
end
function PaGlobal_Teleport:resize()
  if nil == Panel_UndertheSea then
    return
  end
  for index = 0, self._teleportListSize - 1 do
    key = self._ui.list2_list:getKeyByIndex(index)
    content = self._ui.list2_list:GetContentByKey(key)
    if nil == content then
      return
    end
    local contentButton = UI.getChildControl(content, "RadioButton_1")
    local contentText = UI.getChildControl(contentButton, "StaticText_Title")
    local countText = UI.getChildControl(contentButton, "StaticText_count")
    local speedText = UI.getChildControl(contentButton, "StaticText_Speed")
    contentButton:addInputEvent("Mouse_LUp", "HandleEventEnter_Teleport_ClickTeleport(" .. tostring(index) .. ")")
    countText:SetPosX(220)
    speedText:SetPosX(270)
  end
end
function PaGlobal_Teleport:getTeleportListSize()
  self._teleportListSize = ToClient_GetTeleportListSize()
  self._ui.list2_list:getElementManager():clearKey()
  self._buffInfo = {}
  if self._teleportListSize < 1 then
    return
  end
  for index = 0, self._teleportListSize - 1 do
    self._regionkey_list[index] = ToClient_GetTeleportListElement(index)
    self._name_list[index] = ToClient_GetTeleportListElementString(index)
    if 0 ~= index then
      self._ui.list2_list:getElementManager():pushKey(toInt64(0, index))
    end
  end
end
function PaGlobal_Teleport:validate()
  if nil == Panel_UndertheSea then
    return
  end
  self._ui.stc_title:isValidate()
  self._ui.btn_close:isValidate()
  self._ui.btn_ok:isValidate()
end
function PaGlobal_Teleport:teleportListUpdate()
  PaGlobal_Teleport:getTeleportListSize()
  local key = 0
  local content
  for index = 0, self._teleportListSize - 1 do
    key = self._ui.list2_list:getKeyByIndex(index)
    content = self._ui.list2_list:GetContentByKey(key)
    if nil == content then
      return
    end
    local contentButton = UI.getChildControl(content, "RadioButton_1")
    local contentText = UI.getChildControl(contentButton, "StaticText_Title")
    local countText = UI.getChildControl(contentButton, "StaticText_count")
    local speedText = UI.getChildControl(contentButton, "StaticText_Speed")
    contentButton:addInputEvent("Mouse_LUp", "HandleEventEnter_Teleport_ClickTeleport(" .. tostring(index) .. ")")
    contentText:SetText(self._name_list[index])
    countText:SetText(PAGetStringParam1(Defines.StringSheet_GAME, "LUA_UNDERTHESEA_DISTANCE", "distance", 999))
    local minCost = ToClient_CaculateTeleportMinCostByIndex(index)
    speedText:SetText(PAGetStringParam1(Defines.StringSheet_GAME, "LUA_UNDERTHESEA_PRICE", "price", minCost))
  end
  self._selectIndex = nil
  self:resize()
end
function PaGlobal_Teleport:RegisterCloseLuaFunc()
  PaGlobal_Teleport:prepareClose()
end
