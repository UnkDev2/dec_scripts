function PaGlobal_HopeGaugeOnOff:initialize()
  if true == PaGlobal_HopeGaugeOnOff._initialize then
    return
  end
  local infoUI = self._ui
  infoUI._stc_slot = UI.getChildControl(Panel_Widget_HopeGaugeOnOff, "Static_Slot")
  infoUI._btn_open = UI.getChildControl(Panel_Widget_HopeGaugeOnOff, "Button_LeftArrow")
  infoUI._btn_off = UI.getChildControl(Panel_Widget_HopeGaugeOnOff, "Button_Off")
  infoUI._btn_1set = UI.getChildControl(Panel_Widget_HopeGaugeOnOff, "Button_1set")
  infoUI._btn_2set = UI.getChildControl(Panel_Widget_HopeGaugeOnOff, "Button_2set")
  infoUI._btn_charge = UI.getChildControl(Panel_Widget_HopeGaugeOnOff, "Button_Charge")
  infoUI._stc_effect = UI.getChildControl(Panel_Widget_HopeGaugeOnOff, "Static_Lotate")
  infoUI._stc_off = UI.getChildControl(Panel_Widget_HopeGaugeOnOff, "Static_Off")
  infoUI._stc_1set = UI.getChildControl(Panel_Widget_HopeGaugeOnOff, "Static_1set")
  infoUI._stc_2set = UI.getChildControl(Panel_Widget_HopeGaugeOnOff, "Static_2set")
  self._config._aniStartPosX = infoUI._btn_open:GetPosX()
  self._config._aniStartPosY = infoUI._btn_open:GetPosY() - 10
  self._config._aniSpeed = 0.5
  self._config._aniDistance = infoUI._btn_off:GetSizeX()
  infoUI._btn_off:SetShow(false)
  infoUI._btn_1set:SetShow(false)
  infoUI._btn_2set:SetShow(false)
  infoUI._btn_charge:SetShow(false)
  Panel_Widget_HopeGaugeOnOff:SetChildIndex(infoUI._stc_slot, 9999)
  PaGlobal_HopeGaugeOnOff:registEventHandler()
  PaGlobal_HopeGaugeOnOff:validate()
  PaGlobal_HopeGaugeOnOff._initialize = true
end
function PaGlobal_HopeGaugeOnOff:registEventHandler()
  if nil == Panel_Widget_HopeGaugeOnOff then
    return
  end
  local infoUI = self._ui
  infoUI._btn_charge:addInputEvent("Mouse_LUp", "HandleEventLUp_HopeGaugeOnOff_ChargeOpen()")
  infoUI._btn_charge:addInputEvent("Mouse_On", "PaGlobal_HopeGaugeOnOff_SimpleTooltips(true," .. self._tooltip._charge .. ")")
  infoUI._btn_charge:addInputEvent("Mouse_Out", "PaGlobal_HopeGaugeOnOff_SimpleTooltips(false)")
  Panel_Widget_HopeGaugeOnOff:addInputEvent("Mouse_On", "PaGlobal_HopeGaugeOnOff_SimpleTooltips(true," .. self._tooltip._slot .. ")")
  Panel_Widget_HopeGaugeOnOff:addInputEvent("Mouse_Out", "PaGlobal_HopeGaugeOnOff_SimpleTooltips(false)")
  infoUI._btn_open:addInputEvent("Mouse_LUp", "PaGlobal_HopeGaugeOnOff:TogglePold()")
  Panel_Widget_HopeGaugeOnOff:addInputEvent("Mouse_LUp", "PaGlobal_HopeGaugeOnOff:TogglePold()")
  Panel_Widget_HopeGaugeOnOff:addInputEvent("Mouse_RUp", "PaGlobal_HopeGaugeOnOff:prepareClose()")
  infoUI._btn_off:addInputEvent("Mouse_LUp", "PaGlobal_HopeGaugeOnOff_ApplyItemScroll(0)")
  infoUI._btn_1set:addInputEvent("Mouse_LUp", "PaGlobal_HopeGaugeOnOff_ApplyItemScroll(1)")
  infoUI._btn_2set:addInputEvent("Mouse_LUp", "PaGlobal_HopeGaugeOnOff_ApplyItemScroll(2)")
  infoUI._btn_off:addInputEvent("Mouse_On", "PaGlobal_HopeGaugeOnOff_SimpleTooltips(true," .. self._tooltip._off .. ")")
  infoUI._btn_1set:addInputEvent("Mouse_On", "PaGlobal_HopeGaugeOnOff_SimpleTooltips(true," .. self._tooltip._1set .. ")")
  infoUI._btn_2set:addInputEvent("Mouse_On", "PaGlobal_HopeGaugeOnOff_SimpleTooltips(true," .. self._tooltip._2set .. ")")
  infoUI._btn_off:addInputEvent("Mouse_Out", "PaGlobal_HopeGaugeOnOff_SimpleTooltips(false)")
  infoUI._btn_1set:addInputEvent("Mouse_Out", "PaGlobal_HopeGaugeOnOff_SimpleTooltips(false)")
  infoUI._btn_2set:addInputEvent("Mouse_Out", "PaGlobal_HopeGaugeOnOff_SimpleTooltips(false)")
end
function PaGlobal_HopeGaugeOnOff:prepareOpen()
  if nil == Panel_Widget_HopeGaugeOnOff then
    return
  end
  local infoUI = self._ui
  infoUI._stc_slot:EraseAllEffect()
  infoUI._stc_slot:AddEffect("fUI_HopeGauge_On_01A", false, 0, 0)
  PaGlobal_HopeGaugeOnOff:updatePos()
  PaGlobal_HopeGaugeOnOff:update()
  PaGlobal_HopeGaugeOnOff:open()
end
function PaGlobal_HopeGaugeOnOff:open()
  if nil == Panel_Widget_HopeGaugeOnOff then
    return
  end
  Panel_Widget_HopeGaugeOnOff:SetShow(true)
end
function PaGlobal_HopeGaugeOnOff:prepareClose()
  if nil == Panel_Widget_HopeGaugeOnOff then
    return
  end
  if 0 ~= ToClient_AppliedHopeDrop() then
    return
  end
  PaGlobal_HopeGaugeOnOff_SimpleTooltips(false)
  PaGlobal_HopeGauge_Close()
  PaGlobal_HopeGaugeOnOff:close()
end
function PaGlobal_HopeGaugeOnOff:close()
  if nil == Panel_Widget_HopeGaugeOnOff then
    return
  end
  Panel_Widget_HopeGaugeOnOff:SetShow(false)
end
function PaGlobal_HopeGaugeOnOff:updatePos()
  if false == self._initialize then
    return
  end
  self:initPos()
end
function PaGlobal_HopeGaugeOnOff_ApplyItemScroll(idx)
  local infoUI = PaGlobal_HopeGaugeOnOff._ui
  if infoUI._btn_off:isPlayAnimation() or infoUI._btn_1set:isPlayAnimation() or infoUI._btn_2set:isPlayAnimation() then
    return
  end
  ToClient_ApplyItemCollectionScroll(idx, false)
end
function PaGlobal_HopeGaugeOnOff:ChangeTexture(idx)
  local infoUI = self._ui
  local config = self._config
  infoUI._stc_off:SetShow(false)
  infoUI._stc_1set:SetShow(false)
  infoUI._stc_2set:SetShow(false)
  infoUI._btn_off:setRenderTexture(infoUI._btn_off:getBaseTexture())
  infoUI._btn_1set:setRenderTexture(infoUI._btn_1set:getBaseTexture())
  infoUI._btn_2set:setRenderTexture(infoUI._btn_2set:getBaseTexture())
  if 1 == idx then
    infoUI._stc_1set:SetShow(true)
    infoUI._btn_1set:setRenderTexture(infoUI._btn_1set:getClickTexture())
  elseif 2 == idx then
    infoUI._stc_2set:SetShow(true)
    infoUI._btn_2set:setRenderTexture(infoUI._btn_2set:getClickTexture())
  else
    infoUI._stc_off:SetShow(true)
    infoUI._btn_off:setRenderTexture(infoUI._btn_off:getClickTexture())
  end
  if 0 ~= idx then
    infoUI._stc_effect:SetShow(true)
  else
    infoUI._stc_effect:SetShow(false)
  end
end
function PaGlobal_HopeGaugeOnOff_buttonOn()
  local selfPlayer = getSelfPlayer()
  if nil == selfPlayer then
    return
  end
  local grade = selfPlayer:get():getItemCollectionScrollGrade()
  local infoUI = PaGlobal_HopeGaugeOnOff._ui
  infoUI._btn_off:setRenderTexture(infoUI._btn_off:getBaseTexture())
  infoUI._btn_1set:setRenderTexture(infoUI._btn_1set:getBaseTexture())
  infoUI._btn_2set:setRenderTexture(infoUI._btn_2set:getBaseTexture())
  if 1 == grade then
    infoUI._btn_1set:setRenderTexture(infoUI._btn_1set:getClickTexture())
  elseif 2 == grade then
    infoUI._btn_2set:setRenderTexture(infoUI._btn_2set:getClickTexture())
  else
    infoUI._btn_off:setRenderTexture(infoUI._btn_off:getClickTexture())
  end
end
function PaGlobal_HopeGaugeOnOff_buttonOff(idx)
  local infoUI = PaGlobal_HopeGaugeOnOff._ui
  if 0 ~= idx then
    infoUI._btn_off:setRenderTexture(infoUI._btn_off:getBaseTexture())
  end
  if 1 ~= idx then
    infoUI._btn_1set:setRenderTexture(infoUI._btn_1set:getBaseTexture())
  end
  if 2 ~= idx then
    infoUI._btn_2set:setRenderTexture(infoUI._btn_2set:getBaseTexture())
  end
end
function PaGlobal_HopeGaugeOnOff:initPos()
  local targetPanel = Panel_Radar
  local inventoryType, itemWrapper
  if nil ~= Panel_WorldMiniMap and true == Panel_WorldMiniMap:GetShow() then
    targetPanel = Panel_WorldMiniMap
  end
  if false == _ContentsGroup_RemasterUI_Main_Alert then
    if Panel_HorseEndurance:GetShow() or Panel_CarriageEndurance:GetShow() or Panel_ShipEndurance:GetShow() or Panel_SailShipEndurance:GetShow() then
      if PcEnduranceToggle() then
        Panel_Widget_HopeGaugeOnOff:SetPosX(getScreenSizeX() - targetPanel:GetSizeX() - 280 - 10)
        Panel_Widget_HopeGaugeOnOff:SetPosY(targetPanel:GetSizeY() - 100)
      else
        Panel_Widget_HopeGaugeOnOff:SetPosX(getScreenSizeX() - targetPanel:GetSizeX() - 190 - 10)
        Panel_Widget_HopeGaugeOnOff:SetPosY(targetPanel:GetSizeY() - 100)
      end
    else
      Panel_Widget_HopeGaugeOnOff:SetPosX(getScreenSizeX() - targetPanel:GetSizeX() - 150 - 10)
      Panel_Widget_HopeGaugeOnOff:SetPosY(targetPanel:GetSizeY() - 100)
    end
  end
  if nil ~= self.slotNo then
    inventoryType = Inventory_GetCurrentInventoryType()
    itemWrapper = getInventoryItemByType(inventoryType, self.slotNo)
  end
  if nil ~= Panel_LocalwarByBalanceServer and Panel_LocalwarByBalanceServer:GetShow() then
    Panel_Widget_HopeGaugeOnOff:SetPosX(getScreenSizeX() - targetPanel:GetSizeX() - 150 - 10)
  else
    Panel_Widget_HopeGaugeOnOff:SetPosX(getScreenSizeX() - targetPanel:GetSizeX() - 80 - 10)
  end
  if nil ~= Panel_WhereUseItemDirection and Panel_WhereUseItemDirection:GetShow() then
    Panel_Widget_HopeGaugeOnOff:SetPosX(Panel_Widget_HopeGaugeOnOff:GetPosX() - Panel_WhereUseItemDirection:GetSizeX())
  end
  Panel_Widget_HopeGaugeOnOff:SetPosY(targetPanel:GetPosY() + targetPanel:GetSizeY() - Panel_Widget_HopeGaugeOnOff:GetSizeY())
  self._isPold = true
  local infoUI = self._ui
  infoUI._btn_off:SetShow(false)
  infoUI._btn_1set:SetShow(false)
  infoUI._btn_2set:SetShow(false)
  infoUI._btn_charge:SetShow(false)
end
function PaGlobal_HopeGaugeOnOff:update()
  if nil == Panel_Widget_HopeGaugeOnOff then
    return
  end
  if false == self._initialize then
    return
  end
  local selfPlayer = getSelfPlayer()
  if nil == selfPlayer then
    return
  end
  local point = selfPlayer:get():getItemCollectionScrollPoint()
  local grade = selfPlayer:get():getItemCollectionScrollGrade()
  local infoUI = self._ui
  self:ChangeTexture(grade)
  local alertPoint = ToClient_getHopeGaugeAlert()
  if point < alertPoint and 0 ~= grade then
    infoUI._stc_slot:EraseAllEffect()
    infoUI._stc_slot:AddEffect("fUI_HopeGauge_01A", true, 0, 0)
  elseif 0 == grade then
    infoUI._stc_slot:EraseAllEffect()
  end
end
function PaGlobal_HopeGaugeOnOff_GetChargePosX()
  local infoUI = PaGlobal_HopeGaugeOnOff._ui
  return infoUI._btn_charge:GetPosX()
end
function PaGlobal_HopeGaugeOnOff:validate()
  if nil == Panel_Widget_HopeGaugeOnOff then
    return
  end
  local infoUI = self._ui
  infoUI._btn_charge:isValidate()
  infoUI._stc_slot:isValidate()
  infoUI._btn_open:isValidate()
  infoUI._btn_off:isValidate()
  infoUI._btn_1set:isValidate()
  infoUI._btn_2set:isValidate()
  infoUI._stc_effect:isValidate()
  infoUI._stc_off:isValidate()
  infoUI._stc_1set:isValidate()
  infoUI._stc_2set:isValidate()
end
function PaGlobal_HopeGaugeOnOff:TogglePold()
  local infoUI = self._ui
  if true == self._isPold then
    infoUI._btn_off:SetShow(true)
    infoUI._btn_1set:SetShow(true)
    infoUI._btn_2set:SetShow(true)
    infoUI._btn_charge:SetShow(true)
    self._isPold = false
    self:ShowUnPoldAni()
  else
    self:ShowPoldAni()
    PaGlobal_HopeGauge_Close()
    self._isPold = true
  end
end
function PaGlobal_HopeGaugeOnOff:ShowUnPoldAni()
  local infoUI = self._ui
  local config = self._config
  local offAni = infoUI._btn_off:addMoveAnimation(0, config._aniSpeed, CppEnums.PAUI_ANIM_ADVANCE_TYPE.PAUI_ANIM_ADVANCE_SIN_HALF_PI)
  offAni:SetStartPosition(config._aniStartPosX, config._aniStartPosY)
  offAni:SetEndPosition(config._aniStartPosX - config._aniDistance * 3, config._aniStartPosY)
  offAni.IsChangeChild = true
  infoUI._btn_off:CalcUIAniPos(offAni)
  local set1Ani = infoUI._btn_1set:addMoveAnimation(0, config._aniSpeed, CppEnums.PAUI_ANIM_ADVANCE_TYPE.PAUI_ANIM_ADVANCE_SIN_HALF_PI)
  set1Ani:SetStartPosition(config._aniStartPosX, config._aniStartPosY)
  set1Ani:SetEndPosition(config._aniStartPosX - config._aniDistance * 2, config._aniStartPosY)
  set1Ani.IsChangeChild = true
  infoUI._btn_off:CalcUIAniPos(set1Ani)
  local set2Ani = infoUI._btn_2set:addMoveAnimation(0, config._aniSpeed, CppEnums.PAUI_ANIM_ADVANCE_TYPE.PAUI_ANIM_ADVANCE_SIN_HALF_PI)
  set2Ani:SetStartPosition(config._aniStartPosX, config._aniStartPosY)
  set2Ani:SetEndPosition(config._aniStartPosX - config._aniDistance * 1, config._aniStartPosY)
  set2Ani.IsChangeChild = true
  infoUI._btn_2set:CalcUIAniPos(set2Ani)
  local chargeAni = infoUI._btn_charge:addMoveAnimation(0, config._aniSpeed, CppEnums.PAUI_ANIM_ADVANCE_TYPE.PAUI_ANIM_ADVANCE_SIN_HALF_PI)
  chargeAni:SetStartPosition(config._aniStartPosX, config._aniStartPosY)
  chargeAni:SetEndPosition(config._aniStartPosX - config._aniDistance * 3 - infoUI._btn_charge:GetSizeX(), config._aniStartPosY)
  chargeAni.IsChangeChild = true
  infoUI._btn_charge:CalcUIAniPos(chargeAni)
end
function PaGlobal_HopeGaugeOnOff:ShowPoldAni()
  local infoUI = self._ui
  local config = self._config
  local offAni = infoUI._btn_off:addMoveAnimation(0, config._aniSpeed, CppEnums.PAUI_ANIM_ADVANCE_TYPE.PAUI_ANIM_ADVANCE_SIN_HALF_PI)
  offAni:SetStartPosition(config._aniStartPosX - config._aniDistance * 3, config._aniStartPosY)
  offAni:SetEndPosition(config._aniStartPosX, config._aniStartPosY)
  offAni:SetHideAtEnd(true)
  offAni.IsChangeChild = true
  infoUI._btn_off:CalcUIAniPos(offAni)
  local set1Ani = infoUI._btn_1set:addMoveAnimation(0, config._aniSpeed, CppEnums.PAUI_ANIM_ADVANCE_TYPE.PAUI_ANIM_ADVANCE_SIN_HALF_PI)
  set1Ani:SetStartPosition(config._aniStartPosX - config._aniDistance * 2, config._aniStartPosY)
  set1Ani:SetEndPosition(config._aniStartPosX, config._aniStartPosY)
  set1Ani:SetHideAtEnd(true)
  set1Ani.IsChangeChild = true
  infoUI._btn_off:CalcUIAniPos(set1Ani)
  local set2Ani = infoUI._btn_2set:addMoveAnimation(0, config._aniSpeed, CppEnums.PAUI_ANIM_ADVANCE_TYPE.PAUI_ANIM_ADVANCE_SIN_HALF_PI)
  set2Ani:SetStartPosition(config._aniStartPosX - config._aniDistance * 1, config._aniStartPosY)
  set2Ani:SetEndPosition(config._aniStartPosX, config._aniStartPosY)
  set2Ani:SetHideAtEnd(true)
  set2Ani.IsChangeChild = true
  infoUI._btn_2set:CalcUIAniPos(set2Ani)
  local chargeAni = infoUI._btn_charge:addMoveAnimation(0, config._aniSpeed, CppEnums.PAUI_ANIM_ADVANCE_TYPE.PAUI_ANIM_ADVANCE_SIN_HALF_PI)
  chargeAni:SetStartPosition(config._aniStartPosX - config._aniDistance * 3 - infoUI._btn_charge:GetSizeX(), config._aniStartPosY)
  chargeAni:SetEndPosition(config._aniStartPosX, config._aniStartPosY)
  chargeAni:SetHideAtEnd(true)
  chargeAni.IsChangeChild = true
  infoUI._btn_charge:CalcUIAniPos(chargeAni)
end
function PaGlobal_HopeGaugeOnOff_SimpleTooltips(isShow, type)
  if not isShow then
    TooltipSimple_Hide()
    PaGlobal_HopeGaugeOnOff_buttonOn()
    return
  end
  local name, desc, control
  local selfPlayer = getSelfPlayer()
  if nil == selfPlayer then
    return
  end
  local player = selfPlayer:get()
  local grade = player:getItemCollectionScrollGrade()
  local point = player:getItemCollectionScrollPoint()
  local drop = ToClient_AppliedHopeDrop()
  local count = ToClient_AppliedHopeCount()
  local tooltip = PaGlobal_HopeGaugeOnOff._tooltip
  name = PAGetString(Defines.StringSheet_GAME, "LUA_HOPEGAUGE_TOOLTIP_TITLE")
  if tooltip._slot == type then
    desc = PAGetStringParam4(Defines.StringSheet_GAME, "LUA_HOPEGAUGE_TOOLTIP_DESC", "point", makeDotMoney(point), "grade", grade, "drop", drop, "count", count)
    if 0 == grade then
      desc = PAGetStringParam1(Defines.StringSheet_GAME, "LUA_HOPEGAUGE_TOOLTIP_DESC_OFF", "point", makeDotMoney(point))
    elseif 2 == grade then
      desc = PAGetStringParam4(Defines.StringSheet_GAME, "LUA_HOPEGAUGE_TOOLTIP_DESC2", "point", makeDotMoney(point), "grade", grade, "drop", drop, "count", count)
    end
  elseif tooltip._charge == type then
    desc = PAGetString(Defines.StringSheet_GAME, "LUA_HOPE_CHARGE_INFO")
  elseif tooltip._off == type then
    desc = PAGetString(Defines.StringSheet_GAME, "LUA_HOPE_USE_OFF")
  elseif tooltip._1set == type then
    desc = PAGetStringParam3(Defines.StringSheet_GAME, "LUA_HOPE_GRADE_ON", "grade", type, "drop", ToClient_GetHopeDropByGrade(type), "count", ToClient_GetHopeCountByGrade(type))
    if 1 == grade then
      PaGlobal_HopeGaugeOnOff_buttonOn()
    end
  elseif tooltip._2set == type then
    desc = PAGetStringParam3(Defines.StringSheet_GAME, "LUA_HOPE_GRADE_ON2", "grade", type, "drop", ToClient_GetHopeDropByGrade(type), "count", ToClient_GetHopeCountByGrade(type))
    if 2 == grade then
      PaGlobal_HopeGaugeOnOff_buttonOn()
    end
  end
  TooltipSimple_Show(Panel_Widget_HopeGaugeOnOff, name, desc)
end
