function HandleEventLUp_HopeGaugeOnOff_ChargeOpen()
  if nil == Panel_Widget_HopeGaugeOnOff then
    return
  end
  local infoUI = PaGlobal_HopeGaugeOnOff._ui
  if infoUI._btn_charge:isPlayAnimation() then
    return
  end
  PaGlobal_HopeGauge:prepareOpen()
end
function HandleEventLUp_HopeGaugeOnOff_Open()
  if nil == Panel_Widget_HopeGaugeOnOff then
    return
  end
  if true == Panel_Widget_HopeGaugeOnOff:GetShow() then
    return
  end
  PaGlobal_HopeGaugeOnOff:prepareOpen()
  PaGlobalFunc_Inventory_All_Close()
end
function FromClient_HopeGaugeOnOff_updateHopeGrade(isAlarm)
  if nil == Panel_Widget_HopeGaugeOnOff then
    return
  end
  local selfPlayer = getSelfPlayer()
  if nil == selfPlayer then
    return
  end
  local player = selfPlayer:get()
  local grade = player:getItemCollectionScrollGrade()
  if false == Panel_Widget_HopeGaugeOnOff:GetShow() and 0 ~= grade then
    PaGlobal_HopeGaugeOnOff:prepareOpen()
  elseif true == isAlarm then
    local msg = PAGetStringParam1(Defines.StringSheet_GAME, "LUA_HOPEGAUGE_UPDATE_GRADE", "grade", grade)
    if 0 == grade then
      msg = PAGetString(Defines.StringSheet_GAME, "LUA_HOPE_POINT_ZERO")
    end
    Proc_ShowMessage_Ack(msg)
  end
  PaGlobal_HopeGaugeOnOff:update()
end
function FromCLient_HopeGaugeOnOff_changeScreen()
  PaGlobal_HopeGaugeOnOff:updatePos()
end
function FromClient_HopeGaugeOnOff_updateHopePoint()
  local selfPlayer = getSelfPlayer()
  if nil == selfPlayer then
    return
  end
  local player = selfPlayer:get()
  local point = player:getItemCollectionScrollPoint()
  local msg = PAGetStringParam1(Defines.StringSheet_GAME, "LUA_HOPEGAUGE_CHARGE", "point", makeDotMoney(point))
  if 0 == point then
    msg = PAGetString(Defines.StringSheet_GAME, "LUA_HOPE_POINT_ZERO")
  end
  Proc_ShowMessage_Ack(msg)
end
function PaGloabl_HopeGaugeOnOff_ShowAni()
  if nil == Panel_Widget_HopeGaugeOnOff then
    return
  end
end
function PaGloabl_HopeGaugeOnOff_HideAni()
  if nil == Panel_Widget_HopeGaugeOnOff then
    return
  end
end
function PaGlobal_HopeGaugeOnOff_Close()
  PaGlobal_HopeGaugeOnOff:prepareClose()
end
registerEvent("FromClient_updateItemCollectionPoint", "FromClient_HopeGaugeOnOff_updateHopePoint")
registerEvent("FromClient_updateItemCollectionGrade", "FromClient_HopeGaugeOnOff_updateHopeGrade")
registerEvent("onScreenResize", "FromCLient_HopeGaugeOnOff_changeScreen")
