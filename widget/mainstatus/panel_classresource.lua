local UI_PSFT = CppEnums.PAUI_SHOW_FADE_TYPE
local UI_ANI_ADV = CppEnums.PAUI_ANIM_ADVANCE_TYPE
local UI_color = Defines.Color
local resourceText = UI.getChildControl(Panel_ClassResource, "StaticText_ResourceText")
local resourceValue = UI.getChildControl(Panel_ClassResource, "StaticText_Count")
local phantomPopMSG = UI.getChildControl(Panel_ClassResource, "StaticText_PhantomPopHelp")
local _phantomCount_Icon = UI.getChildControl(Panel_ClassResource, "Static_BlackStone")
local _phantomCount_HelpText_Box = UI.getChildControl(Panel_ClassResource, "StaticText_PhantomHelp")
local _phantom_Effect_1stChk = false
local _phantom_Effect_2ndChk = false
local _phantom_Effect_3rdChk = false
local _fighterIcon = UI.getChildControl(Panel_ClassResource, "Static_FighterIcon")
local _fighterIcon_Point1 = UI.getChildControl(_fighterIcon, "Static_Point1")
local _fighterIcon_Point2 = UI.getChildControl(_fighterIcon, "Static_Point2")
local _fighterIcon_Point3 = UI.getChildControl(_fighterIcon, "Static_Point3")
local _shyIcon = UI.getChildControl(Panel_ClassResource, "Static_ShyIcon")
local _shyIcon_Point1 = UI.getChildControl(_shyIcon, "Static_Point1")
local _shyIcon_Point2 = UI.getChildControl(_shyIcon, "Static_Point2")
local _shyIcon_Point3 = UI.getChildControl(_shyIcon, "Static_Point3")
local _maehwaIcon = UI.getChildControl(Panel_ClassResource, "Static_MaehwaIcon")
local _maehwaIcon_Point1 = UI.getChildControl(_maehwaIcon, "Static_Left")
local _maehwaIcon_Point2 = UI.getChildControl(_maehwaIcon, "Static_Middle")
local _maehwaIcon_Point3 = UI.getChildControl(_maehwaIcon, "Static_Right")
local _maehwaIcon_Point4 = UI.getChildControl(_maehwaIcon, "Static_Main")
local _novaIcon = UI.getChildControl(Panel_ClassResource, "Static_NovaBG")
local _novaIconPercent = UI.getChildControl(_novaIcon, "StaticText_Percent")
local _novaCircularProgress = UI.getChildControl(Panel_ClassResource, "CircularProgress_Nova")
local _novaEffectTime = 0
local _maxNovaEffectTime = 0.01
local _isNovaProgressUp = false
local _curNovaProgress = 0
local _maxNovaProgress = 0
local _isNovaEffectUpdate = false
local _isNovaProgressFull = false
local curClassType = __eClassType_Count
local function classResource_RegisterEvent()
  if nil == Panel_ClassResource then
    return
  end
  Panel_ClassResource:RegisterShowEventFunc(true, "ClassResource_ShowAni()")
  Panel_ClassResource:RegisterShowEventFunc(false, "ClassResource_HideAni()")
  Panel_ClassResource:addInputEvent("Mouse_On", "ClassResource_ChangeTexture_On()")
  Panel_ClassResource:addInputEvent("Mouse_Out", "ClassResource_ChangeTexture_Off()")
  _phantomCount_Icon:addInputEvent("Mouse_On", "PhantomCount_HelpComment( true )")
  _phantomCount_Icon:addInputEvent("Mouse_Out", "PhantomCount_HelpComment( false )")
  _fighterIcon:addInputEvent("Mouse_On", "ClassResource_IconHelpComment( true )")
  _fighterIcon:addInputEvent("Mouse_Out", "ClassResource_IconHelpComment( false )")
  _shyIcon:addInputEvent("Mouse_On", "ClassResource_IconHelpComment( true )")
  _shyIcon:addInputEvent("Mouse_Out", "ClassResource_IconHelpComment( false )")
  _maehwaIcon:addInputEvent("Mouse_On", "ClassResource_IconHelpComment( true )")
  _maehwaIcon:addInputEvent("Mouse_Out", "ClassResource_IconHelpComment( false )")
  _novaCircularProgress:addInputEvent("Mouse_On", "ClassResource_IconHelpComment( true )")
  _novaCircularProgress:addInputEvent("Mouse_Out", "ClassResource_IconHelpComment( false )")
end
local function init()
  local selfPlayerWrapper = getSelfPlayer()
  if nil == selfPlayerWrapper then
    PaGlobalFunc_ClassResource_SetShowControl(false)
    return
  end
  local selfPlayer = selfPlayerWrapper:get()
  if nil == selfPlayer then
    PaGlobalFunc_ClassResource_SetShowControl(false)
    return
  end
  resourceValue:SetShow(false)
  _phantomCount_Icon:SetShow(false)
  _fighterIcon:SetShow(false)
  _shyIcon:SetShow(false)
  _maehwaIcon:SetShow(false)
  _novaIcon:SetShow(false)
  _novaCircularProgress:SetShow(false)
  curClassType = selfPlayerWrapper:getClassType()
  if __eClassType_Sorcerer == curClassType then
    local phantomCount = selfPlayer:getSubResourcePoint()
    Panel_ClassResource_SetShow(true)
    resourceValue:SetText("X " .. phantomCount)
    resourceValue:SetShow(true)
    _phantomCount_Icon:SetShow(true)
  elseif __eClassType_Combattant == curClassType or __eClassType_Mystic == curClassType then
    Panel_ClassResource_SetShow(true)
    _fighterIcon:SetShow(true)
  elseif __eClassType_ShyWaman == curClassType then
    Panel_ClassResource_SetShow(true)
    local count = selfPlayer:getSubResourcePoint()
    resourceValue:SetText("X " .. count)
    resourceValue:SetShow(true)
    local movespanX = _shyIcon:GetSizeX() - _phantomCount_Icon:GetSizeX()
    local resoueceValueOriginalSpan = resourceValue:GetSpanSize()
    resourceValue:SetSpanSize(resoueceValueOriginalSpan.x + movespanX, resoueceValueOriginalSpan.y)
    resourceValue:ComputePos()
    _shyIcon:SetShow(true)
  elseif __eClassType_BladeMasterWoman == curClassType then
    if true == PaGlobal_ClassResource_CanMeahwaIconEnable() then
      Panel_ClassResource_SetShow(true)
      _maehwaIcon:SetShow(true)
    end
  elseif __eClassType_Nova == curClassType and true == PaGlobal_ClassResource_CanNovaIconEnable() then
    Panel_ClassResource_SetShow(true)
    _novaIcon:SetShow(true)
    _novaCircularProgress:SetShow(true)
    _novaIcon:EraseAllEffect()
    _isNovaEffectUpdate = false
    _isNovaProgressFull = false
    local showCount = 0
    local maxResourceCount = selfPlayer:getMaxSubResourcePoint(0)
    if maxResourceCount > 0 then
      local resourceCount = math.floor(selfPlayer:getSubResourcePoint(0))
      showCount = resourceCount / maxResourceCount * 100
      if showCount >= 100 then
        showCount = 100
      end
    end
    _maxNovaProgress = showCount
    _curNovaProgress = showCount
    _novaCircularProgress:SetProgressRate(_curNovaProgress)
    local showCountString = string.format("%.1f", _curNovaProgress)
    _novaIconPercent:SetText(showCountString .. "%")
    _novaIconPercent:SetShow(true)
  else
    PaGlobalFunc_ClassResource_SetShowControl(false)
  end
  classResource_RegisterEvent()
end
local function ResizeInit()
  local isReplayMode = ToClient_IsPlayingReplay()
  if true == isReplayMode then
    return
  end
  local selfPlayerWrapper = getSelfPlayer()
  if nil == selfPlayerWrapper then
    PaGlobalFunc_ClassResource_SetShowControl(false)
    return
  end
  local selfPlayer = selfPlayerWrapper:get()
  if nil == selfPlayer then
    PaGlobalFunc_ClassResource_SetShowControl(false)
    return
  end
  resourceValue:SetShow(false)
  _phantomCount_Icon:SetShow(false)
  _fighterIcon:SetShow(false)
  _shyIcon:SetShow(false)
  _novaIcon:SetShow(false)
  _novaCircularProgress:SetShow(false)
  curClassType = selfPlayerWrapper:getClassType()
  if __eClassType_Sorcerer == curClassType then
    local phantomCount = selfPlayer:getSubResourcePoint()
    resourceValue:SetText("X " .. phantomCount)
    Panel_ClassResource_SetShow(true)
    resourceValue:SetShow(true)
    _phantomCount_Icon:SetShow(true)
    if 0 < ToClient_GetUiInfo(CppEnums.PAGameUIType.PAGameUIPanel_ClassResource, 0, CppEnums.PanelSaveType.PanelSaveType_IsSaved) then
      Panel_ClassResource_SetShow(ToClient_GetUiInfo(CppEnums.PAGameUIType.PAGameUIPanel_ClassResource, 0, CppEnums.PanelSaveType.PanelSaveType_IsShow))
      if Panel_ClassResource:GetShow() == true then
        resourceValue:SetShow(true)
        _phantomCount_Icon:SetShow(true)
      else
        resourceValue:SetShow(false)
        _phantomCount_Icon:SetShow(false)
      end
    end
  elseif __eClassType_Combattant == curClassType or __eClassType_Mystic == curClassType then
    Panel_ClassResource_SetShow(true)
    if 0 < ToClient_GetUiInfo(CppEnums.PAGameUIType.PAGameUIPanel_ClassResource, 0, CppEnums.PanelSaveType.PanelSaveType_IsSaved) then
      Panel_ClassResource_SetShow(ToClient_GetUiInfo(CppEnums.PAGameUIType.PAGameUIPanel_ClassResource, 0, CppEnums.PanelSaveType.PanelSaveType_IsShow))
    end
    if Panel_ClassResource:GetShow() then
      _fighterIcon:SetShow(true)
    end
  elseif __eClassType_ShyWaman == curClassType then
    local shyCount = selfPlayer:getSubResourcePoint()
    resourceValue:SetText("X " .. shyCount)
    Panel_ClassResource_SetShow(true)
    if 0 < ToClient_GetUiInfo(CppEnums.PAGameUIType.PAGameUIPanel_ClassResource, 0, CppEnums.PanelSaveType.PanelSaveType_IsSaved) then
      Panel_ClassResource_SetShow(ToClient_GetUiInfo(CppEnums.PAGameUIType.PAGameUIPanel_ClassResource, 0, CppEnums.PanelSaveType.PanelSaveType_IsShow))
    end
    if Panel_ClassResource:GetShow() then
      resourceValue:SetShow(true)
      _shyIcon:SetShow(true)
    end
  elseif __eClassType_BladeMasterWoman == curClassType then
    Panel_ClassResource_SetShow(true)
    if 0 < ToClient_GetUiInfo(CppEnums.PAGameUIType.PAGameUIPanel_ClassResource, 0, CppEnums.PanelSaveType.PanelSaveType_IsSaved) then
      Panel_ClassResource_SetShow(ToClient_GetUiInfo(CppEnums.PAGameUIType.PAGameUIPanel_ClassResource, 0, CppEnums.PanelSaveType.PanelSaveType_IsShow))
    end
    if Panel_ClassResource:GetShow() then
      _maehwaIcon:SetShow(PaGlobal_ClassResource_CanMeahwaIconEnable())
    end
  elseif __eClassType_Nova == curClassType and true == PaGlobal_ClassResource_CanNovaIconEnable() then
    Panel_ClassResource_SetShow(true)
    if 0 < ToClient_GetUiInfo(CppEnums.PAGameUIType.PAGameUIPanel_ClassResource, 0, CppEnums.PanelSaveType.PanelSaveType_IsSaved) then
      Panel_ClassResource_SetShow(ToClient_GetUiInfo(CppEnums.PAGameUIType.PAGameUIPanel_ClassResource, 0, CppEnums.PanelSaveType.PanelSaveType_IsShow))
    end
    if true == Panel_ClassResource:GetShow() then
      _novaCircularProgress:SetShow(true)
      _novaIcon:SetShow(true)
      _novaIcon:EraseAllEffect()
      _isNovaEffectUpdate = false
      _isNovaProgressFull = false
      local showCount = 0
      local maxResourceCount = selfPlayer:getMaxSubResourcePoint(0)
      if maxResourceCount > 0 then
        local resourceCount = math.floor(selfPlayer:getSubResourcePoint(0))
        showCount = resourceCount / maxResourceCount * 100
        if showCount >= 100 then
          showCount = 100
        end
      end
      _maxNovaProgress = showCount
      _curNovaProgress = showCount
      _novaCircularProgress:SetProgressRate(_curNovaProgress)
      local showCountString = string.format("%.1f", _curNovaProgress)
      _novaIconPercent:SetText(showCountString .. "%")
      _novaIconPercent:SetShow(true)
    end
  else
    PaGlobalFunc_ClassResource_SetShowControl(false)
  end
end
function PaGlobalFunc_ClassResource_SetShowControl(isShow, isAni)
  local isReplayMode = ToClient_IsPlayingReplay()
  if true == isShow and false == isReplayMode then
    local selfPlayer = getSelfPlayer()
    if nil == selfPlayer then
      return
    end
    local classType = selfPlayer:getClassType()
    if __eClassType_Sorcerer == classType then
      resourceValue:SetShow(true)
      _phantomCount_Icon:SetShow(true)
    elseif __eClassType_Combattant == classType or __eClassType_Mystic == classType then
      _fighterIcon:SetShow(true)
    elseif __eClassType_ShyWaman == classType then
      resourceValue:SetShow(true)
      _shyIcon:SetShow(true)
    elseif __eClassType_BladeMasterWoman == classType then
      _maehwaIcon:SetShow(PaGlobal_ClassResource_CanMeahwaIconEnable())
    else
      if __eClassType_Nova == classType then
        local canShow = PaGlobal_ClassResource_CanNovaIconEnable()
        _novaCircularProgress:SetShow(canShow)
        _novaIcon:SetShow(canShow)
      else
      end
    end
  elseif false == isShow then
    resourceValue:SetShow(false)
    _phantomCount_Icon:SetShow(false)
    _fighterIcon:SetShow(false)
    _shyIcon:SetShow(false)
    _maehwaIcon:SetShow(false)
    _novaIcon:SetShow(false)
    _novaCircularProgress:SetShow(false)
  end
end
function PhantomCount_HelpComment(_isShowPhantomHelp)
  if _isShowPhantomHelp == true then
    local _phantomCount_Message = ""
    local selfPlayer = getSelfPlayer()
    if nil == selfPlayer then
      return
    end
    local classType = selfPlayer:getClassType()
    if __eClassType_Sorcerer == classType then
      _phantomCount_Message = PAGetString(Defines.StringSheet_GAME, "LUA_PHANTOMCOUNT_MESSAGE")
    elseif __eClassType_Combattant == classType or __eClassType_Mystic == classType then
      _phantomCount_Message = PAGetString(Defines.StringSheet_GAME, "LUA_CLASSRESOURCE_FIGHTER")
    end
    Panel_ClassResource:SetChildIndex(_phantomCount_HelpText_Box, 9999)
    _phantomCount_HelpText_Box:SetTextMode(__eTextMode_AutoWrap)
    _phantomCount_HelpText_Box:SetAutoResize(true)
    _phantomCount_HelpText_Box:SetText(_phantomCount_Message)
    _phantomCount_HelpText_Box:SetPosX(getMousePosX() - Panel_ClassResource:GetPosX() - 70)
    _phantomCount_HelpText_Box:SetPosY(getMousePosY() - Panel_ClassResource:GetPosY() - 90)
    _phantomCount_HelpText_Box:ComputePos()
    _phantomCount_HelpText_Box:SetSize(_phantomCount_HelpText_Box:GetSizeX(), _phantomCount_HelpText_Box:GetSizeY())
    _phantomCount_HelpText_Box:SetAlpha(0)
    _phantomCount_HelpText_Box:SetFontAlpha(0)
    UIAni.AlphaAnimation(1, _phantomCount_HelpText_Box, 0, 0.2)
    _phantomCount_HelpText_Box:SetShow(true)
  else
    local aniInfo = UIAni.AlphaAnimation(0, _phantomCount_HelpText_Box, 0, 0.2)
    aniInfo:SetHideAtEnd(true)
  end
end
function ClassResource_IconHelpComment(isShow)
  if false == isShow then
    TooltipSimple_Hide()
    return
  end
  local selfPlayerWrapper = getSelfPlayer()
  if nil == selfPlayerWrapper then
    return
  end
  local selfPlayer = selfPlayerWrapper:get()
  if nil == selfPlayer then
    return
  end
  local classType = selfPlayerWrapper:getClassType()
  if __eClassType_Combattant == classType then
    name = PAGetString(Defines.StringSheet_GAME, "LUA_CLASSRESOURCE_FIGHTERTITLE")
    desc = PAGetString(Defines.StringSheet_GAME, "LUA_CLASSRESOURCE_FIGHTER")
    control = _fighterIcon
  elseif __eClassType_Mystic == classType then
    name = PAGetString(Defines.StringSheet_GAME, "LUA_CLASSRESOURCE_FIGHTERTITLE")
    desc = PAGetString(Defines.StringSheet_GAME, "LUA_CLASSRESOURCE_MYSTIC")
    control = _fighterIcon
  elseif __eClassType_ShyWaman == classType then
    name = PAGetString(Defines.StringSheet_GAME, "LUA_CLASSRESOURCE_SHY_NAME")
    desc = PAGetString(Defines.StringSheet_GAME, "LUA_CLASSRESOURCE_SHY_DESC")
    control = _shyIcon
  elseif __eClassType_BladeMasterWoman == classType then
    name = PAGetString(Defines.StringSheet_GAME, "LUA_CLASSRESOURCE_MAEHAW_NAME")
    desc = PAGetString(Defines.StringSheet_GAME, "LUA_CLASSRESOURCE_MAEHAW")
    control = _maehwaIcon
  elseif __eClassType_Nova == classType then
    name = PAGetString(Defines.StringSheet_GAME, "LUA_CLASSRESOURCE_NOVA_NAME")
    local showCount = 0
    local maxResourceCount = selfPlayer:getMaxSubResourcePoint(0)
    if maxResourceCount > 0 then
      local resourceCount = math.floor(selfPlayer:getSubResourcePoint(0))
      showCount = resourceCount / maxResourceCount * 100
      if showCount > 100 then
        showCount = 100
      end
    end
    showCount = string.format("%.1f", showCount)
    desc = PAGetStringParam1(Defines.StringSheet_GAME, "LUA_CLASSRESOURCE_NOVA", "count", showCount)
    control = _novaIcon
  end
  TooltipSimple_Show(control, name, desc)
end
function ClassResource_ChangeTexture_On()
  audioPostEvent_SystemUi(0, 10)
  Panel_ClassResource:ChangeTextureInfoName("new_ui_common_forlua/default/window_sample_drag.dds")
  resourceText:SetText(PAGetString(Defines.StringSheet_GAME, "LUA_PVPMODE_UI_MOVE"))
end
function ClassResource_ChangeTexture_Off()
  if Panel_UIControl:IsShow() then
    Panel_ClassResource:ChangeTextureInfoName("new_ui_common_forlua/default/window_sample_isWidget.dds")
    resourceText:SetText(PAGetString(Defines.StringSheet_GAME, "LUA_PVPMODE_UI"))
  else
    Panel_ClassResource:ChangeTextureInfoName("new_ui_common_forlua/default/window_sample_empty.dds")
  end
end
function ClassResource_ShowAni()
  Panel_ClassResource:SetShowWithFade(UI_PSFT.PAUI_ANI_TYPE_FADE_IN)
  local ClassResourceOpen_Alpha = Panel_ClassResource:addColorAnimation(0, 0.6, UI_ANI_ADV.PAUI_ANIM_ADVANCE_SIN_HALF_PI)
  ClassResourceOpen_Alpha:SetStartColor(UI_color.C_00FFFFFF)
  ClassResourceOpen_Alpha:SetEndColor(UI_color.C_FFFFFFFF)
  ClassResourceOpen_Alpha.IsChangeChild = true
end
function ClassResource_HideAni()
  Panel_ClassResource:SetShowWithFade(UI_PSFT.PAUI_ANI_TYPE_FADE_OUT)
  local ClassResourceClose_Alpha = Panel_ClassResource:addColorAnimation(0, 0.6, UI_ANI_ADV.PAUI_ANIM_ADVANCE_SIN_HALF_PI)
  ClassResourceClose_Alpha:SetStartColor(UI_color.C_FFFFFFFF)
  ClassResourceClose_Alpha:SetEndColor(UI_color.C_00FFFFFF)
  ClassResourceClose_Alpha.IsChangeChild = true
  ClassResourceClose_Alpha:SetHideAtEnd(true)
  ClassResourceClose_Alpha:SetDisableWhileAni(true)
end
local beforeFigherCount = -1
local beforeShyCount = -1
local beforeMaehwaCount = -1
function ClassResource_Update()
  local isReplayMode = ToClient_IsPlayingReplay()
  if true == isReplayMode then
    return
  end
  local selfPlayerWrapper = getSelfPlayer()
  if nil == selfPlayerWrapper then
    return
  end
  local selfPlayer = selfPlayerWrapper:get()
  if nil == selfPlayer then
    return
  end
  local playerMp = selfPlayer:getMp()
  local playerMaxMp = selfPlayer:getMaxMp()
  local playerMpRate = playerMp / playerMaxMp * 100
  if __eClassType_Sorcerer == curClassType then
    local phantomCount = selfPlayer:getSubResourcePoint()
    resourceValue:SetText("X " .. phantomCount)
    if phantomCount >= 10 and phantomCount <= 19 and _phantom_Effect_1stChk == false then
      _phantomCount_Icon:EraseAllEffect()
      _phantomCount_Icon:AddEffect("UI_Button_Hide", false, 0, 0)
      _phantom_Effect_1stChk = true
      _phantom_Effect_2ndChk = false
      _phantom_Effect_3rdChk = false
    elseif phantomCount >= 20 and phantomCount <= 29 and _phantom_Effect_2ndChk == false then
      _phantomCount_Icon:EraseAllEffect()
      _phantomCount_Icon:AddEffect("UI_Button_Hide", false, 0, 0)
      _phantom_Effect_1stChk = false
      _phantom_Effect_2ndChk = true
      _phantom_Effect_3rdChk = false
    elseif phantomCount == 30 and _phantom_Effect_3rdChk == false then
      _phantomCount_Icon:EraseAllEffect()
      _phantomCount_Icon:AddEffect("UI_Button_Hide", false, 0, 0)
      _phantom_Effect_1stChk = false
      _phantom_Effect_2ndChk = false
      _phantom_Effect_3rdChk = true
    elseif phantomCount == 0 then
      _phantomCount_Icon:EraseAllEffect()
      _phantom_Effect_1stChk = false
      _phantom_Effect_2ndChk = false
      _phantom_Effect_3rdChk = false
    end
    if phantomCount >= 10 and playerMpRate < 20 then
      phantomPopMSG:SetShow(true)
      phantomPopMSG:SetText(PAGetString(Defines.StringSheet_GAME, "LUA_CLASSRESOURCE_PHANTOMPOPMSG"))
    else
      phantomPopMSG:SetShow(false)
    end
  elseif __eClassType_Combattant == curClassType or __eClassType_Mystic == curClassType then
    local fighterCount = selfPlayer:getSubResourcePoint()
    _fighterIcon_Point1:SetShow(fighterCount >= 10)
    _fighterIcon_Point2:SetShow(fighterCount >= 20)
    _fighterIcon_Point3:SetShow(fighterCount >= 30)
    if beforeFigherCount < 10 then
      _fighterIcon_Point3:EraseAllEffect()
      if _fighterIcon_Point1:GetShow() then
        _fighterIcon_Point1:AddEffect("fUI_PCM_Energy_01A", false, 0, 0)
      end
    elseif beforeFigherCount < 20 then
      _fighterIcon_Point2:EraseAllEffect()
      if _fighterIcon_Point2:GetShow() then
        _fighterIcon_Point2:AddEffect("fUI_PCM_Energy_01A", false, 0, 0)
      end
    elseif beforeFigherCount < 30 then
      _fighterIcon_Point1:EraseAllEffect()
      if _fighterIcon_Point3:GetShow() then
        _fighterIcon_Point3:AddEffect("fUI_PCM_Energy_01A", false, 0, 0)
        _fighterIcon_Point3:AddEffect("fUI_PCM_Energy_02A", false, -15, -8)
        _fighterIcon_Point3:AddEffect("fUI_PCM_Energy_02B", true, -15, -8)
      end
    end
    if beforeFigherCount < 30 and 30 == fighterCount then
      _fighterIcon_Point1:AddEffect("fUI_PCM_Energy_01A", false, 0, 0)
      _fighterIcon_Point2:AddEffect("fUI_PCM_Energy_01A", false, 0, 0)
      _fighterIcon_Point3:AddEffect("fUI_PCM_Energy_01A", false, 0, 0)
      _fighterIcon_Point3:AddEffect("fUI_PCM_Energy_02A", false, -15, -8)
      _fighterIcon_Point3:AddEffect("fUI_PCM_Energy_02B", true, -15, -8)
    end
    if 30 == beforeFigherCount and 0 == fighterCount then
      _fighterIcon:AddEffect("CO_Empty", false, 0, 0)
    end
    beforeFigherCount = fighterCount
  elseif __eClassType_ShyWaman == curClassType then
    local shyCount = selfPlayer:getSubResourcePoint(0)
    resourceValue:SetText("X " .. shyCount)
    _shyIcon_Point1:SetShow(shyCount >= 10)
    _shyIcon_Point2:SetShow(shyCount >= 20)
    _shyIcon_Point3:SetShow(shyCount >= 30)
    if beforeShyCount < 10 then
      _shyIcon_Point1:EraseAllEffect()
      if _shyIcon_Point1:GetShow() then
        _shyIcon_Point1:AddEffect("fUI_PLW_Energy_01A", true, 0, 0)
      end
    end
    if beforeShyCount < 20 then
      _shyIcon_Point2:EraseAllEffect()
      if _shyIcon_Point2:GetShow() then
        _shyIcon_Point2:AddEffect("fUI_PLW_Energy_01B", true, 0, 0)
      end
    end
    if beforeShyCount < 30 then
      _shyIcon_Point3:EraseAllEffect()
      if _shyIcon_Point3:GetShow() then
        _shyIcon_Point1:EraseAllEffect()
        _shyIcon_Point2:EraseAllEffect()
        _shyIcon_Point3:AddEffect("fUI_PLW_Energy_01C", true, 0, 0)
      end
    end
    if 30 == beforeShyCount and 0 == shyCount then
      _shyIcon:AddEffect("fUI_PLW_Energy_01D", false, 0, 0)
    end
    beforeShyCount = shyCount
  elseif __eClassType_BladeMasterWoman == curClassType then
    local maehwaCount = selfPlayer:getSubResourcePoint()
    local isIconShow = PaGlobal_ClassResource_CanMeahwaIconEnable()
    _maehwaIcon:SetShow(isIconShow)
    if false == isIconShow then
      return
    end
    _maehwaIcon_Point1:SetShow(maehwaCount >= 1)
    _maehwaIcon_Point2:SetShow(maehwaCount >= 2)
    _maehwaIcon_Point3:SetShow(maehwaCount >= 3)
    if beforeMaehwaCount < 1 then
      _maehwaIcon_Point1:EraseAllEffect()
      if _maehwaIcon_Point1:GetShow() then
        _maehwaIcon_Point1:AddEffect("fUI_PKW_UP_Icon_01A", true, -22, 10)
        audioPostEvent_SystemUi(2, 5)
      end
    end
    if beforeMaehwaCount < 2 then
      _maehwaIcon_Point2:EraseAllEffect()
      if _maehwaIcon_Point2:GetShow() then
        _maehwaIcon_Point2:AddEffect("fUI_PKW_UP_Icon_01A", true, -9, -17)
        audioPostEvent_SystemUi(2, 5)
      end
    end
    if beforeMaehwaCount < 3 then
      _maehwaIcon_Point3:EraseAllEffect()
      if _maehwaIcon_Point3:GetShow() then
        _maehwaIcon_Point3:AddEffect("fUI_PKW_UP_Icon_01A", true, 17, -17)
        audioPostEvent_SystemUi(2, 5)
      end
    end
    if beforeMaehwaCount >= 2 and 0 == maehwaCount then
      local remainTime = PaGlobalAppliedBuffList:GetMaehwaBuff_RemainTime()
      if remainTime > -1 and false == _maehwaIcon_Point4:GetShow() then
        _maehwaIcon:AddEffect("fUI_PKW_UP_Icon_02A", false, 3, -3)
        audioPostEvent_SystemUi(2, 6)
        _maehwaIcon_Point4:SetShow(true)
        luaTimer_AddEvent(Panel_ClassResource_HideMaehwaSymbol, remainTime, false, 0)
      end
    end
    beforeMaehwaCount = maehwaCount
  elseif __eClassType_Nova == curClassType then
    if nil ~= PaGlobal_ClassResource_CanNovaIconEnable and true == PaGlobal_ClassResource_CanNovaIconEnable() then
      _novaIcon:SetShow(true)
      _novaCircularProgress:SetShow(true)
      _novaIconPercent:SetShow(true)
      local maxResourceCount = selfPlayer:getMaxSubResourcePoint(0)
      if maxResourceCount > 0 then
        local resourceCount = math.floor(selfPlayer:getSubResourcePoint(0))
        local showCount = resourceCount / maxResourceCount * 100
        if showCount >= 100 then
          showCount = 100
          if false == _isNovaProgressFull then
            _isNovaProgressFull = true
            _novaIcon:EraseAllEffect()
            _novaIcon:AddEffect("fUI_PPW_ARO_Icon_01A", true, 0, 0)
          end
        else
          _isNovaProgressFull = false
        end
        _novaEffectTime = _maxNovaEffectTime
        _isNovaProgressUp = showCount >= _maxNovaProgress
        _maxNovaProgress = showCount
        if true == _isNovaProgressUp then
          if nil ~= Panel_ClassResource then
            Panel_ClassResource:RegisterUpdateFunc("Panel_ClassResource_UpdateNovaProgress")
          end
        else
          if nil ~= Panel_ClassResource then
            Panel_ClassResource:ClearUpdateLuaFunc()
          end
          if _maxNovaProgress < 1 then
            _isNovaEffectUpdate = false
            _novaIcon:EraseAllEffect()
            _curNovaProgress = 0
            _maxNovaProgress = 0
          else
            _curNovaProgress = _maxNovaProgress
            if false == _isNovaEffectUpdate then
              _isNovaEffectUpdate = true
              _novaIcon:EraseAllEffect()
              _novaIcon:AddEffect("fUI_test_Circle", true, 0, 0)
            end
          end
          _novaCircularProgress:SetProgressRate(_curNovaProgress)
          local showCountString = string.format("%.1f", _curNovaProgress)
          _novaIconPercent:SetText(showCountString .. "%")
        end
      end
    else
      _novaIcon:EraseAllEffect()
      _novaIcon:SetShow(false)
      _novaCircularProgress:SetShow(false)
      _novaIconPercent:SetShow(false)
      Panel_ClassResource:ClearUpdateLuaFunc()
    end
  end
end
function Panel_ClassResource_UpdateNovaProgress(deltaTime)
  if nil == Panel_ClassResource then
    return
  end
  _novaEffectTime = _novaEffectTime + deltaTime
  if _maxNovaEffectTime < _novaEffectTime then
    _novaEffectTime = 0
    if true == _isNovaProgressUp then
      _curNovaProgress = _curNovaProgress + 1
      if _maxNovaProgress <= _curNovaProgress then
        _curNovaProgress = _maxNovaProgress
        Panel_ClassResource:ClearUpdateLuaFunc()
      end
      _novaCircularProgress:SetProgressRate(_curNovaProgress)
      local showCountString = string.format("%.1f", _curNovaProgress)
      _novaIconPercent:SetText(showCountString .. "%")
    else
      Panel_ClassResource:ClearUpdateLuaFunc()
    end
  end
end
function Panel_ClassResource_HideMaehwaSymbol()
  audioPostEvent_SystemUi(2, 7)
  _maehwaIcon_Point4:SetShow(false)
end
function Panel_ClasssResource_GetIconSize(idx)
  local x, y = 50, 50
  if idx == __eClassType_BladeMasterWoman then
    x, y = _maehwaIcon:GetSizeX(), _maehwaIcon:GetSizeY()
  elseif idx == __eClassType_ShyWaman then
    x, y = _shyIcon:GetSizeX(), _shyIcon:GetSizeY()
  elseif idx == __eClassType_Combattant or idx == __eClassType_Mystic then
    x, y = _fighterIcon:GetSizeX(), _fighterIcon:GetSizeY()
  elseif idx == __eClassType_Nova then
    x, y = _novaIcon:GetSizeX(), _novaIcon:GetSizeY()
  end
  return x, y
end
function Panel_ClassResource_EnableSimpleUI()
  Panel_ClassResource_SetAlphaAllChild(Panel_MainStatus_User_Bar:GetAlpha())
end
function Panel_ClassResource_DisableSimpleUI()
  Panel_ClassResource_SetAlphaAllChild(1)
end
function Panel_ClassResource_UpdateSimpleUI(fDeltaTime)
  Panel_ClassResource_SetAlphaAllChild(Panel_MainStatus_User_Bar:GetAlpha())
end
function Panel_ClassResource_SetAlphaAllChild(alphaValue)
  resourceText:SetFontAlpha(alphaValue)
  resourceValue:SetFontAlpha(alphaValue)
  _phantomCount_Icon:SetAlpha(alphaValue)
  _phantomCount_HelpText_Box:SetAlpha(alphaValue)
end
registerEvent("SimpleUI_UpdatePerFrame", "Panel_ClassResource_UpdateSimpleUI")
registerEvent("EventSimpleUIEnable", "Panel_ClassResource_EnableSimpleUI")
registerEvent("EventSimpleUIDisable", "Panel_ClassResource_DisableSimpleUI")
registerEvent("FromClient_SelfPlayerMpChanged", "ClassResource_Update")
function Phantom_Locate()
  local selfPlayer = getSelfPlayer()
  if nil == selfPlayer then
    return
  end
  local spanPosY = 30
  if Panel_ClassResource:GetRelativePosX() == -1 and Panel_ClassResource:GetRelativePosY() == -1 then
    local initPosX = Panel_MainStatus_User_Bar:GetPosX() + _phantomCount_Icon:GetSizeX() - 5
    local initPosY = Panel_MainStatus_User_Bar:GetPosY() - _phantomCount_Icon:GetSizeY() + 5 - spanPosY
    Panel_ClassResource:SetPosX(initPosX)
    Panel_ClassResource:SetPosY(initPosY)
    changePositionBySever(Panel_ClassResource, CppEnums.PAGameUIType.PAGameUIPanel_ClassResource, true, true, false)
    FGlobal_InitPanelRelativePos(Panel_ClassResource, initPosX, initPosY)
  elseif Panel_ClassResource:GetRelativePosX() == 0 and Panel_ClassResource:GetRelativePosY() == 0 then
    Panel_ClassResource:SetPosX(getScreenSizeX() / 2 - Panel_MainStatus_User_Bar:GetSizeX() / 2 + _phantomCount_Icon:GetSizeX() - 5)
    Panel_ClassResource:SetPosY(getScreenSizeY() - Panel_QuickSlot:GetSizeY() - Panel_MainStatus_User_Bar:GetSizeY() - _phantomCount_Icon:GetSizeY() + 5 - spanPosY)
  else
    Panel_ClassResource:SetPosX(getScreenSizeX() * Panel_ClassResource:GetRelativePosX() - Panel_ClassResource:GetSizeX() / 2)
    Panel_ClassResource:SetPosY(getScreenSizeY() * Panel_ClassResource:GetRelativePosY() - Panel_ClassResource:GetSizeY() / 2)
  end
  if 0 < ToClient_GetUiInfo(CppEnums.PAGameUIType.PAGameUIPanel_ClassResource, 0, CppEnums.PanelSaveType.PanelSaveType_IsSaved) then
    Panel_ClassResource_SetShow(ToClient_GetUiInfo(CppEnums.PAGameUIType.PAGameUIPanel_ClassResource, 0, CppEnums.PanelSaveType.PanelSaveType_IsShow))
  end
  FGlobal_PanelRepostionbyScreenOut(Panel_ClassResource)
  ResizeInit()
end
function Phantom_Resize()
  local relativePosX = ToClient_GetUiInfo(CppEnums.PAGameUIType.PAGameUIPanel_ClassResource, 0, CppEnums.PanelSaveType.PanelSaveType_RelativePositionX)
  local relativePosY = ToClient_GetUiInfo(CppEnums.PAGameUIType.PAGameUIPanel_ClassResource, 0, CppEnums.PanelSaveType.PanelSaveType_RelativePositionY)
  local spanPosY = 30
  if relativePosX == -1 and relativePosY == -1 then
    local initPosX = Panel_MainStatus_User_Bar:GetPosX() + _phantomCount_Icon:GetSizeX() - 5
    local initPosY = Panel_MainStatus_User_Bar:GetPosY() - _phantomCount_Icon:GetSizeY() + 5 - spanPosY
    Panel_ClassResource:SetPosX(initPosX)
    Panel_ClassResource:SetPosY(initPosY)
    changePositionBySever(Panel_ClassResource, CppEnums.PAGameUIType.PAGameUIPanel_ClassResource, true, true, false)
    FGlobal_InitPanelRelativePos(Panel_ClassResource, initPosX, initPosY)
  elseif relativePosX == 0 and relativePosY == 0 then
    Panel_ClassResource:SetPosX(getScreenSizeX() / 2 - Panel_MainStatus_User_Bar:GetSizeX() / 2 + _phantomCount_Icon:GetSizeX() - 5)
    Panel_ClassResource:SetPosY(getScreenSizeY() - Panel_QuickSlot:GetSizeY() - Panel_MainStatus_User_Bar:GetSizeY() - _phantomCount_Icon:GetSizeY() + 5 - spanPosY)
  else
    Panel_ClassResource:SetRelativePosX(relativePosX)
    Panel_ClassResource:SetRelativePosY(relativePosY)
    Panel_ClassResource:SetPosX(getScreenSizeX() * relativePosX - Panel_ClassResource:GetSizeX() / 2)
    Panel_ClassResource:SetPosY(getScreenSizeY() * relativePosY - Panel_ClassResource:GetSizeY() / 2)
  end
  FGlobal_PanelRepostionbyScreenOut(Panel_ClassResource)
  if 0 < ToClient_GetUiInfo(CppEnums.PAGameUIType.PAGameUIPanel_ClassResource, 0, CppEnums.PanelSaveType.PanelSaveType_IsSaved) then
    Panel_ClassResource_SetShow(ToClient_GetUiInfo(CppEnums.PAGameUIType.PAGameUIPanel_ClassResource, 0, CppEnums.PanelSaveType.PanelSaveType_IsShow))
  end
  ResizeInit()
end
function Panel_ClassResource_ShowToggle()
  if Panel_ClassResource:IsShow() then
    Panel_ClassResource_SetShow(false)
  else
    Panel_ClassResource_SetShow(true)
  end
end
function renderModeChange_Phantom_Locate(prevRenderModeList, nextRenderModeList)
  if CheckRenderModebyGameMode(nextRenderModeList) == false then
    return
  end
  Phantom_Locate()
end
function PaGlobal_ClassResource_MaehawBuffSymbolShow(set)
  if false == Panel_ClassResource:GetShow() then
    return
  end
  if __eClassType_BladeMasterWoman ~= curClassType then
    return
  end
  _maehwaIcon_Point4:SetShow(set)
end
function PaGlobal_ClassResource_CanMeahwaIconEnable()
  local selfPlayer = getSelfPlayer()
  if nil == selfPlayer then
    return false
  end
  local classType = selfPlayer:getClassType()
  if classType ~= __eClassType_BladeMasterWoman then
    return false
  end
  local checkSkillType = ToClient_GetSelfPlayerSkillType()
  if nil == checkSkillType then
    return false
  elseif nil ~= Panel_MainStatus_Remaster and true == Panel_MainStatus_Remaster:GetShow() then
    if __eSkillTypeParam_Inherit ~= checkSkillType then
      if nil ~= Panel_MainStatus_Remaster_MaeHwaResourceHide then
        Panel_MainStatus_Remaster_MaeHwaResourceHide()
      end
    elseif nil ~= Panel_MainStatus_Remaster_MaeHwaResourceShow then
      Panel_MainStatus_Remaster_MaeHwaResourceShow()
    end
  end
  local isAwaken = __eSkillTypeParam_Inherit == checkSkillType
  return isAwaken
end
function PaGlobal_ClassResource_CanNovaIconEnable()
  if false == ToClient_IsContentsGroupOpen("1466") then
    return
  end
  local selfPlayer = getSelfPlayer()
  if nil == selfPlayer then
    return false
  end
  local classType = selfPlayer:getClassType()
  if classType ~= __eClassType_Nova then
    return false
  end
  local checkSkillType = ToClient_GetSelfPlayerSkillType()
  if nil == checkSkillType then
    return false
  end
  local isAwaken = __eSkillTypeParam_Awaken == checkSkillType
  if nil ~= Panel_MainStatus_Remaster and true == Panel_MainStatus_Remaster:GetShow() and nil ~= Panel_MainStatus_Remaster_SetNovaResourceShow then
    Panel_MainStatus_Remaster_SetNovaResourceShow(isAwaken)
  end
  return isAwaken
end
function Panel_ClassResource_SetShow(isShow, isAni)
  local isGetUIInfo = false
  if 0 < ToClient_GetUiInfo(CppEnums.PAGameUIType.PAGameUIPanel_ClassResource, 0, CppEnums.PanelSaveType.PanelSaveType_IsShow) then
    isGetUIInfo = true
  else
    isGetUIInfo = false
  end
  Panel_ClassResource:SetShow(isShow and isGetUIInfo and not PaGlobalFunc_IsRemasterUIOption())
end
registerEvent("FromClient_RenderModeChangeState", "renderModeChange_Phantom_Locate")
init()
Phantom_Locate()
registerEvent("subResourceChanged", "ClassResource_Update")
registerEvent("EventPlayerPvPAbleChanged", "Phantom_Locate")
registerEvent("onScreenResize", "Phantom_Resize")
registerEvent("FromClient_ApplyUISettingPanelInfo", "Phantom_Resize")
Panel_ClassResource:addInputEvent("Mouse_LUp", "ResetPos_WidgetButton()")
